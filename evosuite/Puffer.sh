usage() { echo -e "Usage: $0 \n a - Generate tests using random test generator \n b - Run tests with soot analysis \n c - Derive constraints \n d - Generate performance tests \n e - Compile and execute generated tests \n secondParameter - packageName \n thirdParameter - className 
without .class" 1>&2; exit 1; }
track=0
p1=p2=p3=p4=0
while getopts ":Rabcde:" o; do
	case "${o}" in
		R)  
			p1=1
			p2=1
			p3=1
			p4=1
			p5=1
			echo " Run all phases"
			;;  
		a)  
			p1=1
			echo " Phase 1 : Generate tests using random test generator"
			;;  
		b)  
			echo " Phase 2 : Run tests with soot analysis"
			p2=1
			;;  
		c)  
			echo " Phase 3 : Derive constraints"
			p3=1
			;;  
		d)  
			echo " Phase 4 : Generate performance tests"
			p4=1
			;;  
		e)  
			echo " Phase 5 : Compile and execute generated tests"
			p5=1
			;;  
		*)  
			usage
			;;  
	esac
done

function generateTest 
{
	echo -e  "2\n" | sudo update-alternatives --config java
	java -version
#	java -jar evosuite-1.0.1.jar -generateRandom  -Ddse_probability=1.0  -Dbranch_eval=true  -Dbranch_statement=true -Dfilter_sandbox_tests=true -Dassertions=false -Dfilter_assertions=true -Dcriterion=method:statement:branch:output   -projectCP ./../benchmark/ -class $1 
	java -jar evosuite-1.0.1.jar -projectCP ./../benchmark/ -class $1 
}

function parseTest 
{
	path=`tr '.' '/' <<< "$1"`
	cp "evosuite-tests/"$path"/"$2"_ESTest.java" . 
	./scripts/parseTest.sh $2"_ESTest.java"
}

function deriveConstraints
{
	cd ../sootAnalysis
	ant -f build.xml
	cp src/benchmarks/Summary.txt classes/.
	cd classes
	java -cp ../lib/soot.jar:.:../../benchmark/. javato.activetesting.DeriveConstraints $1
	cd ../
}

function generateTests
{
	cd ../sootAnalysis
	ant -f build.xml
	cd classes
	folderName=`tr '.' '/' <<< "$2"`
	ls "../../benchmark/"$folderName > list
	params=""
	while read line
	do
		params=$params" "$2"."${line%.*}
	done < list
	java -cp ../lib/soot.jar:.:../../benchmark javato.activetesting.GetCandidateMethods $1 $params
	cd ..
}

function check
{
        base=`echo $1 | tr . /`
        data="../perfTests/"$2"/*"
        data="../perfTests/all/*"
        addr="../benchmark/"$base"/"
	counter=0;
        for file in $data
        do
                cp $file $addr
                cd ../benchmark
                name=`basename $file`
                echo -e "\t\t\t***********Compiling $base/$name ****************"       
                javac $base"/"$name
                if [ $? -ne 0 ]
                then

                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
                        echo -e "\t \t \t COMPILATION FAILED FOR $base/$name "
			echo -e "\t \t \t Removing the file $file"
                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
			rm $file	
			rm $addr`basename $file`	
                        cd ../evosuite
                        continue;
                fi
		filename="${file%.*}"
		extension="${filename##*/}"
                java $1"."$extension 10
                if [ $? -ne 0 ]
                then

                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
                        echo -e "\t \t \t EXECUTION FAILED FOR $base/$name "
			echo -e "\t \t \t Removing the file $file"
                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
			rm $file	
			rm $addr`basename $file`	
                        cd ../evosuite
                        continue;
                else
                        echo -e "\t\t\t***********Execution successful for $base/$name ****************"      
		fi
	
		#rm $addr`basename $file`	
		(( counter++ ))
                cd ../evosuite
        done
        echo -e "\t \t \t ----------------------------------------------- "
	echo -e "\t \t \t Total number of tests generated : $counter"
        echo -e "\t \t \t ---------------------------------------------- "
}

packageName=$2
name=$3
className=$packageName"."$name
log="out"$name".file"

if [[ $p1 == 1 ]]
then
	echo "................................ Processing $name ..........................................."
	#generateTest $className | tee -a $log
	generateTest $className #> $log
	echo "................................ Parsing $name"_ESTest.java" ................................."
	#parseTest $packageName $name | tee -a $log
	parseTest $packageName $name #>> $log
	rm -rf evosuite-report evosuite-tests 
fi
if [[ $p2 == 1 ]]
then
	pwd
	echo "................. Running soot analysis on tests of  $name"_ESTest.java" ..........................."
		
	dir=EvoTests
	if find "$dir" -maxdepth 0 -empty | read;
	then
	        echo "$dir empty."
	        echo "No tests generated!!."
		continue;
	fi
	echo -e "1\n" | sudo update-alternatives --config java	
	java -version
	rm -f ../sootAnalysis/src/benchmarks/Summary.txt  ../sootAnalysis/classes/Summary.txt  ../sootAnalysis/classes/revisedSummary.txt
	#./scripts/run-soot.sh $packageName $packageName"."$name | tee -a $log
	./scripts/run-soot.sh $packageName $packageName"."$name #>> $log
	
fi
if [[ $p3 == 1 ]]
then
	echo "............................... Deriving constraints for $name ..........................."
	#deriveConstraints $className $packageName | tee -a $log
	deriveConstraints $className $packageName #>> $log
fi
if [[ $p4 == 1 ]]
then
	echo "............................... Generating tests for $name ..........................."
	#generateTests $className $packageName | tee -a $log
	generateTests $className $packageName #>> $log 
fi
if [[ $p5 == 1 ]]
then
	echo "............................... Generating tests for $name ..........................."
	check $packageName $name 
fi


