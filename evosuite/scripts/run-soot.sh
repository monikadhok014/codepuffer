# This script is used to run dynamic analysis on tests generated using evosuite.

name=`tr '.' '/' <<< "$1"`
target="../benchmark/"$name
dir=EvoTests

if find "$dir" -maxdepth 0 -empty | read;
then
	echo "$dir empty."
	echo "No tests generated!!."
	exit 127
else
	for filename in EvoTests/*
	do

		echo " ##################### Processing : " $filename " ############################# "		#Processing message"
		base="Test.java"		       				 #Find out the base name of the file
		cp $filename $target"/"$base					#Copy the file from EvoTests to location in benchmark folder
		echo "copied $filename to $target"/"$base"
		cd ../benchmark										#Go in to the benchmark folder and compile it
		echo "Shifted in to benchmarks folder"

		name=`basename $filename`
		echo "Compiling  $target"/"$base"
		javac $target"/"$base											#Compile the file thus copied
		if [ $? -ne 0 ]
		then
			echo -e "\t \t \t ***************ERROR: Most probably versions do not match for Evosuite and soot.*****************"
			echo -e  "\t \t \t ***************ERROR: Deleting the result for this test.***********"
			cd ../evosuite
			continue;
		fi

		echo "Running soot analysis"
		cd ../sootAnalysis											#Go in to sootAnalysis
		ant  -Djavato.app.name=$2 -Djavato.app.main.class=$1".Test" -f run.xml genSummary 			#Process it for generating summaries
		cd ../evosuite
	done;
	echo "Calfuzzer run has finished!"
fi

