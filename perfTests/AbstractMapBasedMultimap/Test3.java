package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		com.google.common.collect.ArrayListMultimap p11= com.google.common.collect.ArrayListMultimap.create();
		java.lang.Object p12= new java.lang.Object() ;
		java.util.TreeSet p13= new java.util.TreeSet() ;
		for (int i = 0; i < largeNumber; i++) {			 p13.add(i);
		}
		com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet p14=  p11.new WrappedNavigableSet( p12, p13,null ) ;

		/* Create first parameter. */
		ArrayList p15= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p15.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p14.containsAll( p15);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
