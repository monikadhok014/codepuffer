package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet;
import java.util.ArrayList;
public class Test7
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		com.google.common.collect.ArrayListMultimap p6= com.google.common.collect.ArrayListMultimap.create();
		java.lang.Object p7= new java.lang.Object() ;
		java.util.TreeSet p8= new java.util.TreeSet() ;
		for (int i = 1; i < largeNumber; i++) {			 p8.add(i);
		}
		com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet p9=  p6.new WrappedNavigableSet( p7, p8,null ) ;

		/* Create first parameter. */
		ArrayList p10= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p10.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p9.retainAll( p10);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
