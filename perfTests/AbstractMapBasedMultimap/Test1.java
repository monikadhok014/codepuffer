package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		com.google.common.collect.ArrayListMultimap p1= com.google.common.collect.ArrayListMultimap.create();
		java.lang.Object p2= new java.lang.Object() ;
		java.util.TreeSet p3= new java.util.TreeSet() ;
		for (int i = 0; i < largeNumber; i++) {			 p3.add(i);
		}
		com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet p4=  p1.new WrappedNavigableSet( p2, p3,null ) ;

		/* Create first parameter. */
		ArrayList p5= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p5.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p4.containsAll( p5);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
