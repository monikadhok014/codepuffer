package com.google.common.collect;

import com.google.common.collect.BoundType;
import com.google.common.collect.DescendingImmutableSortedMultiset;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.EmptyContiguousSet;
import com.google.common.collect.EmptyImmutableListMultimap;
import com.google.common.collect.EmptyImmutableSetMultimap;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Multiset;
import com.google.common.collect.RegularImmutableSortedMultiset;
import com.google.common.collect.RegularImmutableSortedSet;
import com.google.common.collect.SortedLists;
import com.google.common.collect.Synchronized;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		TreeMap<BoundType, MapMaker.RemovalCause> treeMap0 = new TreeMap<BoundType, MapMaker.RemovalCause>();
		EmptyImmutableSetMultimap emptyImmutableSetMultimap0 = EmptyImmutableSetMultimap.INSTANCE;
		Synchronized.SynchronizedNavigableMap<BoundType, MapMaker.RemovalCause> synchronized_SynchronizedNavigableMap0 = new Synchronized.SynchronizedNavigableMap<BoundType, MapMaker.RemovalCause>(treeMap0, emptyImmutableSetMultimap0);
		NavigableMap<BoundType, MapMaker.RemovalCause> navigableMap0 = synchronized_SynchronizedNavigableMap0.delegate();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
