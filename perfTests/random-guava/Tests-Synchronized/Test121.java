package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Synchronized;
import java.util.HashSet;

public class Test {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		HashSet<Integer> set = new HashSet<Integer>();
		set.add(0);
		set.add(1);
		Synchronized.SynchronizedSet syncSet = new Synchronized.SynchronizedSet(set, null);

		List<Integer> list = new ArrayList<Integer>();
			list.add(0);
		
	        boolean boolean1 = syncSet.removeAll(list);
		long stop = System.currentTimeMillis();
		System.out.println(" Total time taken : "+ (stop - start));

	}
}
