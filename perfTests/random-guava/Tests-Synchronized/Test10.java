package com.google.common.collect;

import com.google.common.collect.BoundType;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.EmptyContiguousSet;
import com.google.common.collect.EmptyImmutableBiMap;
import com.google.common.collect.EmptyImmutableListMultimap;
import com.google.common.collect.MapMaker;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableSortedMultiset;
import com.google.common.collect.RegularImmutableSortedSet;
import com.google.common.collect.Synchronized;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Synchronized.SynchronizedNavigableMap<EmptyImmutableBiMap, MapMaker.RemovalCause> synchronized_SynchronizedNavigableMap0 = null;
		try {
		synchronized_SynchronizedNavigableMap0 = new Synchronized.SynchronizedNavigableMap<EmptyImmutableBiMap, MapMaker.RemovalCause>((NavigableMap<EmptyImmutableBiMap, MapMaker.RemovalCause>) null, (Object) null);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
