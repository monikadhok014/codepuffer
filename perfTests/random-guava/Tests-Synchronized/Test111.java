package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Synchronized;
import java.util.HashSet;

public class Test {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		HashSet<Integer> set = new HashSet<Integer>();
		for(int i=0; i<15000; i++)
			set.add(i);
		Synchronized.SynchronizedSet syncSet = new Synchronized.SynchronizedSet(set, null);

		List<Integer> list = new ArrayList<Integer>();
		for(int i=0; i<15000; i++)
			list.add(i);
		
	        boolean boolean1 = syncSet.retainAll(list);
		long stop = System.currentTimeMillis();
		System.out.println(" Total time taken : "+ (stop - start));

	}
}
