package com.google.common.collect;

import com.google.common.base.Converter;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.EmptyImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.common.collect.SingletonImmutableBiMap;
import com.google.common.collect.SortedLists;
import java.io.CharArrayWriter;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		TreeMap<SortedLists.KeyPresentBehavior, SortedLists.KeyAbsentBehavior> treeMap0 = new TreeMap<SortedLists.KeyPresentBehavior, SortedLists.KeyAbsentBehavior>();
		Maps.UnmodifiableNavigableMap<SortedLists.KeyPresentBehavior, SortedLists.KeyAbsentBehavior> maps_UnmodifiableNavigableMap0 = new Maps.UnmodifiableNavigableMap<SortedLists.KeyPresentBehavior, SortedLists.KeyAbsentBehavior>(treeMap0);
		Maps.NavigableKeySet<SortedLists.KeyPresentBehavior, SortedLists.KeyAbsentBehavior> maps_NavigableKeySet0 = new Maps.NavigableKeySet<SortedLists.KeyPresentBehavior, SortedLists.KeyAbsentBehavior>(maps_UnmodifiableNavigableMap0);
		Comparator<? super SortedLists.KeyPresentBehavior> comparator0 = maps_NavigableKeySet0.comparator();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
