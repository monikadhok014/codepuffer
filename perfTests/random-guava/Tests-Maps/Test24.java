package com.google.common.collect;

import com.google.common.base.Converter;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.EmptyImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.common.collect.SingletonImmutableBiMap;
import com.google.common.collect.SortedLists;
import java.io.CharArrayWriter;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		EmptyImmutableBiMap emptyImmutableBiMap0 = EmptyImmutableBiMap.INSTANCE;
		Integer integer0 = new Integer(1);
		SingletonImmutableBiMap<EmptyImmutableBiMap, Integer> singletonImmutableBiMap0 = new SingletonImmutableBiMap<EmptyImmutableBiMap, Integer>(emptyImmutableBiMap0, integer0);
		// Undeclared exception!
		try {
		Maps.filterEntries((BiMap<EmptyImmutableBiMap, Integer>) singletonImmutableBiMap0, (Predicate<? super Map.Entry<EmptyImmutableBiMap, Integer>>) null);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
