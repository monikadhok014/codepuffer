package com.google.common.collect;

import com.google.common.base.Converter;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.EmptyImmutableBiMap;
import com.google.common.collect.Maps;
import com.google.common.collect.SingletonImmutableBiMap;
import com.google.common.collect.SortedLists;
import java.io.CharArrayWriter;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		TreeMap<SortedLists.KeyAbsentBehavior, SortedLists.KeyPresentBehavior> treeMap0 = new TreeMap<SortedLists.KeyAbsentBehavior, SortedLists.KeyPresentBehavior>();
		Maps.UnmodifiableNavigableMap<SortedLists.KeyAbsentBehavior, SortedLists.KeyPresentBehavior> maps_UnmodifiableNavigableMap0 = new Maps.UnmodifiableNavigableMap<SortedLists.KeyAbsentBehavior, SortedLists.KeyPresentBehavior>(treeMap0, (Maps.UnmodifiableNavigableMap<SortedLists.KeyAbsentBehavior, SortedLists.KeyPresentBehavior>) null);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
