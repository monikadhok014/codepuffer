package org.apache.tools.ant.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.apache.tools.ant.util.VectorSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		VectorSet vectorSet0 = new VectorSet();
		Object object0 = new Object();
		vectorSet0.add(0, object0);
		// Undeclared exception!
		try {
		vectorSet0.removeAll(vectorSet0);
		
		} catch(NoSuchElementException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
