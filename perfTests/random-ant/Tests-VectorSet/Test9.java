package org.apache.tools.ant.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.apache.tools.ant.util.VectorSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		VectorSet vectorSet0 = new VectorSet((Collection) null);
		vectorSet0.removeRange(806, 806);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
