package org.apache.tools.ant.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.apache.tools.ant.util.VectorSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		linkedList0.offerFirst("");
		VectorSet vectorSet0 = new VectorSet((Collection) linkedList0);
		vectorSet0.removeElement("");
		// Undeclared exception!
		try {
		vectorSet0.addAll((-1), (Collection) linkedList0);
		
		} catch(ArrayIndexOutOfBoundsException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
