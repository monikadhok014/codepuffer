package org.apache.tools.ant.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.apache.tools.ant.util.VectorSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		linkedList0.add("");
		VectorSet vectorSet0 = new VectorSet((Collection) linkedList0);
		VectorSet vectorSet1 = new VectorSet((Collection) vectorSet0);
		vectorSet1.removeElement("");
		vectorSet1.containsAll(vectorSet0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
