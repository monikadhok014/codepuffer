package org.apache.tools.ant.util;

import java.util.Stack;
import org.apache.tools.ant.util.IdentityStack;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		Object object0 = new Object();
		IdentityStack identityStack0 = new IdentityStack(object0);
		IdentityStack identityStack1 = new IdentityStack((Object) identityStack0);
		identityStack1.add(object0);
		int int0 = identityStack1.lastIndexOf((Object) identityStack0, 1);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
