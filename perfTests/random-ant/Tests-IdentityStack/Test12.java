package org.apache.tools.ant.util;

import java.util.Stack;
import org.apache.tools.ant.util.IdentityStack;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		Stack<Integer> stack0 = new Stack<Integer>();
		IdentityStack identityStack0 = IdentityStack.getInstance(stack0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
