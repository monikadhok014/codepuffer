package org.apache.commons.collections4.bidimap;
import java.util.Collection;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.bidimap.DualHashBidiMap p13= new org.apache.commons.collections4.bidimap.DualHashBidiMap( ) ;
		org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet p14= new org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet( p13 ) ;

		/* Create first parameter. */
		ArrayList p15= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p15.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p14.retainAll( p15);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
