package org.apache.commons.collections4;
import java.util.Collection;
import java.util.Collection;
import org.apache.commons.collections4.ListUtils;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p9= new ArrayList();
		for (int i = 0; i < largeNumber * 0.5; i++) {
			 p9.add(i); 
		}

		ArrayList p10= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p10.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.apache.commons.collections4.ListUtils.retainAll( p9,  p10);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
