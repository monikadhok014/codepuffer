package com.google.common.collect;
import java.lang.Iterable;
import java.util.Collection;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p5= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p5.add(new Object()); 
		}

		ArrayList p6= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p6.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		com.google.common.collect.Iterables.retainAll( p5,  p6);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
