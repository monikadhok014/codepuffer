package org.apache.tools.ant.util;
import java.util.Collection;
import org.apache.tools.ant.util.VectorSet;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.tools.ant.util.VectorSet p1 = new org.apache.tools.ant.util.VectorSet();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p2 = (int)Math.random();

		ArrayList p3 = new ArrayList();
		 p1.addAll( p2,  p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
