package org.apache.pdfbox.cos;
import java.util.Collection;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
public class Test18
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		org.apache.pdfbox.pdmodel.common.COSArrayList p1 = new org.apache.pdfbox.pdmodel.common.COSArrayList();
		for (int i = 1; i < 3000; i++) 						 				p1.add(alpha(i).toString());
		ArrayList p3= new ArrayList();
		for (int i = 1; i < 3000; i++) 
			 p3.add(alpha(i).toString()); 
		long start = System.currentTimeMillis();
		 p1.retainAll( p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  

}
