package org.apache.pdfbox.pdmodel.common;

import java.awt.Canvas;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Label;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.Scrollbar;
import java.awt.TextComponent;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;
import javax.swing.JLayeredPane;
import javax.swing.table.DefaultTableModel;
import org.apache.fontbox.afm.FontMetric;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSUnread;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.PDDictionaryWrapper;
import org.apache.pdfbox.pdmodel.common.PDMemoryStream;
import org.apache.pdfbox.pdmodel.common.PDNamedTextStream;
import org.apache.pdfbox.pdmodel.common.PDObjectStream;
import org.apache.pdfbox.pdmodel.common.PDPageLabelRange;
import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.common.PDTextStream;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		COSArrayList<ThaiBuddhistDate> cOSArrayList0 = new COSArrayList<ThaiBuddhistDate>();
		COSArray cOSArray0 = cOSArrayList0.toList();
		COSName cOSName0 = COSName.BASE_STATE;
		COSArrayList<PDRectangle> cOSArrayList1 = new COSArrayList<PDRectangle>((PDRectangle) null, (COSBase) cOSArray0, (COSDictionary) null, cOSName0);
		boolean boolean0 = cOSArrayList1.remove((Object) cOSArray0);
	
	
		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
