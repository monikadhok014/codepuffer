package org.jfree.data;

import java.awt.Canvas;
import java.util.List;
import org.jfree.data.ComparableObjectItem;
import org.jfree.data.ComparableObjectSeries;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.Quarter;
import org.jfree.data.time.Week;
import org.jfree.data.time.Year;
import org.jfree.data.xy.VectorDataItem;
import org.jfree.data.xy.VectorSeries;
import org.jfree.data.xy.XIntervalDataItem;
import org.jfree.data.xy.XIntervalSeries;
import org.jfree.data.xy.XYIntervalDataItem;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.YIntervalSeries;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		ComparableObjectSeries comparableObjectSeries0 = new ComparableObjectSeries((Comparable) 2);
		// Undeclared exception!
		try {
		comparableObjectSeries0.updateByIndex(2, comparableObjectSeries0);
		
		} catch(IndexOutOfBoundsException e) {
		//
		// Index: 2, Size: 0
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
