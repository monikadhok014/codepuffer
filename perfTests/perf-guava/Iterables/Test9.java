package com.google.common.collect;
import java.lang.Iterable;
import java.util.Collection;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
public class Test9
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p7= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p7.add((int)Math.random()); 
		}

		ArrayList p8= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p8.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		com.google.common.collect.Iterables.removeAll( p7,  p8);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
