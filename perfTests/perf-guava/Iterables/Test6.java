package com.google.common.collect;
import java.lang.Iterable;
import java.util.Collection;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p1= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		ArrayList p2= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p2.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		com.google.common.collect.Iterables.removeAll( p1,  p2);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
