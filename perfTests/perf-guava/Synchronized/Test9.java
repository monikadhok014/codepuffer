package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Synchronized.SynchronizedSet;
import java.util.ArrayList;
public class Test9
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashSet p13= new java.util.HashSet() ;
		java.lang.Object p14= new java.lang.Object() ;
		com.google.common.collect.Synchronized.SynchronizedSet p15= new com.google.common.collect.Synchronized.SynchronizedSet( p13, p14 ) ;

		/* Create first parameter. */
		ArrayList p16= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p16.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p15.retainAll( p16);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
