package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet;
import java.util.ArrayList;
public class Test4
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		com.google.common.collect.ArrayListMultimap p16= com.google.common.collect.ArrayListMultimap.create();
		java.lang.Object p17= new java.lang.Object() ;
		java.util.TreeSet p18= new java.util.TreeSet() ;
		for (int i = 0; i < largeNumber; i++) {			 p18.add(i);
		}
		com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet p19=  p16.new WrappedNavigableSet( p17, p18,null ) ;

		/* Create first parameter. */
		ArrayList p20= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p20.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p19.containsAll( p20);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
