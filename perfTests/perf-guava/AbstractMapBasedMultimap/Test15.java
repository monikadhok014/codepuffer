package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet;
import java.util.ArrayList;
public class Test15
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		com.google.common.collect.ArrayListMultimap p21= com.google.common.collect.ArrayListMultimap.create();
		java.lang.Object p22= new java.lang.Object() ;
		java.util.TreeSet p23= new java.util.TreeSet() ;
		for (int i = 0; i < largeNumber * 0.5; i++) {			 p23.add(i);
		}
		com.google.common.collect.AbstractMapBasedMultimap.WrappedNavigableSet p24=  p21.new WrappedNavigableSet( p22, p23,null ) ;

		/* Create first parameter. */
		ArrayList p25= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p25.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p24.removeAll( p25);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
