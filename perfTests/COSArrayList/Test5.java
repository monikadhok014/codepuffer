package org.apache.pdfbox.pdmodel.common;
import java.util.Collection;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.pdmodel.common.COSArrayList p9 = new org.apache.pdfbox.pdmodel.common.COSArrayList();
		for (int i = 0; i < largeNumber * 0.5; i++) {
			 p9.add(i); 
		}

		/* Create first parameter. */
		ArrayList p10= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p10.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p9.containsAll( p10);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
