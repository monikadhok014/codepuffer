package org.apache.pdfbox.pdmodel.common;
import java.util.Collection;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
public class Test11
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.pdmodel.common.COSArrayList p1 = new org.apache.pdfbox.pdmodel.common.COSArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		/* Create first parameter. */
		ArrayList p2= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p2.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p1.removeAll( p2);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
