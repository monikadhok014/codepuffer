package org.apache.pdfbox.pdmodel.common;
import java.util.Collection;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
public class Test13
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.pdmodel.common.COSArrayList p5 = new org.apache.pdfbox.pdmodel.common.COSArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p5.add(new Object()); 
		}

		/* Create first parameter. */
		ArrayList p6= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p6.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p5.removeAll( p6);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
