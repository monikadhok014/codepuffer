package org.apache.tools.ant.util;
import java.util.Collection;
import org.apache.tools.ant.util.VectorSet;
import java.util.ArrayList;
public class Test10
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.tools.ant.util.VectorSet p13 = new org.apache.tools.ant.util.VectorSet();
		for (int i = 0; i < largeNumber; i++) {
			 p13.add(new Object()); 
		}

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p14 = (int)Math.random();

		ArrayList p15 = new ArrayList();
		 p13.addAll( p14,  p15);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
