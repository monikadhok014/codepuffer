package org.jfree.data;
import org.jfree.data.ComparableObjectSeries;
import java.util.ArrayList;
public class Test4
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
			Comparable p12= new Comparable() { 
			@Override
			public int compareTo(Object o)
				{
				return 0;
				}
			};

		org.jfree.data.ComparableObjectSeries p13 =  new org.jfree.data.ComparableObjectSeries( p12);

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p14 = (int)Math.random();
		 p13.setMaximumItemCount( p14);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
