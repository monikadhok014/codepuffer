package org.apache.pdfbox.cos;
import java.util.Collection;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.cos.COSArray p7 = new org.apache.pdfbox.cos.COSArray();
		for (int i = 0; i < largeNumber; i++) {			org.apache.pdfbox.pdmodel.common.COSStreamArray p8 = new org.apache.pdfbox.pdmodel.common.COSStreamArray( p7);
			 p7.add(p8);
		}

		/* Create first parameter. */
		ArrayList p9= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p9.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p7.retainAll( p9);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
