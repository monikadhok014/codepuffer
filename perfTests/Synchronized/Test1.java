package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Synchronized.SynchronizedSet;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashSet p1= new java.util.HashSet() ;
		java.lang.Object p2= new java.lang.Object() ;
		com.google.common.collect.Synchronized.SynchronizedSet p3= new com.google.common.collect.Synchronized.SynchronizedSet( p1, p2 ) ;
		for (int i = 0; i < largeNumber; i++) {
			 p3.add(i); 
		}

		/* Create first parameter. */
		ArrayList p4= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p4.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p3.removeAll( p4);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
