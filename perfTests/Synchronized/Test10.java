package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Synchronized.SynchronizedSet;
import java.util.ArrayList;
public class Test10
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashSet p17= new java.util.HashSet() ;
		java.lang.Object p18= new java.lang.Object() ;
		com.google.common.collect.Synchronized.SynchronizedSet p19= new com.google.common.collect.Synchronized.SynchronizedSet( p17, p18 ) ;

		/* Create first parameter. */
		ArrayList p20= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p20.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p19.retainAll( p20);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
