package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Synchronized.SynchronizedSet;
import java.util.ArrayList;
public class Test2
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashSet p5= new java.util.HashSet() ;
		java.lang.Object p6= new java.lang.Object() ;
		com.google.common.collect.Synchronized.SynchronizedSet p7= new com.google.common.collect.Synchronized.SynchronizedSet( p5, p6 ) ;

		/* Create first parameter. */
		ArrayList p8= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p8.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p7.removeAll( p8);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
