package org.apache.commons.collections4.bag;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<Integer> treeBag0 = new TreeBag<Integer>((Comparator<? super Integer>) null);
		Class<Object>[] classArray0 = (Class<Object>[]) Array.newInstance(Class.class, 9);
		InvokerTransformer<Object, Integer> invokerTransformer0 = new InvokerTransformer<Object, Integer>("", (Class<?>[]) classArray0, (Object[]) classArray0);
		TransformedSortedBag<Integer> transformedSortedBag0 = new TransformedSortedBag<Integer>(treeBag0, invokerTransformer0);
		CollectionBag<Integer> collectionBag0 = new CollectionBag<Integer>((Bag<Integer>) transformedSortedBag0);
		Integer integer0 = new Integer((-314));
		// Undeclared exception!
		try {
		collectionBag0.add(integer0, (-1437));
		
		} catch(RuntimeException e) {
		//
		// InvokerTransformer: The method '' on 'class java.lang.Integer' does not exist
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
