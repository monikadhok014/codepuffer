package org.apache.commons.collections4.bag;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<String> treeBag0 = new TreeBag<String>();
		SynchronizedBag<String> synchronizedBag0 = new SynchronizedBag<String>(treeBag0);
		treeBag0.add("l");
		CollectionBag<String> collectionBag0 = new CollectionBag<String>((Bag<String>) synchronizedBag0);
		TreeBag<Boolean> treeBag1 = new TreeBag<Boolean>((Comparator<? super Boolean>) null);
		ConstantTransformer<Boolean, Boolean> constantTransformer0 = new ConstantTransformer<Boolean, Boolean>((Boolean) false);
		treeBag1.add((Boolean) true);
		TransformedSortedBag<Boolean> transformedSortedBag0 = new TransformedSortedBag<Boolean>(treeBag1, constantTransformer0);
		// Undeclared exception!
		try {
		collectionBag0.removeAll(transformedSortedBag0);
		
		} catch(ClassCastException e) {
		//
		// java.lang.String cannot be cast to java.lang.Boolean
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
