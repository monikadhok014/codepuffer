package org.apache.commons.collections4.bag;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<InputStream> treeBag0 = new TreeBag<InputStream>((Comparator<? super InputStream>) null);
		CollectionSortedBag<InputStream> collectionSortedBag0 = new CollectionSortedBag<InputStream>((SortedBag<InputStream>) treeBag0);
		SynchronizedSortedBag<InputStream> synchronizedSortedBag0 = new SynchronizedSortedBag<InputStream>(collectionSortedBag0, treeBag0);
		CollectionBag<InputStream> collectionBag0 = new CollectionBag<InputStream>((Bag<InputStream>) synchronizedSortedBag0);
		PipedInputStream pipedInputStream0 = new PipedInputStream();
		PushbackInputStream pushbackInputStream0 = new PushbackInputStream((InputStream) pipedInputStream0);
		// Undeclared exception!
		try {
		collectionBag0.add((InputStream) pushbackInputStream0, 4312);
		
		} catch(ClassCastException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
