package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedHashSet<String> linkedHashSet0 = new LinkedHashSet<String>();
		linkedHashSet0.add("AIjTYz)prW.zurX*");
		ListOrderedSet<String> listOrderedSet0 = ListOrderedSet.listOrderedSet((Set<String>) linkedHashSet0);
		boolean boolean0 = listOrderedSet0.removeAll(linkedHashSet0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
