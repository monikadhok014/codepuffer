package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		ListOrderedSet<String> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<String>) linkedList0);
		LinkedHashSet<Object> linkedHashSet0 = new LinkedHashSet<Object>((Collection<?>) linkedList0);
		ListOrderedSet<Object> listOrderedSet1 = new ListOrderedSet<Object>(linkedHashSet0);
		ListOrderedSet<Integer> listOrderedSet2 = new ListOrderedSet<Integer>();
		linkedHashSet0.add(listOrderedSet2);
		boolean boolean0 = listOrderedSet1.add((Object) listOrderedSet0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
