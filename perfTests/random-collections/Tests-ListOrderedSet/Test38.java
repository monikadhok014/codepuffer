package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedHashSet<String> linkedHashSet0 = new LinkedHashSet<String>();
		ListOrderedSet<String> listOrderedSet0 = ListOrderedSet.listOrderedSet((Set<String>) linkedHashSet0);
		linkedHashSet0.add("");
		LinkedList<String> linkedList0 = new LinkedList<String>();
		// Undeclared exception!
		try {
		ListOrderedSet.listOrderedSet((Set<String>) listOrderedSet0, (List<String>) linkedList0);
		
		} catch(IllegalArgumentException e) {
		//
		// Set and List must be empty
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
