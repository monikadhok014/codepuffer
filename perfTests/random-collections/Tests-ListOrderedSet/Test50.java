package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedHashSet<String> linkedHashSet0 = new LinkedHashSet<String>();
		ListOrderedSet<String> listOrderedSet0 = ListOrderedSet.listOrderedSet((Set<String>) linkedHashSet0);
		ListOrderedSet<String> listOrderedSet1 = null;
		try {
		listOrderedSet1 = new ListOrderedSet<String>(listOrderedSet0, (List<String>) null);
		
		} catch(NullPointerException e) {
		//
		// List must not be null
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
