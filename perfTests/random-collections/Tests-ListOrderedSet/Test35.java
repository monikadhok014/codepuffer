package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		Object object0 = new Object();
		linkedList0.add(object0);
		ListOrderedSet<String> listOrderedSet0 = new ListOrderedSet<String>();
		boolean boolean0 = listOrderedSet0.removeAll(linkedList0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
