package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		ListOrderedSet<Object> listOrderedSet0 = new ListOrderedSet<Object>();
		List<Object> list0 = listOrderedSet0.asList();
		ListOrderedSet<Object> listOrderedSet1 = ListOrderedSet.listOrderedSet(list0);
		Object object0 = new Object();
		// Undeclared exception!
		try {
		listOrderedSet1.add(1404, object0);
		
		} catch(UnsupportedOperationException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
