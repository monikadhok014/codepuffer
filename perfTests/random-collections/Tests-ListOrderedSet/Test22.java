package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Integer> linkedList0 = new LinkedList<Integer>();
		ListOrderedSet<Integer> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<Integer>) linkedList0);
		LinkedList<Object> linkedList1 = new LinkedList<Object>((Collection<?>) listOrderedSet0);
		ListOrderedSet<Object> listOrderedSet1 = ListOrderedSet.listOrderedSet((List<Object>) linkedList1);
		// Undeclared exception!
		try {
		listOrderedSet1.get(0);
		
		} catch(IndexOutOfBoundsException e) {
		//
		// Index: 0, Size: 0
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
