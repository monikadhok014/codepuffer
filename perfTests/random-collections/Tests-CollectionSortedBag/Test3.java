package org.apache.commons.collections4.bag;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NullIsFalsePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<String> treeBag0 = new TreeBag<String>((Comparator<? super String>) null);
		CollectionBag<String> collectionBag0 = new CollectionBag<String>((Bag<String>) treeBag0);
		treeBag0.add("Y", 984);
		SynchronizedSortedBag<String> synchronizedSortedBag0 = new SynchronizedSortedBag<String>(collectionBag0, collectionBag0);
		CollectionSortedBag<String> collectionSortedBag0 = new CollectionSortedBag<String>((SortedBag<String>) synchronizedSortedBag0);
		// Undeclared exception!
		try {
		collectionSortedBag0.retainAll(collectionBag0);
		// Unstable assertion
		} catch(TooManyResourcesException e) {
		//
		// Loop has been executed more times than the allowed 10000
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
