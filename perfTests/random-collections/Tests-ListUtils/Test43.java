package org.apache.commons.collections4;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections4.Equator;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.functors.AllPredicate;
import org.apache.commons.collections4.functors.ComparatorPredicate;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.EqualPredicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NonePredicate;
import org.apache.commons.collections4.functors.NullIsExceptionPredicate;
import org.apache.commons.collections4.functors.NullIsTruePredicate;
import org.apache.commons.collections4.functors.OrPredicate;
import org.apache.commons.collections4.functors.TransformerPredicate;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Character> linkedList0 = new LinkedList<Character>();
		List<Object> list0 = ListUtils.intersection((List<?>) linkedList0, (List<?>) linkedList0);
		Class<Integer>[] classArray0 = (Class<Integer>[]) Array.newInstance(Class.class, 7);
		InvokerTransformer<Object, String> invokerTransformer0 = new InvokerTransformer<Object, String>("|bah%8?pt)ztt'", (Class<?>[]) classArray0, (Object[]) classArray0);
		List<Object> list1 = ListUtils.transformedList(list0, (Transformer<? super Object, ?>) invokerTransformer0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
