package org.apache.commons.collections4;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections4.Equator;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.functors.AllPredicate;
import org.apache.commons.collections4.functors.ComparatorPredicate;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.EqualPredicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NonePredicate;
import org.apache.commons.collections4.functors.NullIsExceptionPredicate;
import org.apache.commons.collections4.functors.NullIsTruePredicate;
import org.apache.commons.collections4.functors.OrPredicate;
import org.apache.commons.collections4.functors.TransformerPredicate;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		linkedList0.add((String) null);
		Class<Integer>[] classArray0 = (Class<Integer>[]) Array.newInstance(Class.class, 3);
		InvokerTransformer<Object, Boolean> invokerTransformer0 = new InvokerTransformer<Object, Boolean>("", (Class<?>[]) classArray0, (Object[]) classArray0);
		TransformerPredicate<Object> transformerPredicate0 = new TransformerPredicate<Object>((Transformer<? super Object, Boolean>) invokerTransformer0);
		OrPredicate<String> orPredicate0 = new OrPredicate<String>((Predicate<? super String>) transformerPredicate0, (Predicate<? super String>) transformerPredicate0);
		// Undeclared exception!
		try {
		ListUtils.selectRejected((Collection<? extends String>) linkedList0, (Predicate<? super String>) orPredicate0);
		
		} catch(RuntimeException e) {
		//
		// Transformer must return an instanceof Boolean, it was a null object
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
