package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Method, Method>(0, 0);
		HashMap<Method, Method> hashMap0 = new HashMap<Method, Method>();
		hashSetValuedHashMap0.putAll((Map<? extends Method, ? extends Method>) hashMap0);
		ArrayListValuedHashMap<Integer, Integer> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, Integer>(0, 2573);
		arrayListValuedHashMap0.trimToSize();
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap0.new WrappedCollection((Integer) 0);
		abstractMultiValuedMap_WrappedCollection0.toString();
		Integer integer0 = new Integer(0);
		AbstractMap.SimpleEntry<String, Integer> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<String, Integer>("[]", integer0);
		arrayListValuedHashMap0.hashCode();
		AbstractMap.SimpleEntry<Object, Integer> abstractMap_SimpleEntry1 = new AbstractMap.SimpleEntry<Object, Integer>((Map.Entry<?, ? extends Integer>) abstractMap_SimpleEntry0);
		abstractMap_SimpleEntry1.equals(hashMap0);
		// Undeclared exception!
		try {
		Map.Entry.comparingByValue((Comparator<? super Object>) null);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
