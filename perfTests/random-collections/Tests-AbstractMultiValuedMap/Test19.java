package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<String, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<String, Method>(2409);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap0.new WrappedCollection("piPy22DM'");
		abstractMultiValuedMap_WrappedCollection0.add((Method) null);
		ArrayDeque<String> arrayDeque0 = new ArrayDeque<String>();
		abstractMultiValuedMap_WrappedCollection0.removeAll(arrayDeque0);
		ArrayListValuedHashMap<Integer, String> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, String>(2409, 2409);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection1 = arrayListValuedHashMap0.new WrappedCollection((Integer) 2409);
		Object object0 = new Object();
		abstractMultiValuedMap_WrappedCollection1.remove(object0);
		Comparator<Map.Entry<Collection<Integer>, String>> comparator0 = Map.Entry.comparingByValue();
		AbstractMap.SimpleEntry<Integer, String> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<Integer, String>((Integer) 2409, "org.apache.commons.collections4.functors.ExceptionPredicate");
		AbstractMap.SimpleEntry<Integer, String> abstractMap_SimpleEntry1 = new AbstractMap.SimpleEntry<Integer, String>((Map.Entry<? extends Integer, ? extends String>) abstractMap_SimpleEntry0);
		abstractMap_SimpleEntry1.getKey();
		ArrayListValuedHashMap<Object, Object> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Object, Object>();
		arrayListValuedHashMap1.size();
		HashSetValuedHashMap<Integer, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Integer, Object>((MultiValuedMap<? extends Integer, ?>) arrayListValuedHashMap0);
		hashSetValuedHashMap1.containsValue(comparator0);
		ArrayListValuedHashMap<Integer, Integer> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Integer, Integer>(0);
		HashMap<String, Method> hashMap0 = new HashMap<String, Method>();
		boolean boolean0 = hashSetValuedHashMap0.putAll((Map<? extends String, ? extends Method>) hashMap0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
