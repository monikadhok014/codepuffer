package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Integer, Collection<Integer>> hashSetValuedHashMap0 = new HashSetValuedHashMap<Integer, Collection<Integer>>(125);
		Integer integer0 = new Integer(125);
		LinkedList<Collection<Integer>> linkedList0 = new LinkedList<Collection<Integer>>();
		boolean boolean0 = hashSetValuedHashMap0.putAll(integer0, (Iterable<? extends Collection<Integer>>) linkedList0);
		
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Object, Object>();
		Map.Entry.comparingByValue();
		ArrayListValuedHashMap<Object, Object> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Object, Object>(125);
		ArrayListValuedHashMap<Object, Method> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Object, Method>(125, (int) integer0);
		HashMap<Integer, Object> hashMap0 = new HashMap<Integer, Object>();
		Integer integer1 = new Integer(0);
		hashMap0.put(integer1, arrayListValuedHashMap1);
		ArrayListValuedHashMap<Integer, Object> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Integer, Object>((Map<? extends Integer, ?>) hashMap0);
		arrayListValuedHashMap2.remove(linkedList0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
