package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<String, Object> hashSetValuedHashMap0 = new HashSetValuedHashMap<String, Object>((-2615));
		ArrayListValuedHashMap<String, Object> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Object>(1814, 1814);
		hashSetValuedHashMap0.putAll((MultiValuedMap<? extends String, ?>) arrayListValuedHashMap0);
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Method>();
		MultiSet<Method> multiSet0 = hashSetValuedHashMap1.keys();
		HashMap<String, Integer> hashMap0 = new HashMap<String, Integer>();
		ArrayListValuedHashMap<String, Integer> arrayListValuedHashMap1 = new ArrayListValuedHashMap<String, Integer>((Map<? extends String, ? extends Integer>) hashMap0);
		AbstractMap.SimpleImmutableEntry<Method, Object> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<Method, Object>((Method) null, (Object) multiSet0);
		arrayListValuedHashMap1.remove(abstractMap_SimpleImmutableEntry0);
		abstractMap_SimpleImmutableEntry0.getKey();
		HashSetValuedHashMap<String, String> hashSetValuedHashMap2 = new HashSetValuedHashMap<String, String>(1);
		HashSetValuedHashMap<String, String> hashSetValuedHashMap3 = new HashSetValuedHashMap<String, String>((MultiValuedMap<? extends String, ? extends String>) hashSetValuedHashMap2);
		hashSetValuedHashMap3.asMap();
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap4 = new HashSetValuedHashMap<Method, Integer>((-3532));
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap5 = new HashSetValuedHashMap<Method, Integer>((MultiValuedMap<? extends Method, ? extends Integer>) hashSetValuedHashMap4);
		hashSetValuedHashMap5.put((Method) null, (Integer) (-3532));
		ArrayListValuedHashMap<Object, Method> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Object, Method>((MultiValuedMap<?, ? extends Method>) hashSetValuedHashMap1);
		Collection<Map.Entry<Object, Method>> collection0 = (Collection<Map.Entry<Object, Method>>)arrayListValuedHashMap2.entries();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
