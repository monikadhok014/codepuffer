package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Object>((-1));
		hashSetValuedHashMap0.getMap();
		HashMap<String, Collection<Integer>> hashMap0 = new HashMap<String, Collection<Integer>>();
		ArrayListValuedHashMap<String, Collection<Integer>> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Collection<Integer>>((Map<? extends String, ? extends Collection<Integer>>) hashMap0);
		HashSetValuedHashMap<String, Collection<Integer>> hashSetValuedHashMap1 = new HashSetValuedHashMap<String, Collection<Integer>>((MultiValuedMap<? extends String, ? extends Collection<Integer>>) arrayListValuedHashMap0);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap1.new WrappedCollection("yekpEp0Ogb@iN+");
		Vector<Object> vector0 = new Vector<Object>();
		abstractMultiValuedMap_WrappedCollection0.removeAll(vector0);
		HashSetValuedHashMap<Object, Integer> hashSetValuedHashMap2 = new HashSetValuedHashMap<Object, Integer>();
		hashSetValuedHashMap2.clear();
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap3 = new HashSetValuedHashMap<Method, Object>();
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection1 = hashSetValuedHashMap3.new WrappedCollection((Method) null);
		abstractMultiValuedMap_WrappedCollection1.getMapping();
		HashMap<Method, Method> hashMap1 = new HashMap<Method, Method>();
		HashSetValuedHashMap<Object, Method> hashSetValuedHashMap4 = new HashSetValuedHashMap<Object, Method>((Map<?, ? extends Method>) hashMap1);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection2 = hashSetValuedHashMap4.new WrappedCollection((Object) null);
		TreeSet<Method> treeSet0 = new TreeSet<Method>();
		abstractMultiValuedMap_WrappedCollection2.addAll(treeSet0);
		ArrayListValuedHashMap<Integer, Method> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Integer, Method>();
		HashSetValuedHashMap<Integer, Method> hashSetValuedHashMap5 = new HashSetValuedHashMap<Integer, Method>((MultiValuedMap<? extends Integer, ? extends Method>) arrayListValuedHashMap1);
		hashSetValuedHashMap5.createCollection();
		HashMap<Method, Integer> hashMap2 = new HashMap<Method, Integer>();
		ArrayListValuedHashMap<Method, Integer> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Method, Integer>((Map<? extends Method, ? extends Integer>) hashMap2);
		arrayListValuedHashMap2.get((Method) null);
		hashSetValuedHashMap5.removeMapping(hashSetValuedHashMap2, arrayListValuedHashMap1);
		HashSetValuedHashMap<String, Object> hashSetValuedHashMap6 = null;
		try {
		hashSetValuedHashMap6 = new HashSetValuedHashMap<String, Object>((-1), (-1));
		
		} catch(IllegalArgumentException e) {
		//
		// Illegal initial capacity: -1
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
