package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Object, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Method>(972, 5);
		HashSetValuedHashMap<String, Integer> hashSetValuedHashMap1 = new HashSetValuedHashMap<String, Integer>();
		HashMap<String, Collection<Integer>> hashMap0 = new HashMap<String, Collection<Integer>>();
		hashSetValuedHashMap1.setMap(hashMap0);
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap2 = new HashSetValuedHashMap<Method, Method>(5);
		hashSetValuedHashMap1.remove(hashSetValuedHashMap2);
		HashSetValuedHashMap<Integer, Integer> hashSetValuedHashMap3 = new HashSetValuedHashMap<Integer, Integer>(1284);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap3.new WrappedCollection((Integer) 5);
		abstractMultiValuedMap_WrappedCollection0.clear();
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap4 = new HashSetValuedHashMap<Object, String>();
		Object object0 = new Object();
		Integer integer0 = new Integer(1284);
		AbstractMap.SimpleImmutableEntry<String, Integer> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<String, Integer>("Map must not be null.", integer0);
		AbstractMap.SimpleImmutableEntry<String, Integer> abstractMap_SimpleImmutableEntry1 = new AbstractMap.SimpleImmutableEntry<String, Integer>((Map.Entry<? extends String, ? extends Integer>) abstractMap_SimpleImmutableEntry0);
		AbstractMap.SimpleEntry<String, Integer> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<String, Integer>((Map.Entry<? extends String, ? extends Integer>) abstractMap_SimpleImmutableEntry1);
		boolean boolean0 = hashSetValuedHashMap4.containsMapping(object0, abstractMap_SimpleEntry0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
