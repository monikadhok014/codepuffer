package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Integer, Integer> hashSetValuedHashMap0 = new HashSetValuedHashMap<Integer, Integer>();
		ArrayListValuedHashMap<Integer, Integer> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, Integer>((MultiValuedMap<? extends Integer, ? extends Integer>) hashSetValuedHashMap0);
		HashSetValuedHashMap<Integer, Integer> hashSetValuedHashMap1 = new HashSetValuedHashMap<Integer, Integer>((MultiValuedMap<? extends Integer, ? extends Integer>) arrayListValuedHashMap0);
		ArrayListValuedHashMap<String, Method> arrayListValuedHashMap1 = new ArrayListValuedHashMap<String, Method>(17, 17);
		arrayListValuedHashMap0.keySet();
		arrayListValuedHashMap0.wrappedCollection((Integer) 17);
		ArrayListValuedHashMap<Object, Method> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Object, Method>((MultiValuedMap<?, ? extends Method>) arrayListValuedHashMap1);
		AbstractMap.SimpleImmutableEntry<String, Object> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<String, Object>("W?=}#B]I 5!A4v|(+K9", (Object) "-L");
		AbstractMap.SimpleImmutableEntry<Object, Object> abstractMap_SimpleImmutableEntry1 = new AbstractMap.SimpleImmutableEntry<Object, Object>((Map.Entry<?, ?>) abstractMap_SimpleImmutableEntry0);
		arrayListValuedHashMap1.remove(abstractMap_SimpleImmutableEntry1);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap2.new WrappedCollection((Object) arrayListValuedHashMap0);
		abstractMultiValuedMap_WrappedCollection0.add((Method) null);
		abstractMultiValuedMap_WrappedCollection0.add((Method) null);
		
		HashSetValuedHashMap<Integer, Collection<Integer>> hashSetValuedHashMap2 = new HashSetValuedHashMap<Integer, Collection<Integer>>();
		ArrayListValuedHashMap<Integer, Collection<Integer>> arrayListValuedHashMap3 = new ArrayListValuedHashMap<Integer, Collection<Integer>>((MultiValuedMap<? extends Integer, ? extends Collection<Integer>>) hashSetValuedHashMap2);
		arrayListValuedHashMap3.containsKey((Object) null);
		HashSetValuedHashMap<Integer, String> hashSetValuedHashMap3 = new HashSetValuedHashMap<Integer, String>(0, 0);
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap4 = new HashSetValuedHashMap<Method, Integer>();
		Collection<Integer> collection0 = hashSetValuedHashMap4.values();
		boolean boolean0 = hashSetValuedHashMap3.containsValue(collection0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
