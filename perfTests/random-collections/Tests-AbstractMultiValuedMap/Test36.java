package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<String, Collection<Integer>> hashSetValuedHashMap0 = new HashSetValuedHashMap<String, Collection<Integer>>();
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap0.new WrappedCollection("org.apache.commons.collections4.functors.UniquePredicate");
		TreeSet<Collection<Integer>> treeSet0 = new TreeSet<Collection<Integer>>();
		abstractMultiValuedMap_WrappedCollection0.addAll(treeSet0);
		
		HashMap<Method, Method> hashMap0 = new HashMap<Method, Method>();
		ArrayListValuedHashMap<Method, Method> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, Method>((Map<? extends Method, ? extends Method>) hashMap0);
		ArrayListValuedHashMap<Method, Method> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Method, Method>((MultiValuedMap<? extends Method, ? extends Method>) arrayListValuedHashMap0);
		Map<Method, List<Method>> map0 = arrayListValuedHashMap1.getMap();
		HashMap<Method, String> hashMap1 = new HashMap<Method, String>();
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Object, Object>((Map<?, ?>) hashMap1);
		hashSetValuedHashMap1.clear();
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap2 = new HashSetValuedHashMap<Method, Object>(map0);
		hashSetValuedHashMap2.isEmpty();
		HashSetValuedHashMap<Integer, Method> hashSetValuedHashMap3 = new HashSetValuedHashMap<Integer, Method>();
		ArrayListValuedHashMap<Integer, Object> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Integer, Object>((MultiValuedMap<? extends Integer, ?>) hashSetValuedHashMap3);
		Integer integer0 = new Integer(1448);
		arrayListValuedHashMap2.get(integer0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
