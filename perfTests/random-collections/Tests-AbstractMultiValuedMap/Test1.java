package org.apache.commons.collections4.multimap;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import java.util.HashMap;
import java.util.*;

public class Test {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListValuedHashMap<String, HashSet<Integer>>  map = new ArrayListValuedHashMap<String, HashSet<Integer>>();
		map.put("key", new HashSet<Integer>());
		ArrayListValuedHashMap.WrappedCollection wmap = map.new WrappedCollection("key");

		List<Integer> list = new ArrayList<Integer>();
		list.add(0);
		
	        boolean boolean0 = wmap.retainAll(list);
		long stop = System.currentTimeMillis();
		System.out.println(" Total time taken : "+ (stop - start));
	}
}
