package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<String, Integer> hashSetValuedHashMap0 = new HashSetValuedHashMap<String, Integer>();
		hashSetValuedHashMap0.mapIterator();
		HashSetValuedHashMap<Object, Integer> hashSetValuedHashMap1 = new HashSetValuedHashMap<Object, Integer>((MultiValuedMap<?, ? extends Integer>) hashSetValuedHashMap0);
		Map<Object, Collection<Integer>> map0 = hashSetValuedHashMap1.asMap();
		hashSetValuedHashMap1.mapIterator();
		ArrayListValuedHashMap<Object, Object> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Object, Object>(map0);
		arrayListValuedHashMap0.putAll(map0);
		Integer integer0 = new Integer((-627));
		boolean boolean0 = hashSetValuedHashMap1.put(arrayListValuedHashMap0, integer0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
