package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<String, Integer> hashSetValuedHashMap0 = new HashSetValuedHashMap<String, Integer>(1296);
		HashMap<Object, Method> hashMap0 = new HashMap<Object, Method>();
		hashMap0.remove((Object) hashMap0, (Object) hashSetValuedHashMap0);
		HashSetValuedHashMap<Object, Method> hashSetValuedHashMap1 = new HashSetValuedHashMap<Object, Method>((Map<?, ? extends Method>) hashMap0);
		boolean boolean0 = hashSetValuedHashMap1.isEmpty();
		ArrayListValuedHashMap<String, Collection<Integer>> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Collection<Integer>>(1296, 1296);
		arrayListValuedHashMap0.entries();
		boolean boolean1 = arrayListValuedHashMap0.equals(arrayListValuedHashMap0);
		
		boolean boolean2 = arrayListValuedHashMap0.containsKey(hashMap0);
		ArrayListValuedHashMap<Object, Object> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Object, Object>((MultiValuedMap<?, ?>) arrayListValuedHashMap0);
		boolean boolean3 = arrayListValuedHashMap1.putAll((Map<?, ?>) hashMap0);
		
		HashMap<Boolean, String> hashMap1 = new HashMap<Boolean, String>();
		HashSetValuedHashMap<Boolean, String> hashSetValuedHashMap2 = new HashSetValuedHashMap<Boolean, String>((Map<? extends Boolean, ? extends String>) hashMap1);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
