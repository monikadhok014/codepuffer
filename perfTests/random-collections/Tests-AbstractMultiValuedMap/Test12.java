package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		byte[] byteArray0 = new byte[4];
		byteArray0[0] = (byte)123;
		byteArray0[1] = (byte) (-1);
		byteArray0[2] = (byte) (-96);
		byteArray0[3] = (byte) (-107);
		FileSystemHandling.appendDataToFile((EvoSuiteFile) null, byteArray0);
		ArrayListValuedHashMap<String, Integer> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Integer>();
		HashSetValuedHashMap<Object, Boolean> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Boolean>((int) (byte) (-107));
		HashSetValuedHashMap<Method, String> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, String>(2683);
		hashSetValuedHashMap1.put((Method) null, "*fx&hp|17");
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap1.new WrappedCollection((Method) null);
		abstractMultiValuedMap_WrappedCollection0.clear();
		ArrayListValuedHashMap<Integer, Object> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Integer, Object>(125);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
