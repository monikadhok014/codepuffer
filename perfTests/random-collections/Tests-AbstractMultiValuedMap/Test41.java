package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Integer, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Integer, String>(0);
		hashSetValuedHashMap0.put((Integer) 0, "");
		HashMap<String, Collection<Integer>> hashMap0 = new HashMap<String, Collection<Integer>>();
		ArrayListValuedHashMap<String, Collection<Integer>> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Collection<Integer>>((Map<? extends String, ? extends Collection<Integer>>) hashMap0);
		ArrayListValuedHashMap<String, Collection<Integer>> arrayListValuedHashMap1 = new ArrayListValuedHashMap<String, Collection<Integer>>(82, 82);
		arrayListValuedHashMap0.putAll((MultiValuedMap<? extends String, ? extends Collection<Integer>>) arrayListValuedHashMap1);
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Method>(82, (-1182));
		hashSetValuedHashMap1.values();
		ArrayListValuedHashMap<Object, Integer> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Object, Integer>();
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap2 = new HashSetValuedHashMap<Object, Object>();
		HashMap<Method, Method> hashMap1 = new HashMap<Method, Method>();
		ArrayListValuedHashMap<Object, Method> arrayListValuedHashMap3 = new ArrayListValuedHashMap<Object, Method>((Map<?, ? extends Method>) hashMap1);
		hashSetValuedHashMap2.equals(arrayListValuedHashMap3);
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap3 = new HashSetValuedHashMap<Method, Object>();
		hashSetValuedHashMap3.hashCode();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
