package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Object, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Method>();
		HashSetValuedHashMap<Collection<Integer>, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Collection<Integer>, Object>((-4197));
		hashSetValuedHashMap1.mapIterator();
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap2 = new HashSetValuedHashMap<Method, Integer>((-4197));
		hashSetValuedHashMap2.isEmpty();
		Collection<Integer> collection0 = hashSetValuedHashMap2.values();
		hashSetValuedHashMap1.putAll(collection0, (Iterable<?>) collection0);
		AbstractMap.SimpleEntry<Method, Integer> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<Method, Integer>((Method) null, (Integer) (-4197));
		AbstractMap.SimpleEntry<Method, Integer> abstractMap_SimpleEntry1 = new AbstractMap.SimpleEntry<Method, Integer>((Map.Entry<? extends Method, ? extends Integer>) abstractMap_SimpleEntry0);
		abstractMap_SimpleEntry1.getKey();
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap3 = new HashSetValuedHashMap<Object, String>((-4197));
		hashSetValuedHashMap3.keySet();
		HashSetValuedHashMap<Integer, Object> hashSetValuedHashMap4 = new HashSetValuedHashMap<Integer, Object>((-4197));
		HashSetValuedHashMap<String, Method> hashSetValuedHashMap5 = new HashSetValuedHashMap<String, Method>();
		HashSetValuedHashMap<String, Method> hashSetValuedHashMap6 = new HashSetValuedHashMap<String, Method>(0);
		hashSetValuedHashMap5.putAll((MultiValuedMap<? extends String, ? extends Method>) hashSetValuedHashMap6);
		HashSetValuedHashMap<String, Collection<Integer>> hashSetValuedHashMap7 = new HashSetValuedHashMap<String, Collection<Integer>>();
		ArrayListValuedHashMap<String, Collection<Integer>> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Collection<Integer>>((MultiValuedMap<? extends String, ? extends Collection<Integer>>) hashSetValuedHashMap7);
		HashSetValuedHashMap<String, Collection<Integer>> hashSetValuedHashMap8 = new HashSetValuedHashMap<String, Collection<Integer>>((MultiValuedMap<? extends String, ? extends Collection<Integer>>) arrayListValuedHashMap0);
		// Undeclared exception!
		try {
		hashSetValuedHashMap8.doReadObject((ObjectInputStream) null);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
