package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Collection<Integer>, Collection<Integer>> hashMap0 = new HashMap<Collection<Integer>, Collection<Integer>>();
		HashSetValuedHashMap<Collection<Integer>, Collection<Integer>> hashSetValuedHashMap0 = new HashSetValuedHashMap<Collection<Integer>, Collection<Integer>>((Map<? extends Collection<Integer>, ? extends Collection<Integer>>) hashMap0);
		ArrayDeque<Integer> arrayDeque0 = new ArrayDeque<Integer>();
		LinkedHashSet<Collection<Integer>> linkedHashSet0 = new LinkedHashSet<Collection<Integer>>();
		hashSetValuedHashMap0.putAll((Collection<Integer>) arrayDeque0, (Iterable<? extends Collection<Integer>>) linkedHashSet0);
		HashSetValuedHashMap<Collection<Integer>, Collection<Integer>> hashSetValuedHashMap1 = new HashSetValuedHashMap<Collection<Integer>, Collection<Integer>>((MultiValuedMap<? extends Collection<Integer>, ? extends Collection<Integer>>) hashSetValuedHashMap0);
		hashSetValuedHashMap1.toString();
		HashMap<String, String> hashMap1 = new HashMap<String, String>();
		HashSetValuedHashMap<String, String> hashSetValuedHashMap2 = new HashSetValuedHashMap<String, String>((Map<? extends String, ? extends String>) hashMap1);
		hashSetValuedHashMap1.getMap();
		ArrayListValuedHashMap<Method, String> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, String>(0);
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap3 = new HashSetValuedHashMap<Object, Object>(44, (-254));
		hashSetValuedHashMap3.getMap();
		HashMap<Integer, Boolean> hashMap2 = new HashMap<Integer, Boolean>();
		HashSetValuedHashMap<Integer, Boolean> hashSetValuedHashMap4 = new HashSetValuedHashMap<Integer, Boolean>((Map<? extends Integer, ? extends Boolean>) hashMap2);
		hashSetValuedHashMap4.containsMapping(hashSetValuedHashMap3, hashSetValuedHashMap1);
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap5 = new HashSetValuedHashMap<Object, String>((Map<?, ? extends String>) hashMap1);
		hashSetValuedHashMap5.containsMapping(arrayListValuedHashMap0, hashMap1);
		linkedHashSet0.add(arrayDeque0);
		hashSetValuedHashMap5.put(hashSetValuedHashMap2, "LcP*V9??$");
		HashMap<Method, Boolean> hashMap3 = new HashMap<Method, Boolean>();
		HashSetValuedHashMap<Method, Boolean> hashSetValuedHashMap6 = new HashSetValuedHashMap<Method, Boolean>((Map<? extends Method, ? extends Boolean>) hashMap3);
		HashSetValuedHashMap<Method, Boolean> hashSetValuedHashMap7 = new HashSetValuedHashMap<Method, Boolean>((MultiValuedMap<? extends Method, ? extends Boolean>) hashSetValuedHashMap6);
		hashSetValuedHashMap7.mapIterator();
		HashMap<String, Boolean> hashMap4 = new HashMap<String, Boolean>(32);
		hashMap4.put("c'mP+>QpKp]x}5", (Boolean) true);
		hashMap4.putIfAbsent("LcP*V9??$", (Boolean) false);
		HashSetValuedHashMap<String, Boolean> hashSetValuedHashMap8 = new HashSetValuedHashMap<String, Boolean>((Map<? extends String, ? extends Boolean>) hashMap4);
		hashSetValuedHashMap8.keys();
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap9 = new HashSetValuedHashMap<Method, Object>((Map<? extends Method, ?>) hashMap3);
		ArrayListValuedHashMap<Integer, Collection<Integer>> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Integer, Collection<Integer>>(0);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap1.new WrappedCollection((Integer) (-254));
		Stream<Collection<Integer>> stream0 = abstractMultiValuedMap_WrappedCollection0.parallelStream();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
