package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListValuedHashMap<Method, String> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, String>();
		HashSetValuedHashMap<Method, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Method, String>(0, 0);
		arrayListValuedHashMap0.putAll((MultiValuedMap<? extends Method, ? extends String>) hashSetValuedHashMap0);
		ArrayListValuedHashMap<Method, Object> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Method, Object>((MultiValuedMap<? extends Method, ?>) arrayListValuedHashMap0);
		arrayListValuedHashMap1.isEmpty();
		arrayListValuedHashMap0.toString();
		arrayListValuedHashMap1.hashCode();
		arrayListValuedHashMap1.put((Method) null, (Object) null);
		HashSetValuedHashMap<Method, Collection<Integer>> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Collection<Integer>>(0);
		LinkedHashSet<Integer> linkedHashSet0 = new LinkedHashSet<Integer>();
		hashSetValuedHashMap1.put((Method) null, linkedHashSet0);
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap2 = new HashSetValuedHashMap<Object, Object>();
		hashSetValuedHashMap2.putAll((MultiValuedMap<?, ?>) hashSetValuedHashMap1);
		HashSetValuedHashMap<String, String> hashSetValuedHashMap3 = new HashSetValuedHashMap<String, String>();
		hashSetValuedHashMap3.wrappedCollection("TFF'xiS.(/-");
		ArrayListValuedHashMap<Method, Integer> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Method, Integer>();
		hashSetValuedHashMap2.size();
		
		HashMap<String, Method> hashMap0 = new HashMap<String, Method>();
		HashSetValuedHashMap<String, Method> hashSetValuedHashMap4 = new HashSetValuedHashMap<String, Method>((Map<? extends String, ? extends Method>) hashMap0);
		ArrayListValuedHashMap<String, Method> arrayListValuedHashMap3 = new ArrayListValuedHashMap<String, Method>((MultiValuedMap<? extends String, ? extends Method>) hashSetValuedHashMap4);
		arrayListValuedHashMap3.getMap();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
