package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Integer, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Integer, String>(2922, 17);
		ArrayListValuedHashMap<Integer, String> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, String>((MultiValuedMap<? extends Integer, ? extends String>) hashSetValuedHashMap0);
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Object, Object>();
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap2 = new HashSetValuedHashMap<Object, String>((-1));
		// Undeclared exception!
		try {
		hashSetValuedHashMap2.createCollection();
		
		} catch(IllegalArgumentException e) {
		//
		// Illegal initial capacity: -1
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
