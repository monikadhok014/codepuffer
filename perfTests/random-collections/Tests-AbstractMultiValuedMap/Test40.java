package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Object>(3949);
		LinkedList<Collection<Integer>> linkedList0 = new LinkedList<Collection<Integer>>();
		ArrayList<Integer> arrayList0 = new ArrayList<Integer>();
		linkedList0.add((Collection<Integer>) arrayList0);
		linkedList0.offerFirst(arrayList0);
		HashMap<Collection<Integer>, String> hashMap0 = new HashMap<Collection<Integer>, String>(464, 3949);
		hashMap0.put(arrayList0, "bf/'+");
		HashSetValuedHashMap<Collection<Integer>, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Collection<Integer>, Object>((Map<? extends Collection<Integer>, ?>) hashMap0);
		hashSetValuedHashMap0.containsMapping(linkedList0, hashSetValuedHashMap1);
		HashMap<Method, Object> hashMap1 = new HashMap<Method, Object>();
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap2 = new HashSetValuedHashMap<Method, Object>((Map<? extends Method, ?>) hashMap1);
		HashSet<Object> hashSet0 = hashSetValuedHashMap2.createCollection();
		hashMap1.put((Method) null, hashSetValuedHashMap2);
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap3 = new HashSetValuedHashMap<Object, String>();
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap3.new WrappedCollection((Object) hashMap0);
		abstractMultiValuedMap_WrappedCollection0.retainAll(hashSet0);
		ArrayListValuedHashMap<String, Method> arrayListValuedHashMap0 = null;
		try {
		arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Method>((-2853), 44);
		
		} catch(IllegalArgumentException e) {
		//
		// Illegal initial capacity: -2853
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
