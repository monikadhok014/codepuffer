package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		NodeCachingLinkedList<PipedInputStream> nodeCachingLinkedList0 = new NodeCachingLinkedList<PipedInputStream>();
		PipedInputStream pipedInputStream0 = new PipedInputStream();
		nodeCachingLinkedList0.add(pipedInputStream0);
		NodeCachingLinkedList<InputStream> nodeCachingLinkedList1 = new NodeCachingLinkedList<InputStream>((Collection<? extends InputStream>) nodeCachingLinkedList0);
		nodeCachingLinkedList1.removeAllNodes();

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
