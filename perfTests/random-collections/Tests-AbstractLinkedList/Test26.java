package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		NodeCachingLinkedList<Integer> nodeCachingLinkedList0 = new NodeCachingLinkedList<Integer>(0);
		AbstractLinkedList.LinkedSubList<Integer> abstractLinkedList_LinkedSubList0 = new AbstractLinkedList.LinkedSubList<Integer>(nodeCachingLinkedList0, 0, 0);
		abstractLinkedList_LinkedSubList0.add((Integer) 0);
		CursorableLinkedList<Object> cursorableLinkedList0 = new CursorableLinkedList<Object>((Collection<?>) nodeCachingLinkedList0);
		CursorableLinkedList.Cursor<Object> cursorableLinkedList_Cursor0 = new CursorableLinkedList.Cursor<Object>(cursorableLinkedList0, 0);
		cursorableLinkedList_Cursor0.add(cursorableLinkedList_Cursor0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
