package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		NodeCachingLinkedList<Locale.LanguageRange> nodeCachingLinkedList0 = new NodeCachingLinkedList<Locale.LanguageRange>(35);
		AbstractLinkedList.Node<Locale.LanguageRange> abstractLinkedList_Node0 = new AbstractLinkedList.Node<Locale.LanguageRange>();
		nodeCachingLinkedList0.removeNode(abstractLinkedList_Node0);
		// Undeclared exception!
		try {
		nodeCachingLinkedList0.set(35, (Locale.LanguageRange) null);
		
		} catch(IndexOutOfBoundsException e) {
		//
		// Couldn't get the node: index (35) greater than the size of the list (-1).
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
