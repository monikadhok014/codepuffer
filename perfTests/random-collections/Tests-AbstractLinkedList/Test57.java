package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		AbstractLinkedList.Node<InputStream> abstractLinkedList_Node0 = new AbstractLinkedList.Node<InputStream>();
		AbstractLinkedList.Node<InputStream> abstractLinkedList_Node1 = new AbstractLinkedList.Node<InputStream>(abstractLinkedList_Node0, abstractLinkedList_Node0, (InputStream) null);
		AbstractLinkedList.Node<InputStream> abstractLinkedList_Node2 = abstractLinkedList_Node1.getPreviousNode();

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
