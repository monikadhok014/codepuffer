package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		NodeCachingLinkedList<String> nodeCachingLinkedList0 = new NodeCachingLinkedList<String>();
		AbstractLinkedList.LinkedSubList<String> abstractLinkedList_LinkedSubList0 = new AbstractLinkedList.LinkedSubList<String>(nodeCachingLinkedList0, 0, 0);
		AbstractLinkedList.LinkedSubListIterator<String> abstractLinkedList_LinkedSubListIterator0 = new AbstractLinkedList.LinkedSubListIterator<String>(abstractLinkedList_LinkedSubList0, 0);
		abstractLinkedList_LinkedSubListIterator0.nextIndex = 1199;
		// Undeclared exception!
		try {
		abstractLinkedList_LinkedSubListIterator0.next();
		
		} catch(NoSuchElementException e) {
		//
		// No element at index 1199.
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
