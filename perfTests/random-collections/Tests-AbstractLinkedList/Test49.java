package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		NodeCachingLinkedList<Locale.LanguageRange> nodeCachingLinkedList0 = new NodeCachingLinkedList<Locale.LanguageRange>();
		CursorableLinkedList<Locale.LanguageRange> cursorableLinkedList0 = new CursorableLinkedList<Locale.LanguageRange>((Collection<? extends Locale.LanguageRange>) nodeCachingLinkedList0);
		Locale.FilteringMode locale_FilteringMode0 = Locale.FilteringMode.MAP_EXTENDED_RANGES;
		List<Locale> list0 = Locale.filter((List<Locale.LanguageRange>) cursorableLinkedList0, (Collection<Locale>) null, locale_FilteringMode0);
		CursorableLinkedList<Locale> cursorableLinkedList1 = new CursorableLinkedList<Locale>((Collection<? extends Locale>) list0);
		// Undeclared exception!
		try {
		cursorableLinkedList1.addAll((Collection<? extends Locale>) null);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
