package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		Class<Boolean> class0 = Boolean.class;
		Class<String>[] classArray0 = (Class<String>[]) Array.newInstance(Class.class, 1);
		Class<String> class1 = String.class;
		classArray0[0] = class1;
		InstantiateFactory<Boolean> instantiateFactory0 = new InstantiateFactory<Boolean>(class0, (Class<?>[]) classArray0, (Object[]) classArray0);
		LinkedList<String> linkedList0 = new LinkedList<String>();
		FactoryTransformer<String, Object> factoryTransformer0 = new FactoryTransformer<String, Object>((Factory<?>) instantiateFactory0);
		IndexedCollection<Object, String> indexedCollection0 = IndexedCollection.nonUniqueIndexedCollection((Collection<String>) linkedList0, (Transformer<String, Object>) factoryTransformer0);
		linkedList0.add((String) null);
		// Undeclared exception!
		try {
		IndexedCollection.nonUniqueIndexedCollection((Collection<String>) indexedCollection0, (Transformer<String, Object>) factoryTransformer0);
		
		} catch(IllegalArgumentException e) {
		//
		// java.lang.ClassCastException@63f1b36c
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
