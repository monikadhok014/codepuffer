package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		ConstantTransformer<Object, Object> constantTransformer0 = new ConstantTransformer<Object, Object>((Object) linkedList0);
		IndexedCollection<Object, Object> indexedCollection0 = IndexedCollection.uniqueIndexedCollection((Collection<Object>) linkedList0, (Transformer<Object, Object>) constantTransformer0);
		Class<String>[] classArray0 = (Class<String>[]) Array.newInstance(Class.class, 2);
		InvokerTransformer<Object, Integer> invokerTransformer0 = new InvokerTransformer<Object, Integer>((String) null, (Class<?>[]) classArray0, (Object[]) classArray0);
		IndexedCollection<Integer, Object> indexedCollection1 = IndexedCollection.nonUniqueIndexedCollection((Collection<Object>) indexedCollection0, (Transformer<Object, Integer>) invokerTransformer0);
		Integer integer0 = new Integer(11);
		linkedList0.add((Object) integer0);
		boolean boolean0 = indexedCollection1.retainAll(indexedCollection0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
