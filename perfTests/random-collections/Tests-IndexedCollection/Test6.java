package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		Integer integer0 = new Integer(0);
		ConstantFactory<Integer> constantFactory0 = new ConstantFactory<Integer>(integer0);
		FactoryTransformer<Object, Integer> factoryTransformer0 = new FactoryTransformer<Object, Integer>((Factory<? extends Integer>) constantFactory0);
		IndexedCollection<Integer, Object> indexedCollection0 = IndexedCollection.nonUniqueIndexedCollection((Collection<Object>) linkedList0, (Transformer<Object, Integer>) factoryTransformer0);
		LinkedList<String> linkedList1 = new LinkedList<String>();
		linkedList0.add((Object) linkedList1);
		Class<Object>[] classArray0 = (Class<Object>[]) Array.newInstance(Class.class, 2);
		InvokerTransformer<Object, String> invokerTransformer0 = new InvokerTransformer<Object, String>("ExceptionTransformer invoked", (Class<?>[]) classArray0, (Object[]) classArray0);
		// Undeclared exception!
		try {
		IndexedCollection.uniqueIndexedCollection((Collection<Object>) indexedCollection0, (Transformer<Object, String>) invokerTransformer0);
		
		} catch(RuntimeException e) {
		//
		// InvokerTransformer: The method 'ExceptionTransformer invoked' on 'class java.util.LinkedList' does not exist
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
