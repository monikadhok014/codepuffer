package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		Class<Object>[] classArray0 = (Class<Object>[]) Array.newInstance(Class.class, 10);
		InvokerTransformer<String, Object> invokerTransformer0 = new InvokerTransformer<String, Object>("Xo*gG']y", (Class<?>[]) classArray0, (Object[]) classArray0);
		IndexedCollection<Object, String> indexedCollection0 = IndexedCollection.nonUniqueIndexedCollection((Collection<String>) linkedList0, (Transformer<String, Object>) invokerTransformer0);
		// Undeclared exception!
		try {
		indexedCollection0.add("Xo*gG']y");
		
		} catch(RuntimeException e) {
		//
		// InvokerTransformer: The method 'Xo*gG']y' on 'class java.lang.String' does not exist
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
