package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		linkedList0.offerLast("");
		ConstantTransformer<String, Object> constantTransformer0 = new ConstantTransformer<String, Object>((Object) linkedList0);
		IndexedCollection<Object, String> indexedCollection0 = IndexedCollection.uniqueIndexedCollection((Collection<String>) linkedList0, (Transformer<String, Object>) constantTransformer0);
		indexedCollection0.add((String) null);
		Integer integer0 = new Integer((-3817));
		ConstantTransformer<String, Integer> constantTransformer1 = new ConstantTransformer<String, Integer>(integer0);
		// Undeclared exception!
		try {
		IndexedCollection.uniqueIndexedCollection((Collection<String>) linkedList0, (Transformer<String, Integer>) constantTransformer1);
		
		} catch(IllegalArgumentException e) {
		//
		// Duplicate key in uniquely indexed collection.
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
