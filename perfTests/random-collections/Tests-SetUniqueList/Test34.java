package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		SetUniqueList<String> setUniqueList0 = SetUniqueList.setUniqueList((List<String>) linkedList0);
		ListIterator<String> listIterator0 = setUniqueList0.listIterator();
		LinkedHashSet<String> linkedHashSet0 = new LinkedHashSet<String>();
		SetUniqueList.SetListListIterator<String> setUniqueList_SetListListIterator0 = new SetUniqueList.SetListListIterator<String>(listIterator0, linkedHashSet0);
		setUniqueList_SetListListIterator0.add("Op]H`Cmd1@:");
		SetUniqueList.SetListListIterator<String> setUniqueList_SetListListIterator1 = new SetUniqueList.SetListListIterator<String>(setUniqueList_SetListListIterator0, linkedHashSet0);
		String string0 = setUniqueList_SetListListIterator1.previous();

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
