package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Integer> linkedList0 = new LinkedList<Integer>();
		ListIterator<Integer> listIterator0 = linkedList0.listIterator();
		LinkedHashSet<Integer> linkedHashSet0 = new LinkedHashSet<Integer>();
		SetUniqueList.SetListListIterator<Integer> setUniqueList_SetListListIterator0 = new SetUniqueList.SetListListIterator<Integer>(listIterator0, linkedHashSet0);
		// Undeclared exception!
		try {
		setUniqueList_SetListListIterator0.previous();
		
		} catch(NoSuchElementException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
