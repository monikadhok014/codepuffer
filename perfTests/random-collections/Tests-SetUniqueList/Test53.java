package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		HashSet<String> hashSet0 = new HashSet<String>();
		SetUniqueList<String> setUniqueList0 = new SetUniqueList<String>(linkedList0, hashSet0);
		Set<String> set0 = setUniqueList0.asSet();
		SetUniqueList<String> setUniqueList1 = new SetUniqueList<String>(linkedList0, set0);
		// Undeclared exception!
		try {
		setUniqueList1.add("");
		
		} catch(UnsupportedOperationException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
