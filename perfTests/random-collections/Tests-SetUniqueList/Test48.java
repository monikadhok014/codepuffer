package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		LinkedHashSet<Object> linkedHashSet0 = new LinkedHashSet<Object>();
		SetUniqueList<Object> setUniqueList0 = new SetUniqueList<Object>(linkedList0, linkedHashSet0);
		ListIterator<Object> listIterator0 = setUniqueList0.listIterator();
		SetUniqueList.SetListListIterator<Object> setUniqueList_SetListListIterator0 = new SetUniqueList.SetListListIterator<Object>(listIterator0, linkedHashSet0);
		// Undeclared exception!
		try {
		setUniqueList_SetListListIterator0.remove();
		
		} catch(IllegalStateException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
