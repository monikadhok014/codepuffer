package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualTreeBidiMap<Integer, Method> dualTreeBidiMap0 = new DualTreeBidiMap<Integer, Method>();
		DualHashBidiMap<Integer, Method> dualHashBidiMap0 = new DualHashBidiMap<Integer, Method>((Map<? extends Integer, ? extends Method>) dualTreeBidiMap0);
		// Undeclared exception!
		try {
		dualHashBidiMap0.createEntrySetIterator((Iterator<Map.Entry<Integer, Method>>) null);
		
		} catch(NullPointerException e) {
		//
		// Iterator must not be null
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
