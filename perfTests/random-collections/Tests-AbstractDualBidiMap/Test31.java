package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualTreeBidiMap<String, Object> dualTreeBidiMap0 = new DualTreeBidiMap<String, Object>((Comparator<? super String>) null, (Comparator<? super Object>) null);
		DualTreeBidiMap<Object, String> dualTreeBidiMap1 = new DualTreeBidiMap<Object, String>((Comparator<? super Object>) null, (Comparator<? super String>) null);
		DualHashBidiMap<String, Object> dualHashBidiMap0 = new DualHashBidiMap<String, Object>(dualTreeBidiMap0, dualTreeBidiMap1, dualTreeBidiMap1);
		DualLinkedHashBidiMap<String, Object> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<String, Object>(dualHashBidiMap0, dualTreeBidiMap1, dualTreeBidiMap1);
		AbstractDualBidiMap.EntrySet<String, Object> abstractDualBidiMap_EntrySet0 = new AbstractDualBidiMap.EntrySet<String, Object>(dualLinkedHashBidiMap0);
		boolean boolean0 = abstractDualBidiMap_EntrySet0.remove((Object) null);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
