package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualLinkedHashBidiMap<Integer, Integer> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<Integer, Integer>();
		AbstractDualBidiMap.Values<Integer> abstractDualBidiMap_Values0 = new AbstractDualBidiMap.Values<Integer>(dualLinkedHashBidiMap0);
		Iterator<Integer> iterator0 = abstractDualBidiMap_Values0.iterator();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
