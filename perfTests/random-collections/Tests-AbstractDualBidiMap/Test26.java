package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualHashBidiMap<Integer, Method> dualHashBidiMap0 = new DualHashBidiMap<Integer, Method>();
		DualHashBidiMap<Integer, Integer> dualHashBidiMap1 = new DualHashBidiMap<Integer, Integer>();
		AbstractDualBidiMap.Values<Integer> abstractDualBidiMap_Values0 = new AbstractDualBidiMap.Values<Integer>(dualHashBidiMap1);
		boolean boolean0 = abstractDualBidiMap_Values0.remove(dualHashBidiMap0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
