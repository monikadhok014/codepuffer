package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		TreeBidiMap<Integer, Integer> treeBidiMap0 = new TreeBidiMap<Integer, Integer>();
		DualTreeBidiMap<Integer, Object> dualTreeBidiMap0 = new DualTreeBidiMap<Integer, Object>((Map<? extends Integer, ?>) treeBidiMap0);
		DualHashBidiMap<Object, Integer> dualHashBidiMap0 = new DualHashBidiMap<Object, Integer>();
		DualHashBidiMap<Integer, Object> dualHashBidiMap1 = new DualHashBidiMap<Integer, Object>(dualTreeBidiMap0, (Map<Object, Integer>) null, dualHashBidiMap0);
		DualLinkedHashBidiMap<Object, Integer> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<Object, Integer>(dualHashBidiMap0, dualTreeBidiMap0, dualHashBidiMap1);
		AbstractDualBidiMap.BidiMapIterator<Object, Integer> abstractDualBidiMap_BidiMapIterator0 = new AbstractDualBidiMap.BidiMapIterator<Object, Integer>(dualHashBidiMap0);
		AbstractDualBidiMap.KeySetIterator<Object> abstractDualBidiMap_KeySetIterator0 = new AbstractDualBidiMap.KeySetIterator<Object>(abstractDualBidiMap_BidiMapIterator0, dualLinkedHashBidiMap0);
		Iterator<Object> iterator0 = dualLinkedHashBidiMap0.createKeySetIterator(abstractDualBidiMap_KeySetIterator0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
