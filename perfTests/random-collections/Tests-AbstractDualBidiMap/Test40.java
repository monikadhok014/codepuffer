package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualLinkedHashBidiMap<String, Integer> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<String, Integer>();
		DualTreeBidiMap<String, Integer> dualTreeBidiMap0 = new DualTreeBidiMap<String, Integer>((Map<? extends String, ? extends Integer>) dualLinkedHashBidiMap0);
		AbstractDualBidiMap.BidiMapIterator<String, Integer> abstractDualBidiMap_BidiMapIterator0 = new AbstractDualBidiMap.BidiMapIterator<String, Integer>(dualTreeBidiMap0);
		// Undeclared exception!
		try {
		abstractDualBidiMap_BidiMapIterator0.getKey();
		
		} catch(IllegalStateException e) {
		//
		// Iterator getKey() can only be called after next() and before remove()
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
