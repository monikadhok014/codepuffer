package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Integer, String> hashMap0 = new HashMap<Integer, String>(0, 1157.5897F);
		DualLinkedHashBidiMap<Integer, String> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<Integer, String>((Map<? extends Integer, ? extends String>) hashMap0);
		DualTreeBidiMap<Object, String> dualTreeBidiMap0 = new DualTreeBidiMap<Object, String>((Map<?, ? extends String>) dualLinkedHashBidiMap0);
		MapIterator<Object, String> mapIterator0 = dualTreeBidiMap0.mapIterator();
		AbstractDualBidiMap.KeySetIterator<Object> abstractDualBidiMap_KeySetIterator0 = new AbstractDualBidiMap.KeySetIterator<Object>(mapIterator0, dualTreeBidiMap0);
		// Undeclared exception!
		try {
		abstractDualBidiMap_KeySetIterator0.remove();
		
		} catch(IllegalStateException e) {
		//
		// Iterator remove() can only be called once after next()
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
