package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Integer integer0 = new Integer(0);
		AbstractMap.SimpleImmutableEntry<Method, Integer> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<Method, Integer>((Method) null, integer0);
		AbstractMap.SimpleEntry<Object, Integer> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<Object, Integer>((Map.Entry<?, ? extends Integer>) abstractMap_SimpleImmutableEntry0);
		DualHashBidiMap<Integer, Method> dualHashBidiMap0 = new DualHashBidiMap<Integer, Method>();
		DualLinkedHashBidiMap<Integer, Method> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<Integer, Method>((Map<? extends Integer, ? extends Method>) dualHashBidiMap0);
		DualHashBidiMap<Integer, Object> dualHashBidiMap1 = new DualHashBidiMap<Integer, Object>((Map<? extends Integer, ?>) dualLinkedHashBidiMap0);
		BidiMap<Object, Integer> bidiMap0 = dualHashBidiMap1.inverseBidiMap();
		DualHashBidiMap<Object, Integer> dualHashBidiMap2 = new DualHashBidiMap<Object, Integer>(bidiMap0, dualHashBidiMap1, dualHashBidiMap1);
		AbstractDualBidiMap.MapEntry<Object, Integer> abstractDualBidiMap_MapEntry0 = new AbstractDualBidiMap.MapEntry<Object, Integer>(abstractMap_SimpleEntry0, dualHashBidiMap2);
		abstractDualBidiMap_MapEntry0.setValue(integer0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
