package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualLinkedHashBidiMap<Integer, Method> dualLinkedHashBidiMap0 = new DualLinkedHashBidiMap<Integer, Method>();
		DualTreeBidiMap<Integer, Object> dualTreeBidiMap0 = new DualTreeBidiMap<Integer, Object>((Map<? extends Integer, ?>) dualLinkedHashBidiMap0);
		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		ListIterator<Object> listIterator0 = linkedList0.listIterator();
		DualTreeBidiMap<Object, Method> dualTreeBidiMap1 = new DualTreeBidiMap<Object, Method>((Map<?, ? extends Method>) dualLinkedHashBidiMap0);
		AbstractDualBidiMap.KeySetIterator<Object> abstractDualBidiMap_KeySetIterator0 = new AbstractDualBidiMap.KeySetIterator<Object>(listIterator0, dualTreeBidiMap1);
		AbstractDualBidiMap.ValuesIterator<Object> abstractDualBidiMap_ValuesIterator0 = new AbstractDualBidiMap.ValuesIterator<Object>(abstractDualBidiMap_KeySetIterator0, dualTreeBidiMap0);
		// Undeclared exception!
		try {
		abstractDualBidiMap_ValuesIterator0.remove();
		
		} catch(IllegalStateException e) {
		//
		// Iterator remove() can only be called once after next()
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
