package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualTreeBidiMap<Integer, String> dualTreeBidiMap0 = new DualTreeBidiMap<Integer, String>();
		DualTreeBidiMap<Object, Object> dualTreeBidiMap1 = new DualTreeBidiMap<Object, Object>((Map<?, ?>) dualTreeBidiMap0);
		AbstractDualBidiMap.EntrySet<Object, Object> abstractDualBidiMap_EntrySet0 = new AbstractDualBidiMap.EntrySet<Object, Object>(dualTreeBidiMap1);
		Integer integer0 = new Integer(2140);
		AbstractMap.SimpleEntry<Integer, Method> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<Integer, Method>(integer0, (Method) null);
		AbstractMap.SimpleEntry<Integer, Method> abstractMap_SimpleEntry1 = new AbstractMap.SimpleEntry<Integer, Method>((Map.Entry<? extends Integer, ? extends Method>) abstractMap_SimpleEntry0);
		AbstractMap.SimpleImmutableEntry<Object, Method> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<Object, Method>((Map.Entry<?, ? extends Method>) abstractMap_SimpleEntry1);
		boolean boolean0 = abstractDualBidiMap_EntrySet0.remove(abstractMap_SimpleImmutableEntry0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
