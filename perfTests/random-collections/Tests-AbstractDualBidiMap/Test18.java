package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualTreeBidiMap<Object, Object> dualTreeBidiMap0 = new DualTreeBidiMap<Object, Object>((Comparator<? super Object>) null, (Comparator<? super Object>) null);
		AbstractDualBidiMap.EntrySet<Object, Object> abstractDualBidiMap_EntrySet0 = new AbstractDualBidiMap.EntrySet<Object, Object>(dualTreeBidiMap0);
		Object[] objectArray0 = new Object[3];
		boolean boolean0 = abstractDualBidiMap_EntrySet0.equals(objectArray0[0]);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
