package org.apache.commons.collections4.list;
import java.util.Collection;
import org.apache.commons.collections4.list.SetUniqueList;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		org.apache.commons.collections4.list.CursorableLinkedList p13 = new org.apache.commons.collections4.list.CursorableLinkedList();
		for (int i = 0; i < largeNumber * 0.5; i++) {
			 p13.add(i); 
		}

		org.apache.commons.collections4.list.SetUniqueList p14 = org.apache.commons.collections4.list.SetUniqueList.setUniqueList( p13);

		/* Create first parameter. */
		ArrayList p15= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p15.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p14.retainAll( p15);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
