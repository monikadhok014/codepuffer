package org.apache.commons.collections4.collection;
import java.util.Collection;
import org.apache.commons.collections4.collection.IndexedCollection;
import java.util.ArrayList;
public class Test4
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		java.util.ArrayList p16 = new java.util.ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p16.add((int)Math.random()); 
		}

		org.apache.commons.collections4.Transformer p17 = org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();

		org.apache.commons.collections4.map.MultiValueMap p18 = new org.apache.commons.collections4.map.MultiValueMap();

		org.apache.commons.collections4.collection.IndexedCollection p19 =  new org.apache.commons.collections4.collection.IndexedCollection( p16, p17, p18,false);

		/* Create first parameter. */
		ArrayList p20= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p20.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p19.retainAll( p20);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
