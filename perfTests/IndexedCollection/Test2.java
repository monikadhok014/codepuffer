package org.apache.commons.collections4.collection;
import java.util.Collection;
import org.apache.commons.collections4.collection.IndexedCollection;
import java.util.ArrayList;
public class Test2
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		java.util.ArrayList p6 = new java.util.ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p6.add(alpha(i).toString()); 
		}

		org.apache.commons.collections4.Transformer p7 = org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();

		org.apache.commons.collections4.map.MultiValueMap p8 = new org.apache.commons.collections4.map.MultiValueMap();

		org.apache.commons.collections4.collection.IndexedCollection p9 =  new org.apache.commons.collections4.collection.IndexedCollection( p6, p7, p8,false);

		/* Create first parameter. */
		ArrayList p10= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p10.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p9.retainAll( p10);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
