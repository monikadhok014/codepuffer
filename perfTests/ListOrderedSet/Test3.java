package org.apache.commons.collections4.set;
import java.util.Collection;
import org.apache.commons.collections4.set.ListOrderedSet;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.set.ListOrderedSet p5 = new org.apache.commons.collections4.set.ListOrderedSet();
		for (int i = 0; i < largeNumber; i++) {
			 p5.add(new Object()); 
		}

		/* Create first parameter. */
		ArrayList p6= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p6.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p5.retainAll( p6);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
