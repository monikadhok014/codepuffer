package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Maps.Values;
import java.util.ArrayList;
public class Test7
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashMap p4= new java.util.HashMap() ;
		com.google.common.collect.Maps.Values p5= new com.google.common.collect.Maps.Values( p4 ) ;

		/* Create first parameter. */
		ArrayList p6= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p6.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p5.removeAll( p6);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
