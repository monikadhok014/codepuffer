package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Maps.Values;
import java.util.ArrayList;
public class Test9
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashMap p10= new java.util.HashMap() ;
		com.google.common.collect.Maps.Values p11= new com.google.common.collect.Maps.Values( p10 ) ;

		/* Create first parameter. */
		ArrayList p12= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p12.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p11.removeAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
