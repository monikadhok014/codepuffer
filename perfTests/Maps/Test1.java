package com.google.common.collect;
import java.util.Collection;
import com.google.common.collect.Maps.Values;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		java.util.HashMap p1= new java.util.HashMap() ;
		for (int i = 0; i < largeNumber; i++) {
			 p1.put(i, i); 
		}
		com.google.common.collect.Maps.Values p2= new com.google.common.collect.Maps.Values( p1 ) ;

		/* Create first parameter. */
		ArrayList p3= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p3.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p2.retainAll( p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
