package org.apache.lucene.search;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import org.apache.lucene.search.BooleanTopLevelScorers;
import org.apache.lucene.search.ConjunctionScorer;
import org.apache.lucene.search.DisiPriorityQueue;
import org.apache.lucene.search.DisjunctionDISIApproximation;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.FakeScorer;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Vector<Scorer> vector0 = new Vector<Scorer>();
		ConjunctionScorer conjunctionScorer0 = new ConjunctionScorer((Weight) null, vector0, vector0, 0.0F);
		vector0.add((Scorer) conjunctionScorer0);
		ArrayList<Scorer> arrayList0 = new ArrayList<Scorer>();
		ConjunctionScorer conjunctionScorer1 = null;
		try {
		conjunctionScorer1 = new ConjunctionScorer((Weight) null, arrayList0, vector0);
		
		} catch(AssertionError e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
