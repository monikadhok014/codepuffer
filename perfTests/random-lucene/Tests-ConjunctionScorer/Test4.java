package org.apache.lucene.search;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import org.apache.lucene.search.BooleanTopLevelScorers;
import org.apache.lucene.search.ConjunctionScorer;
import org.apache.lucene.search.DisiPriorityQueue;
import org.apache.lucene.search.DisjunctionDISIApproximation;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.FakeScorer;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayList<Scorer> arrayList0 = new ArrayList<Scorer>();
		FakeScorer fakeScorer0 = new FakeScorer();
		arrayList0.add((Scorer) fakeScorer0);
		LinkedList<Scorer> linkedList0 = new LinkedList<Scorer>();
		ConjunctionScorer conjunctionScorer0 = null;
		try {
		conjunctionScorer0 = new ConjunctionScorer((Weight) null, linkedList0, arrayList0, 0.0F);
		
		} catch(AssertionError e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
