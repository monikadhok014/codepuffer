#Generate figure 9

# perf-collections : generated tests are in this folder.
# evosuite/EvoTests or randoop/modifiedTests : store it in scripts/random-collections 
# folder1 : old version 
# folder2 : new version

echo -e "Benchmark\t\"Random tests\"\t\"Generated tests\"" > ../figures/fig9.dat

name="collections"
newversion=../versions/collections/mod/target/classes 
oldversion=../versions/collections/orig/target/classes
perfTests=../perfTests/perf-collections
randomTests=../perfTests/random-collections
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

name="pdfbox"
newversion=../versions/pdfbox/mod/pdfbox/target/classes 
oldversion=../versions/pdfbox/orig/pdfbox/target/classes
perfTests=../perfTests/perf-pdfbox
randomTests=../perfTests/random-pdfbox
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

name="groovy"
newversion=../versions/groovy/mod/target/classes/main 
oldversion=../versions/groovy/orig/target/classes/main
perfTests=../perfTests/perf-groovy
randomTests=../perfTests/random-groovy
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

name="guava"
newversion=../versions/guava/mod/guava/target/classes 
oldversion=../versions/guava/orig/guava/target/classes
perfTests=../perfTests/perf-guava
randomTests=../perfTests/random-guava
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

name="jfree"
newversion=../versions/jfree/mod/target/classes 
oldversion=../versions/jfree/orig/target/classes
perfTests=../perfTests/perf-jfree
randomTests=../perfTests/random-jfree
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

name="ant"
newversion=../versions/ant/mod/build/classes
oldversion=../versions/ant/orig/build/classes
perfTests=../perfTests/perf-ant
randomTests=../perfTests/random-ant
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

name="lucene"
newversion=../versions/lucene/mod/build/core/classes/java
oldversion=../versions/lucene/orig/build/core/classes/java
perfTests=../perfTests/perf-lucene
randomTests=../perfTests/random-lucene
./fig9.sh $newversion $oldversion $perfTests $randomTests $name

