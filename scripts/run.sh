#Summaries are located here!
baseDir="../data"

#place summaries here
loc="../sootAnalysis/src/benchmarks/Summary.txt"
rm -rf ../benchmark/*
reuse=$2 

collections_array=( "org.apache.commons.collections4.bag CollectionBag" 
"org.apache.commons.collections4.bag CollectionSortedBag"
"org.apache.commons.collections4 ListUtils"
"org.apache.commons.collections4.collection IndexedCollection"
"org.apache.commons.collections4 CollectionUtils"
"org.apache.commons.collections4.list SetUniqueList"
"org.apache.commons.collections4.list AbstractLinkedList" 
"org.apache.commons.collections4.bidimap AbstractDualBidiMap" 
"org.apache.commons.collections4.multimap AbstractMultiValuedMap" 
"org.apache.commons.collections4.set ListOrderedSet" )

function reuseTests
{

	if [[ $1 == "groovy.util"  ||  $2 == "COSArrayList" ]]
	then
		cd ../randoop/.
		rm -rf modifiedTests/*
		cp -r $baseDir"/Tests-"$2"/." modifiedTests #folderWithTests 
		./Puffer.sh -b $1 $2 >> $TRACE_FILE 2>&1 # package, class
		rm  $fsehome/benchmark/*.class
		rm  $fsehome/benchmark/*.java
		./Puffer.sh -cde $1 $2 >> $TRACE_FILE 2>&1 # package, class
	else
		cd ../evosuite/.
		rm -rf EvoTests/*
		cp -r $baseDir"/Tests-"$2"/." EvoTests #folderWithTests 
		./Puffer.sh -bcde $1 $2 >> $TRACE_FILE 2>&1 # package, class
	fi
	cp -r $baseDir"/Tests-"$2"/." "../perfTests/random-$3/." #folderWithTests 
	cd ../scripts
}

function reuseSummaries
{
	cp -r $baseDir"/Tests-"$2"" "../perfTests/random-$3/." #folderWithTests 
	cp $baseDir"/Summary-"$2".txt" $loc #name of the summary file
	../evosuite/Puffer.sh -cde $1 $2 >>  $TRACE_FILE 2>&1
}

function run 
{
	echo -e "\t\t\t# Processing $2"
	if [[ $reuse == "" ]]
	then 
		if [[ $2 == "AbstractDualBidiMap" || $2 == "AbstractMultiValuedMap"  ||  $2 == "CategoryPlot"
			||  $1 == "com.google.common.collect" ]] 
		then 
			echo "Generate tests manually!!"
		elif [[ $1 == "groovy.util"  ||  $2 == "COSArrayList" ]]
		then
			cd randoop 
			./Puffer.sh -abc $1 $2 >> $TRACE_FILE 2>&1
			cd ../
		else
			./Puffer.sh -abc $1 $2 >> $TRACE_FILE 2>&1
		fi
		cd evosuite
		./Puffer.sh -abc $1 $2 >> $TRACE_FILE 2>&1
	elif [[ $reuse == *reuseTest* ]]
	then 
		reuseTests $1 $2 $3
	elif [[ $reuse == *reuseSummary* ]]
	then 
		reuseSummaries $1 $2 $3
	fi
}

function startScript
{
	begin=$(date +"%s")
}

function endScript
{
	termin=$(date +"%s")
	difftimelps=$(($termin-$begin))
	#echo "$(($difftimelps / 60)) minutes and $(($difftimelps % 60)) seconds elapsed for Script Execution."
}

function runAll
{
	rm -rf ../perfTests
	mkdir ../perfTests
	echo "" > ../perfTests/TestsOutput.log
	echo "List of benchmarks :  collections, pdfbox, groovy, guava, jfree, ant, lucene"     
	echo -e "100\n 1000\n 10000\n 50000\n 100000"  > ../figures/fig10.dat

	startScript
	./run.sh collections $1
	endScript 
	./fig10.sh ../versions/collections/mod/target/classes ../versions/collections/orig/target/classes collections

	startScript
	./run.sh pdfbox $1
	endScript 
	./fig10.sh ../versions/pdfbox/mod/pdfbox/target/classes ../versions/pdfbox/orig/pdfbox/target/classes pdfbox

	startScript
	./run.sh groovy $1
	endScript 
	./fig10.sh ../versions/groovy/mod/target/classes/main ../versions/groovy/orig/target/classes/main groovy

	startScript
	./run.sh guava $1 
	endScript 
	./fig10.sh ../versions/guava/mod/guava/target/classes ../versions/guava/orig/guava/target/classes guava

	startScript
	./run.sh jfree $1
	endScript 
	./fig10.sh ../versions/jfree/mod/target/classes ../versions/jfree/orig/target/classes jfree

	startScript
	./run.sh ant $1
	endScript
	./fig10.sh ../versions/ant/mod/build/classes ../versions/ant/orig/build/classes ant

	startScript
	./run.sh lucene $1
	endScript 
	./fig10.sh ../versions/lucene/mod/build/core/classes/java ../versions/lucene/orig/build/core/classes/java lucene
}


if [[ $1 == "" ]]
then	
	runAll 
elif [[ $1 == "reuseSummary" ]]
then
	runAll reuseSummary
elif [[ $1 == "reuseTest" ]]
then	
	runAll reuseTest
elif [[ $1 == *collections* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	mkdir -p "../perfTests/random-collections"
	mkdir -p "../perfTests/perf-collections"
	cp -r  ../versions/collections/bin/* ../benchmark/.
	for i in "${collections_array[@]}"
	do
		run $i collections
		folder=`echo $i | cut -d" " -f2`
		mv ../perfTests/all ../perfTests/perf-collections/$folder
	done
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
elif [[ $1 == *pdfbox* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	cp -r ../versions/pdfbox/bin/* ../benchmark/.
	mkdir -p "../perfTests/random-pdfbox"
	mkdir -p "../perfTests/perf-pdfbox"
	run org.apache.pdfbox.pdmodel.common COSArrayList pdfbox 
	mv ../perfTests/all ../perfTests/perf-pdfbox/COSArrayList
	run org.apache.pdfbox.cos COSArray pdfbox 
	mv ../perfTests/all ../perfTests/perf-pdfbox/COSArray
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
elif [[ $1 == *groovy* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	cp -r ../versions/groovy/bin/* ../benchmark/.
	mkdir -p "../perfTests/random-groovy"
	mkdir -p "../perfTests/perf-groovy"
	run groovy.util ObservableList groovy 
	mv ../perfTests/all ../perfTests/perf-groovy/ObservableList
	run groovy.util ObservableSet  groovy 
	mv ../perfTests/all ../perfTests/perf-groovy/ObservableSet
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
elif [[ $1 == *guava* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	cp -r ../versions/guava/bin/* ../benchmark/.
	mkdir -p "../perfTests/random-guava"
	mkdir -p "../perfTests/perf-guava"
	run com.google.common.collect Iterables guava	
	mv ../perfTests/all ../perfTests/perf-guava/Iterables
	run com.google.common.collect Synchronized guava
	mv ../perfTests/all ../perfTests/perf-guava/Synchronized 
	run com.google.common.collect Maps guava 
	mv ../perfTests/all ../perfTests/perf-guava/Maps
	run com.google.common.collect AbstractMapBasedMultimap guava
	mv ../perfTests/all ../perfTests/perf-guava/AbstractMapBasedMultimap
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
elif [[ $1 == *jfree* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	mkdir -p "../perfTests/random-jfree"
	mkdir -p "../perfTests/perf-jfree"
	cp -r ../versions/jfree/bin/* ../benchmark/.
	run org.jfree.chart.plot CategoryPlot jfree
	mv ../perfTests/all ../perfTests/perf-jfree/CategoryPlot
	run org.jfree.data ComparableObjectSeries jfree
	mv ../perfTests/all ../perfTests/perf-jfree/ComparableObjectSeries
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
elif [[ $1 == *ant* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	mkdir -p "../perfTests/random-ant"
	mkdir -p "../perfTests/perf-ant"
	cp -r ../versions/ant/bin/* ../benchmark/.
	run org.apache.tools.ant.util VectorSet ant
	mv ../perfTests/all ../perfTests/perf-ant/VectorSet
	run org.apache.tools.ant.util IdentityStack ant
	mv ../perfTests/all ../perfTests/perf-ant/IdentityStack
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
elif [[ $1 == *lucene* ]]
then
	echo -e "\t\t ************ Test generation for benchmark $1 started!!***************"
	mkdir -p  "../perfTests/random-lucene"
	mkdir -p "../perfTests/perf-lucene"
	cp -r ../versions/lucene/bin/* ../benchmark/.
	run org.apache.lucene.search ConjunctionScorer lucene
	mv ../perfTests/all ../perfTests/perf-lucene/ConjunctionScorer
	echo -e "\t\t ************ Test generation for benchmark $1 is done!!***************"
fi

