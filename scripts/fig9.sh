#!/bin/bash
# ./fig9.sh place1 place2 filesplace
base_lib_path1=$1 #Optimized version where needs to be copied.
base_lib_path2=$2 # unoptimized version where needs to be copied.
declare -A arg_arr_ind
arg_arr_ind=([10000]=0)
arg_arr=(10000)
L=(0)
Ld=(0)
answer=(0)
itr_arr=(0)

Answer=""

function printDebug
{
	if [ $DEBUG_FLAG ]
	then 
		echo -e $1 >> $TRACE_FILE 2>&1
	fi		
}

function process
{
	for file_name in "${files_list[@]}"
	do    
		line=$(head -n 1 "$file_name")
		arr=($line)
		k=${arr[1]::-1}

		target_path=`echo $k | sed -r 's/\./\//g'`
		target_path1=$base_lib_path1/$target_path/
		target_path2=$base_lib_path2/$target_path/

		name=`echo $file_name | rev | cut  -d'/' -f1 | rev`
		o_name=`echo $name | cut -d'.' -f1`
		if grep -F "$o_name" $file_name
		then
			:
		else
			name="Test.java"
			o_name="Test"
		fi
		printDebug "$file_name --"
		if [[ $target_path == *clas* ]]
		then 
			target_path1=$base_lib_path1"/"
		fi
		printDebug "\nOn the modified versions"

		cp "$file_name" $target_path1$name
		#Compile the java code
		cd $base_lib_path1
		if [[ $target_path == *clas* ]]
		then
			javac $name > /dev/null
		else
			javac ./$target_path/$name > /dev/null
		fi
		for i in "${arg_arr[@]}"
		do

			if [[ $target_path == *clas* ]]
			then
				num="$(java $o_name $i)" 
			else
				num="$(java $k.$o_name $i)"
			fi
			if [ $? -ne 0 ] 
			then
				continue;
			fi
			val=`echo $num | cut -d':' -f2`
			printDebug "values is : $val"
			index=${arg_arr_ind[$i]};
			if [ -z "$val" ]; then
				continue;
			fi
			Ld[$index]=$((${Ld[$index]}+$val))
		done
		#Copy the java files to target2
		cd $fsehome/scripts/.   

		printDebug "\nOn the original versions"
		if [[ $target_path == *clas* ]]
		then 
			target_path2=$base_lib_path2"/"
		fi
		cp $file_name $target_path2$name
		cd $base_lib_path2
		if [[ $target_path == *clas* ]]
		then
			javac $name > /dev/null
		else
			javac ./$target_path/$name > /dev/null
		fi
		for i in "${arg_arr[@]}"
		do
			if [[ $target_path == *clas* ]]
			then
				num="$(java $o_name $i)"
			else
				num="$(java $k.$o_name $i)"
			fi
			if [ $? -ne 0 ] 
			then
				continue;
			fi
			val=`echo $num | cut -d':' -f2`
			printDebug "$val"
			index=${arg_arr_ind[$i]};
			if [ -z "$val" ]; then
				continue;
			fi
			L[$index]=$((${L[$index]}+$val))
		done 
		cd $fsehome/scripts/.   
	done

	for i in "${itr_arr[@]}"
	do    
		l=${L[$i]}
		printDebug "l is :  $l"
		ld=${Ld[$i]}
		printDebug "ld is : $ld"
		diff=$((($l-$ld)*100))
		printDebug "diff is :  $diff"
		res=`echo $diff/$l | bc -l`
		val=${res#-}
		val=$( printf "%.0f" $val )
		printDebug "val is :  $val"
		answer[$i]=$val
	done
	printDebug "========================================================================================================="
	printDebug "" 
	printDebug "Answer = ${answer[@]}"
	printDebug ""
	printDebug "========================================================================================================="
}


files=`find $3 -type f`
files_list=($files)
process 
printDebug "Answer = ${answer[@]}"
a=${answer[@]}

L=(0)
Ld=(0)
answer=(0)

files=`find $4 -type f`
files_list=($files)
process 
printDebug "Answer = ${answer[@]}	,$a"
a="${answer[@]}	,$a"
cd $fsehome/scripts
if [[ $5 == *collections* ]]
then 
	echo -e "B1\t$a" >> ../figures/fig9.dat
elif [[ $5 == *pdfbox* ]]
then 
	echo -e "B2\t$a" >> ../figures/fig9.dat
elif [[ $5 == *groovy* ]]
then
	echo -e "B3\t$a" >> ../figures/fig9.dat
elif [[ $5 == *guava* ]]
then
	echo -e "B4\t$a" >> ../figures/fig9.dat
elif [[ $5 == *jfree* ]]
then
	echo -e "B5\t$a" >> ../figures/fig9.dat
elif [[ $5 == *ant* ]]
then
	echo -e "B6\t$a" >> ../figures/fig9.dat
elif [[ $5 == *lucene* ]]
then
	echo -e "B7\t$a" >> ../figures/fig9.dat
fi
