#!/bin/bash
base_lib_path1=$1
base_lib_path2=$2
files=`find ../perfTests/perf-$3 -type f`
files_list=($files)
declare -A arg_arr_ind
arg_arr_ind=([100]=0 [1000]=1 [10000]=2 [50000]=3 [100000]=4)
arg_arr=(100 1000 10000 50000 100000)
#arg_arr_ind=([100]=0 [200]=1 [300]=2 [400]=3 [500]=4)
#arg_arr=(100 200 300 400 500)
L=(0 0 0 0 0)
Ld=(0 0 0 0 0)
answer=(0 0 0 0 0)
itr_arr=(0 1 2 3 4)

function printDebug
{
	if [ $DEBUG_FLAG ]
	then 
		echo -e $1 >> $TRACE_FILE 2>&1
	fi    
}

for file_name in "${files_list[@]}"
do
	printDebug "Processing $file_name"
	line=$(head -n 1 $file_name)
	arr=($line)
	k=${arr[1]::-1}

	target_path=`echo $k | sed -r 's/\./\//g'`
	target_path1=$base_lib_path1/$target_path/
	target_path2=$base_lib_path2/$target_path/

	name=`echo $file_name | rev | cut  -d'/' -f1 | rev`
	o_name=`echo $name | cut -d'.' -f1`
	cp $file_name $target_path1

	#Compile the java code
	cd $base_lib_path1
	javac ./$target_path/$name
	if [ $? -ne 0 ]
	then
		continue;
	fi
	printDebug "./$target_path/$name --"
	printDebug "\nOn the modified versions"
	for i in "${arg_arr[@]}"
	do
		num="$(java $k.$o_name $i)"
		if [ $? -ne 0 ]
		then
			continue;
		fi
		index=${arg_arr_ind[$i]};
		val=`echo $num | cut -d':' -f2`
		printDebug "$i : $val"
		if [ -z "$val" ]; then
			continue;
		fi
		Ld[$index]=$((${Ld[$index]}+$val))
	done
	#Copy the java files to target2
	cd $fsehome/evosuite/.	

	printDebug  "\nOn the original versions"
	cp $file_name $target_path2
	cd $base_lib_path2
	javac ./$target_path/$name
	if [ $? -ne 0 ]
	then
		continue;
	fi
	for i in "${arg_arr[@]}"
	do
		num="$(java $k.$o_name $i)"
		if [ $? -ne 0 ]
		then
			continue;
		fi
		index=${arg_arr_ind[$i]};
		val=`echo $num | cut -d':' -f2`
		printDebug "$i : $val"
		if [ -z "$val" ]; then
			continue;
		fi
		L[$index]=$((${L[$index]}+$val))
	done 
	cd $fsehome/evosuite/.	
done

for i in "${itr_arr[@]}"
do
	l=${L[$i]}
	ld=${Ld[$i]}
	diff=$((($l-$ld)*100))
	res=`echo $diff/$l | bc -l` 
	val=${res#-}
	val=$( printf "%.0f" $val )
	answer[$i]=$val
done
printDebug "======================================================================================================="
printDebug ""
printDebug "Answer = ${answer[@]}"
printDebug ""
cd $fsehome/scripts/.

counter=0
while read line           
do  
	T=`printf "%0.2f\n" ${answer[$counter]}` 
	lines[$counter]="$line	,$T"    
	(( counter++ ))
done <../figures/fig10.dat
rm ../figures/fig10.dat
for i in "${lines[@]}"
do
	printDebug $i
	echo $i >> ../figures/fig10.dat
done

printDebug "========================================================================================================"
