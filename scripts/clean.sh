#iCurrent directory	
pwd
rm -rf out*

#Clean up evosuite
cd ../evosuite
rm -rf EvoTests *ESTest.java out*

#Clean calfuzzer
#cd ../sootAnalysis
#ant -f build.xml cleanall 	
#rm -rf out*
#ant -f build.xml build-tests 	
#cd ../scripts

#Clean benchmark folder
rm -rf ../benchmark/*

#Clean benchmark folder
rm -rf ../perfTests/*

#Remove all tilde files + swap files 
find . -type f -name '*~' -delete
find . -type f -name "*.swp" -exec rm -f {} \;

#figures folder
rm ../figures/fig9.dat
rm ../figures/fig10.dat
