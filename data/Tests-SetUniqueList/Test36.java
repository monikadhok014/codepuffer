package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Integer> linkedList0 = new LinkedList<Integer>();
		SetUniqueList<Integer> setUniqueList0 = SetUniqueList.setUniqueList((List<Integer>) linkedList0);
		Integer integer0 = new Integer(0);
		linkedList0.add(integer0);
		LinkedList<Object> linkedList1 = new LinkedList<Object>();
		SetUniqueList<Object> setUniqueList1 = SetUniqueList.setUniqueList((List<Object>) linkedList1);
		boolean boolean0 = setUniqueList1.containsAll(setUniqueList0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
