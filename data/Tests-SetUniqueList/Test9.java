package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		LinkedHashSet<String> linkedHashSet0 = new LinkedHashSet<String>();
		linkedList0.add(0, "org.apache.commons.collections4.collection.AbstractCollectionDecorator");
		SetUniqueList<String> setUniqueList0 = new SetUniqueList<String>(linkedList0, linkedHashSet0);
		boolean boolean0 = setUniqueList0.addAll(1, (Collection<? extends String>) linkedHashSet0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
