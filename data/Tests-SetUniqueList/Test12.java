package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		linkedList0.add((Object) linkedList0);
		SetUniqueList<Object> setUniqueList0 = SetUniqueList.setUniqueList((List<Object>) linkedList0);
		Set<Object> set0 = setUniqueList0.asSet();
		SetUniqueList<Object> setUniqueList1 = new SetUniqueList<Object>(setUniqueList0, set0);
		// Undeclared exception!
		try {
		SetUniqueList.setUniqueList((List<Object>) setUniqueList1);
		
		} catch(UnsupportedOperationException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
