package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedHashSet<Object> linkedHashSet0 = new LinkedHashSet<Object>();
		Iterator<Object> iterator0 = linkedHashSet0.iterator();
		SetUniqueList.SetListIterator<Object> setUniqueList_SetListIterator0 = new SetUniqueList.SetListIterator<Object>(iterator0, linkedHashSet0);
		// Undeclared exception!
		try {
		setUniqueList_SetListIterator0.remove();
		
		} catch(IllegalStateException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
