package org.apache.commons.collections4.list;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.SetUniqueList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		SetUniqueList<String> setUniqueList0 = null;
		try {
		setUniqueList0 = new SetUniqueList<String>(linkedList0, (Set<String>) null);
		
		} catch(NullPointerException e) {
		//
		// Set must not be null
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
