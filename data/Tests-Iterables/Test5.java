package com.google.common.collect;

import com.google.common.base.Predicate;
import com.google.common.collect.BoundType;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.EmptyContiguousSet;
import com.google.common.collect.EmptyImmutableSortedMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMapEntry;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.RegularImmutableBiMap;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableSortedSet;
import com.google.common.collect.SortedLists;
import java.lang.reflect.Array;
import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.sql.SQLTransientException;
import java.sql.SQLWarning;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Vector;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Object[] objectArray0 = new Object[9];
		Iterable<Object> iterable0 = Iterables.cycle(objectArray0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
