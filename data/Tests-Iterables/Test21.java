package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Iterables;

public class Test {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		List<Integer> one = new ArrayList<Integer>();
			one.add(0);
		List<Integer> two = new ArrayList<Integer>();
			two.add(1);
		
	        Iterables.removeAll(one,two);
		long stop = System.currentTimeMillis();
		System.out.println(" Total time taken : "+ (stop - start));

	}
}
