package com.google.common.collect;

import com.google.common.base.Predicate;
import com.google.common.collect.BoundType;
import com.google.common.collect.EmptyImmutableSet;
import com.google.common.collect.EmptyImmutableSortedMultiset;
import com.google.common.collect.ImmutableMapEntry;
import com.google.common.collect.Iterables;
import com.google.common.collect.MapMaker;
import com.google.common.collect.RegularImmutableAsList;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableMap;
import com.google.common.collect.SingletonImmutableSet;
import com.google.common.collect.SortedLists;
import com.google.common.collect.TreeMultiset;
import java.lang.reflect.Array;
import java.sql.DataTruncation;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLInvalidAuthorizationSpecException;
import java.sql.SQLNonTransientException;
import java.sql.SQLRecoverableException;
import java.sql.SQLTransientException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Iterable<DataTruncation> iterable0 = null;
		int int0 = 0;
		// Undeclared exception!
		try {
		Iterables.get(iterable0, int0);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
