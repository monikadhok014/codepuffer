package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Synchronized;
import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentSkipListSet;
import com.google.common.collect.HashMultimap;

public class Test {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMultimap<Integer, Integer> hashMultiMap = HashMultimap.create();
		ConcurrentSkipListSet<Integer> concurrentSkipList = new ConcurrentSkipListSet<Integer>();
			concurrentSkipList.add(0);
			concurrentSkipList.add(1);

		HashMultimap.WrappedNavigableSet wrappedNavigableSet  = hashMultiMap.new WrappedNavigableSet(null, concurrentSkipList,null);
		
		List<Integer> list = new ArrayList<Integer>();
		list.add(0);
		
	        boolean boolean1 = wrappedNavigableSet.removeAll(list);
		long stop = System.currentTimeMillis();
		System.out.println(" Total time taken : "+ (stop - start));

	}
}
