package com.google.common.collect;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.BoundType;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Multimap;
import com.google.common.collect.SortedLists;
import com.google.common.collect.TreeMultimap;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListMultimap<Object, String> arrayListMultimap0 = ArrayListMultimap.create();
		arrayListMultimap0.trimToSize();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
