package org.apache.commons.collections4.bag;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<String> treeBag0 = new TreeBag<String>();
		CollectionBag<String> collectionBag0 = new CollectionBag<String>((Bag<String>) treeBag0);
		boolean boolean0 = collectionBag0.removeAll(treeBag0);
		Set<String> set0 = collectionBag0.uniqueSet();
		TreeBag<Boolean> treeBag1 = new TreeBag<Boolean>((Comparator<? super Boolean>) null);
		CollectionBag<Boolean> collectionBag1 = new CollectionBag<Boolean>((Bag<Boolean>) treeBag1);
		treeBag0.add("org.apache.commons.collections4.functors.NonePredicate");
		treeBag1.add((Boolean) boolean0);
		// Undeclared exception!
		try {
		collectionBag1.removeAll(set0);
		
		} catch(ClassCastException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
