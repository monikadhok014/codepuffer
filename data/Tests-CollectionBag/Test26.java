package org.apache.commons.collections4.bag;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		HashBag<String> hashBag0 = new HashBag<String>();
		CollectionBag<String> collectionBag0 = new CollectionBag<String>((Bag<String>) hashBag0);
		HashBag<Object> hashBag1 = new HashBag<Object>();
		CollectionBag<Object> collectionBag1 = new CollectionBag<Object>((Bag<Object>) hashBag1);
		hashBag1.add((Object) collectionBag0);
		boolean boolean0 = collectionBag1.retainAll(hashBag0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
