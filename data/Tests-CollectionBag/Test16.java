package org.apache.commons.collections4.bag;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		Class<String> class0 = String.class;
		InstanceofPredicate instanceofPredicate0 = new InstanceofPredicate(class0);
		TreeBag<Integer> treeBag0 = new TreeBag<Integer>();
		CollectionSortedBag<Integer> collectionSortedBag0 = new CollectionSortedBag<Integer>((SortedBag<Integer>) treeBag0);
		Object object0 = new Object();
		SynchronizedSortedBag<Integer> synchronizedSortedBag0 = new SynchronizedSortedBag<Integer>(collectionSortedBag0, object0);
		PredicatedSortedBag<Integer> predicatedSortedBag0 = new PredicatedSortedBag<Integer>(synchronizedSortedBag0, instanceofPredicate0);
		CollectionBag<Integer> collectionBag0 = new CollectionBag<Integer>((Bag<Integer>) predicatedSortedBag0);
		// Undeclared exception!
		try {
		collectionBag0.add((Integer) (-866), (-866));
		
		} catch(IllegalArgumentException e) {
		//
		// Cannot add Object '-866' - Predicate 'org.apache.commons.collections4.functors.InstanceofPredicate@1' rejected it
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
