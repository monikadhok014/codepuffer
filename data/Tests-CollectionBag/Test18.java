package org.apache.commons.collections4.bag;
import java.util.Collection;
import org.apache.commons.collections4.bag.CollectionBag;
import java.util.ArrayList;
public class Test
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		org.apache.commons.collections4.bag.TreeBag p1 = new org.apache.commons.collections4.bag.TreeBag();
		for (int i = 0; i < 30000; i++) 
			 p1.add(i); 
		org.apache.commons.collections4.bag.CollectionBag p2 =  new org.apache.commons.collections4.bag.CollectionBag( p1);
		ArrayList p3= new ArrayList();
		for (int i = 0; i < 30000; i++) 
			 p3.add(i); 
		long start = System.currentTimeMillis();
		 p2.retainAll( p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
