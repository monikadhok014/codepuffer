package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		ListOrderedSet<Object> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<Object>) linkedList0);
		linkedList0.add((Object) null);
		Integer integer0 = new Integer(0);
		boolean boolean0 = listOrderedSet0.addAll((int) integer0, (Collection<?>) listOrderedSet0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
