package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<String> linkedList0 = new LinkedList<String>();
		linkedList0.add((String) null);
		linkedList0.add("Collection must not be null.");
		ListOrderedSet<String> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<String>) linkedList0);
		linkedList0.pop();
		linkedList0.add((String) null);
		String string0 = listOrderedSet0.remove(1);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
