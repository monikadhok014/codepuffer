package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		ListOrderedSet<Object> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<Object>) linkedList0);
		linkedList0.add((Object) null);
		ListOrderedSet<Object> listOrderedSet1 = new ListOrderedSet<Object>(listOrderedSet0, linkedList0);
		Integer integer0 = new Integer(0);
		// Undeclared exception!
		try {
		listOrderedSet1.addAll((int) integer0, (Collection<?>) listOrderedSet0);
		
		} catch(ConcurrentModificationException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
