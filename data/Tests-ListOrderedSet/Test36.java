package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		Integer integer0 = new Integer(2825);
		LinkedList<Integer> linkedList0 = new LinkedList<Integer>();
		linkedList0.add(integer0);
		ListOrderedSet<Integer> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<Integer>) linkedList0);
		Integer integer1 = listOrderedSet0.remove(0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
