package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedHashSet<String> linkedHashSet0 = new LinkedHashSet<String>();
		LinkedList<String> linkedList0 = new LinkedList<String>();
		ListOrderedSet<String> listOrderedSet0 = new ListOrderedSet<String>(linkedHashSet0, linkedList0);
		linkedList0.offer("Set must not be null");
		int int0 = listOrderedSet0.indexOf("Set must not be null");

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
