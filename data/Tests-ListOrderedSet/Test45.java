package org.apache.commons.collections4.set;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.OrderedIterator;
import org.apache.commons.collections4.set.ListOrderedSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		ListOrderedSet<Object> listOrderedSet0 = ListOrderedSet.listOrderedSet((List<Object>) linkedList0);
		List<Object> list0 = listOrderedSet0.asList();
		ListOrderedSet<Object> listOrderedSet1 = ListOrderedSet.listOrderedSet(list0);
		// Undeclared exception!
		try {
		listOrderedSet1.add((Object) listOrderedSet1);
		
		} catch(UnsupportedOperationException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
