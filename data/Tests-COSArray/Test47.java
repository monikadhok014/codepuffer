package org.apache.pdfbox.cos;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdfwriter.COSWriter;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.function.PDFunctionType2;
import org.apache.pdfbox.pdmodel.common.function.PDFunctionType3;
import org.apache.pdfbox.pdmodel.graphics.PDExtendedGraphicsState;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		COSArray cOSArray0 = new COSArray();
		PDFunctionType3 pDFunctionType3_0 = new PDFunctionType3((COSBase) cOSArray0);
		cOSArray0.add((COSObjectable) pDFunctionType3_0);
		COSName cOSName0 = COSName.CAP_HEIGHT;
		boolean boolean0 = cOSArray0.removeObject(cOSName0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
