package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DualHashBidiMap<Integer, Integer> dualHashBidiMap0 = new DualHashBidiMap<Integer, Integer>();
		TreeBidiMap<Integer, String> treeBidiMap0 = new TreeBidiMap<Integer, String>();
		DualHashBidiMap<String, Integer> dualHashBidiMap1 = new DualHashBidiMap<String, Integer>((Map<String, Integer>) null, treeBidiMap0, treeBidiMap0);
		// Undeclared exception!
		try {
		dualHashBidiMap1.remove((Object) dualHashBidiMap0);
		
		} catch(NullPointerException e) {
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
