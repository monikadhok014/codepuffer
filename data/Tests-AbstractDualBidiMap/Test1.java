package org.apache.commons.collections4.bidimap;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import org.apache.commons.collections4.collection.SynchronizedCollection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.*;

public class Test {
	public static void main(String[] args) {

		long start = System.currentTimeMillis();
		HashMap hashMap = new HashMap();
		hashMap.put(0, Math.random());
		hashMap.put(1, Math.random());
                DualTreeBidiMap d = new DualTreeBidiMap(hashMap);
		AbstractDualBidiMap.EntrySet abstractDualBidiMap_EntrySet0 = new AbstractDualBidiMap.EntrySet(d);

		HashSet<Integer> set = new HashSet<Integer>();
		set.add(0);
	
		boolean boolean0 = abstractDualBidiMap_EntrySet0.retainAll(set);
		long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
	}
}
