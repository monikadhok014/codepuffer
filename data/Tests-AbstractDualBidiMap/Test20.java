package org.apache.commons.collections4.bidimap;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		TreeBidiMap<Integer, Integer> treeBidiMap0 = new TreeBidiMap<Integer, Integer>();
		Integer integer0 = new Integer(1);
		treeBidiMap0.put((Integer) (-1684), integer0);
		DualTreeBidiMap<Object, Integer> dualTreeBidiMap0 = new DualTreeBidiMap<Object, Integer>((Map<?, ? extends Integer>) treeBidiMap0);
		boolean boolean0 = dualTreeBidiMap0.isEmpty();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
