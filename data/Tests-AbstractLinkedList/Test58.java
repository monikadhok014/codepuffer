package org.apache.commons.collections4.list;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.SequenceInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.collections4.list.AbstractLinkedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		CursorableLinkedList<PipedInputStream> cursorableLinkedList0 = new CursorableLinkedList<PipedInputStream>();
		AbstractLinkedList.LinkedSubList<PipedInputStream> abstractLinkedList_LinkedSubList0 = new AbstractLinkedList.LinkedSubList<PipedInputStream>(cursorableLinkedList0, 0, 0);
		// Undeclared exception!
		try {
		abstractLinkedList_LinkedSubList0.subList(1, 0);
		
		} catch(IllegalArgumentException e) {
		//
		// fromIndex(1) > toIndex(0)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
