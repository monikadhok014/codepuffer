package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Collection<Integer>, String> hashMap0 = new HashMap<Collection<Integer>, String>();
		HashSetValuedHashMap<Collection<Integer>, Object> hashSetValuedHashMap0 = new HashSetValuedHashMap<Collection<Integer>, Object>((Map<? extends Collection<Integer>, ?>) hashMap0);
		hashSetValuedHashMap0.size();
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Method>(0);
		boolean boolean0 = hashSetValuedHashMap1.containsValue(hashSetValuedHashMap0);
		
		HashMap<Collection<Integer>, Method> hashMap1 = new HashMap<Collection<Integer>, Method>();
		HashSetValuedHashMap<Collection<Integer>, Method> hashSetValuedHashMap2 = new HashSetValuedHashMap<Collection<Integer>, Method>((Map<? extends Collection<Integer>, ? extends Method>) hashMap1);
		int int0 = hashSetValuedHashMap2.size();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
