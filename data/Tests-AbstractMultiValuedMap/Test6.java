package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Integer, Method> hashMap0 = new HashMap<Integer, Method>();
		HashSetValuedHashMap<Object, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Method>((Map<?, ? extends Method>) hashMap0);
		hashSetValuedHashMap0.get(hashSetValuedHashMap0);
		hashSetValuedHashMap0.keySet();
		HashSetValuedHashMap<Integer, Method> hashSetValuedHashMap1 = new HashSetValuedHashMap<Integer, Method>();
		hashMap0.put((Integer) null, (Method) null);
		hashSetValuedHashMap0.size();
		hashSetValuedHashMap1.putAll((Map<? extends Integer, ? extends Method>) hashMap0);
		hashSetValuedHashMap0.values();
		hashSetValuedHashMap0.createCollection();
		hashSetValuedHashMap1.containsMapping(hashSetValuedHashMap0, (Object) null);
		HashSetValuedHashMap<String, String> hashSetValuedHashMap2 = new HashSetValuedHashMap<String, String>(0);
		hashSetValuedHashMap2.clear();
		hashSetValuedHashMap2.put("", "i,$&D%");
		// Undeclared exception!
		try {
		hashSetValuedHashMap2.putAll((Map<? extends String, ? extends String>) null);
		
		} catch(NullPointerException e) {
		//
		// Map must not be null.
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
