package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListValuedHashMap<Integer, Object> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, Object>();
		HashSetValuedHashMap<Object, Collection<Integer>> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Collection<Integer>>(0);
		ArrayListValuedHashMap<Method, String> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Method, String>();
		arrayListValuedHashMap1.values();
		arrayListValuedHashMap1.remove((Object) null);
		HashSetValuedHashMap<Object, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Object, Object>((MultiValuedMap<?, ?>) arrayListValuedHashMap0);
		hashSetValuedHashMap1.size();
		HashSetValuedHashMap<String, Method> hashSetValuedHashMap2 = new HashSetValuedHashMap<String, Method>();
		hashSetValuedHashMap2.getMap();
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap3 = new HashSetValuedHashMap<Method, Object>(0, 0);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap3.new WrappedCollection((Method) null);
		abstractMultiValuedMap_WrappedCollection0.iterator();
		ArrayListValuedHashMap<Integer, Method> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Integer, Method>(0, 0);
		arrayListValuedHashMap2.remove((Object) null);
		HashSetValuedHashMap<String, Object> hashSetValuedHashMap4 = new HashSetValuedHashMap<String, Object>();
		// Undeclared exception!
		try {
		arrayListValuedHashMap2.putAll((MultiValuedMap<? extends Integer, ? extends Method>) null);
		
		} catch(NullPointerException e) {
		//
		// Map must not be null.
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
