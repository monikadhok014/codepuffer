package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListValuedHashMap<Method, Method> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, Method>(0);
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Method, Method>((MultiValuedMap<? extends Method, ? extends Method>) arrayListValuedHashMap0);
		Map<Method, Collection<Method>> map0 = hashSetValuedHashMap0.asMap();
		hashSetValuedHashMap0.entries();
		ArrayListValuedHashMap<Method, Collection<Integer>> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Method, Collection<Integer>>((-931));
		Collection<Collection<Integer>> collection0 = arrayListValuedHashMap1.remove(map0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
