package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap0 = new HashSetValuedHashMap<Method, Integer>(0);
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Integer>((MultiValuedMap<? extends Method, ? extends Integer>) hashSetValuedHashMap0);
		hashSetValuedHashMap1.entries();
		ArrayListValuedHashMap<Integer, Method> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, Method>(0);
		Integer integer0 = new Integer(0);
		arrayListValuedHashMap0.put(integer0, (Method) null);
		ArrayListValuedHashMap<Method, String> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Method, String>(1592, 0);
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap2 = new HashSetValuedHashMap<Method, Method>(0);
		Collection<Method> collection0 = hashSetValuedHashMap2.wrappedCollection((Method) null);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
