package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListValuedHashMap<Method, String> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, String>();
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, String>(1323, 1323);
		Vector<Locale.LanguageRange> vector0 = new Vector<Locale.LanguageRange>();
		HashSet<String> hashSet0 = new HashSet<String>();
		boolean boolean0 = vector0.add((Locale.LanguageRange) null);
		List<String> list0 = Locale.filterTags((List<Locale.LanguageRange>) vector0, (Collection<String>) hashSet0);
		hashSetValuedHashMap0.wrappedCollection(list0);
		ArrayListValuedHashMap<String, String> arrayListValuedHashMap1 = new ArrayListValuedHashMap<String, String>(1323);
		HashSetValuedHashMap<String, String> hashSetValuedHashMap1 = new HashSetValuedHashMap<String, String>((MultiValuedMap<? extends String, ? extends String>) arrayListValuedHashMap1);
		hashSetValuedHashMap1.get("The list of items must not be null");
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap2 = new HashSetValuedHashMap<Method, Method>(1323);
		hashSetValuedHashMap2.mapIterator();
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap0.new WrappedCollection((Method) null);
		List<String> list1 = arrayListValuedHashMap0.wrappedCollection((Method) null);
		boolean boolean1 = abstractMultiValuedMap_WrappedCollection0.addAll(list1);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
