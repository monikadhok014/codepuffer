package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<String, Collection<Integer>> hashMap0 = new HashMap<String, Collection<Integer>>();
		ArrayListValuedHashMap<String, Collection<Integer>> arrayListValuedHashMap0 = new ArrayListValuedHashMap<String, Collection<Integer>>((Map<? extends String, ? extends Collection<Integer>>) hashMap0);
		arrayListValuedHashMap0.clear();
		PriorityQueue<Integer> priorityQueue0 = new PriorityQueue<Integer>();
		hashMap0.put("fhb3j;hoF~o4ZWw(#n`", priorityQueue0);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap0.new WrappedCollection((String) null);
		abstractMultiValuedMap_WrappedCollection0.clear();
		ArrayListValuedHashMap<Integer, String> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Integer, String>(1);
		arrayListValuedHashMap1.values();
		ArrayListValuedHashMap<Integer, Object> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Integer, Object>(2366);
		HashMap<Integer, Method> hashMap1 = new HashMap<Integer, Method>();
		HashSetValuedHashMap<Object, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Method>((Map<?, ? extends Method>) hashMap1);
		ArrayListValuedHashMap<Collection<Integer>, Method> arrayListValuedHashMap3 = new ArrayListValuedHashMap<Collection<Integer>, Method>(0);
		arrayListValuedHashMap3.clear();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
