package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Method, Method> hashMap0 = new HashMap<Method, Method>();
		HashSetValuedHashMap<Method, Method> hashSetValuedHashMap0 = new HashSetValuedHashMap<Method, Method>((Map<? extends Method, ? extends Method>) hashMap0);
		hashMap0.put((Method) null, (Method) null);
		hashSetValuedHashMap0.createCollection();
		hashSetValuedHashMap0.putAll((Map<? extends Method, ? extends Method>) hashMap0);
		ArrayListValuedHashMap<Method, Object> arrayListValuedHashMap0 = null;
		try {
		arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, Object>((-2035), (-1660));
		
		} catch(IllegalArgumentException e) {
		//
		// Illegal initial capacity: -2035
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
