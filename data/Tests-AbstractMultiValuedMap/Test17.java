package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashSetValuedHashMap<Method, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Method, String>(3800, 2320);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap0.new WrappedCollection((Method) null);
		Spliterator<String> spliterator0 = abstractMultiValuedMap_WrappedCollection0.spliterator();
		ArrayListValuedHashMap<Object, String> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Object, String>((MultiValuedMap<?, ? extends String>) hashSetValuedHashMap0);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection1 = arrayListValuedHashMap0.new WrappedCollection((Object) spliterator0);
		boolean boolean0 = abstractMultiValuedMap_WrappedCollection1.addAll(abstractMultiValuedMap_WrappedCollection0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
