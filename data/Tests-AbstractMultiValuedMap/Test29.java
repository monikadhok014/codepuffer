package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Method, Integer> hashMap0 = new HashMap<Method, Integer>();
		Integer integer0 = new Integer(26);
		hashMap0.put((Method) null, integer0);
		HashSetValuedHashMap<Object, Integer> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, Integer>((Map<?, ? extends Integer>) hashMap0);
		hashSetValuedHashMap0.mapIterator();
		
		ArrayListValuedHashMap<Method, Integer> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Method, Integer>((Map<? extends Method, ? extends Integer>) hashMap0);
		ArrayListValuedHashMap<Method, Integer> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Method, Integer>((MultiValuedMap<? extends Method, ? extends Integer>) arrayListValuedHashMap0);
		arrayListValuedHashMap1.isEmpty();
		hashSetValuedHashMap0.clear();
		
		HashSetValuedHashMap<Method, Object> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Object>((int) integer0);
		hashSetValuedHashMap1.keys();
		ArrayListValuedHashMap<Object, Method> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Object, Method>();
		HashMap<String, Method> hashMap1 = new HashMap<String, Method>();
		arrayListValuedHashMap2.putAll((Map<?, ? extends Method>) hashMap1);
		ArrayListValuedHashMap<String, Object> arrayListValuedHashMap3 = new ArrayListValuedHashMap<String, Object>((Map<? extends String, ?>) hashMap1);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap3.new WrappedCollection("");
		abstractMultiValuedMap_WrappedCollection0.contains(hashSetValuedHashMap1);
		ArrayListValuedHashMap<Integer, String> arrayListValuedHashMap4 = new ArrayListValuedHashMap<Integer, String>();
		ArrayListValuedHashMap<Integer, Object> arrayListValuedHashMap5 = new ArrayListValuedHashMap<Integer, Object>((MultiValuedMap<? extends Integer, ?>) arrayListValuedHashMap4);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
