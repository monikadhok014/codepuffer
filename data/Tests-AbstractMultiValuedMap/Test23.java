package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Integer, Method> hashMap0 = new HashMap<Integer, Method>();
		ArrayListValuedHashMap<Integer, Method> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, Method>((Map<? extends Integer, ? extends Method>) hashMap0);
		HashMap<Integer, String> hashMap1 = new HashMap<Integer, String>();
		ArrayListValuedHashMap<Integer, String> arrayListValuedHashMap1 = new ArrayListValuedHashMap<Integer, String>((Map<? extends Integer, ? extends String>) hashMap1);
		HashSetValuedHashMap<Integer, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Integer, String>((MultiValuedMap<? extends Integer, ? extends String>) arrayListValuedHashMap1);
		arrayListValuedHashMap1.entries();
		Integer integer0 = new Integer(44);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = hashSetValuedHashMap0.new WrappedCollection(integer0);
		// Undeclared exception!
		try {
		abstractMultiValuedMap_WrappedCollection0.removeIf((java.util.function.Predicate<? super String>) null);
		
		} catch(NullPointerException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
