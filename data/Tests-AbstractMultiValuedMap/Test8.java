package org.apache.commons.collections4.multimap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayListValuedHashMap<Integer, Method> arrayListValuedHashMap0 = new ArrayListValuedHashMap<Integer, Method>();
		arrayListValuedHashMap0.keySet();
		arrayListValuedHashMap0.entries();
		Integer integer0 = new Integer((-4198));
		arrayListValuedHashMap0.put(integer0, (Method) null);
		Integer integer1 = new Integer((-4198));
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection0 = arrayListValuedHashMap0.new WrappedCollection(integer1);
		Object[] objectArray0 = abstractMultiValuedMap_WrappedCollection0.toArray();
		ArrayListValuedHashMap<String, String> arrayListValuedHashMap1 = new ArrayListValuedHashMap<String, String>(128);
		arrayListValuedHashMap1.clear();
		arrayListValuedHashMap1.keys();
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection1 = arrayListValuedHashMap1.new WrappedCollection("H;Y");
		abstractMultiValuedMap_WrappedCollection1.toArray(objectArray0);
		ArrayListValuedHashMap<Method, Object> arrayListValuedHashMap2 = new ArrayListValuedHashMap<Method, Object>((int) integer1);
		AbstractMultiValuedMap.WrappedCollection abstractMultiValuedMap_WrappedCollection2 = arrayListValuedHashMap2.new WrappedCollection((Method) null);
		abstractMultiValuedMap_WrappedCollection2.spliterator();
		HashSetValuedHashMap<Object, String> hashSetValuedHashMap0 = new HashSetValuedHashMap<Object, String>();
		hashSetValuedHashMap0.containsKey(abstractMultiValuedMap_WrappedCollection2);
		HashMap<Method, Integer> hashMap0 = new HashMap<Method, Integer>();
		ArrayListValuedHashMap<Method, Integer> arrayListValuedHashMap3 = new ArrayListValuedHashMap<Method, Integer>((Map<? extends Method, ? extends Integer>) hashMap0);
		HashSetValuedHashMap<Method, Integer> hashSetValuedHashMap1 = new HashSetValuedHashMap<Method, Integer>((MultiValuedMap<? extends Method, ? extends Integer>) arrayListValuedHashMap3);
		hashSetValuedHashMap1.containsKey((Object) null);
		ArrayListValuedHashMap<Integer, Integer> arrayListValuedHashMap4 = new ArrayListValuedHashMap<Integer, Integer>((-426));
		arrayListValuedHashMap1.equals((Object) null);
		ArrayListValuedHashMap<Object, Object> arrayListValuedHashMap5 = null;
		try {
		arrayListValuedHashMap5 = new ArrayListValuedHashMap<Object, Object>((-4198), (-179));
		
		} catch(IllegalArgumentException e) {
		//
		// Illegal initial capacity: -4198
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
