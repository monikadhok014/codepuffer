package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import com.google.common.collect.AbstractMapBasedMultimap;

public class Test {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
		hashMap.put(0,(int)Math.random());
		hashMap.put(1,(int)Math.random());

		Maps.Values maps_values = new Maps.Values(hashMap);
		List<Integer> list = new ArrayList<Integer>();
		list.add(0);
	        boolean boolean1 =  maps_values.retainAll(list);
		long stop = System.currentTimeMillis();
		System.out.println(" Total time taken : "+ (stop - start));
	}
}
