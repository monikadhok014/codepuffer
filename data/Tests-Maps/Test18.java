package com.google.common.collect;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.BoundType;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Maps;
import com.google.common.collect.RegularImmutableBiMap;
import com.google.common.collect.SingletonImmutableBiMap;
import com.google.common.collect.SortedLists;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.ConcurrentMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Maps.UnmodifiableEntrySet<SortedLists.KeyPresentBehavior, MapMaker.RemovalCause> maps_UnmodifiableEntrySet0 = new Maps.UnmodifiableEntrySet<SortedLists.KeyPresentBehavior, MapMaker.RemovalCause>((Set<Map.Entry<SortedLists.KeyPresentBehavior, MapMaker.RemovalCause>>) null);
		String string0 = "%@\|TI-gz{/<&?~";
		Method method0 = null;
		AbstractMap.SimpleEntry<String, Method> abstractMap_SimpleEntry0 = new AbstractMap.SimpleEntry<String, Method>(string0, method0);
		AbstractMap.SimpleImmutableEntry<String, Method> abstractMap_SimpleImmutableEntry0 = new AbstractMap.SimpleImmutableEntry<String, Method>((Map.Entry<? extends String, ? extends Method>) abstractMap_SimpleEntry0);
		SingletonImmutableBiMap<String, Method> singletonImmutableBiMap0 = null;
		try {
		singletonImmutableBiMap0 = new SingletonImmutableBiMap<String, Method>(abstractMap_SimpleImmutableEntry0);
		
		} catch(NullPointerException e) {
		//
		// null value in entry: %@\|TI-gz{/<&?~=null
		//
		}
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
