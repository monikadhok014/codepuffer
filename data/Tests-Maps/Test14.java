package com.google.common.collect;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.BoundType;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Maps;
import com.google.common.collect.RegularImmutableBiMap;
import com.google.common.collect.SingletonImmutableBiMap;
import com.google.common.collect.SortedLists;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.ConcurrentMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		SortedLists.KeyAbsentBehavior sortedLists_KeyAbsentBehavior0 = SortedLists.KeyAbsentBehavior.INVERTED_INSERTION_INDEX;
		MapMaker.RemovalCause mapMaker_RemovalCause0 = MapMaker.RemovalCause.EXPLICIT;
		Map.Entry<SortedLists.KeyAbsentBehavior, MapMaker.RemovalCause> map_Entry0 = Maps.immutableEntry(sortedLists_KeyAbsentBehavior0, mapMaker_RemovalCause0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
