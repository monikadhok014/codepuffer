package org.apache.tools.ant.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.apache.tools.ant.util.VectorSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		VectorSet vectorSet0 = new VectorSet();
		Object object0 = new Object();
		vectorSet0.add(0, object0);
		
		Object object1 = new Object();
		Object object2 = vectorSet0.set(0, object1);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
