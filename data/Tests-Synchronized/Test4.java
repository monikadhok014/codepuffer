package com.google.common.collect;

import com.google.common.collect.BoundType;
import com.google.common.collect.DescendingImmutableSortedMultiset;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.EmptyContiguousSet;
import com.google.common.collect.EmptyImmutableListMultimap;
import com.google.common.collect.EmptyImmutableSetMultimap;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Multiset;
import com.google.common.collect.RegularImmutableSortedMultiset;
import com.google.common.collect.RegularImmutableSortedSet;
import com.google.common.collect.SortedLists;
import com.google.common.collect.Synchronized;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		RegularImmutableSortedSet<Object> regularImmutableSortedSet0 = null;
		int[] intArray0 = new int[9];
		long[] longArray0 = new long[8];
		RegularImmutableSortedMultiset<Object> regularImmutableSortedMultiset0 = new RegularImmutableSortedMultiset<Object>((RegularImmutableSortedSet<Object>) null, intArray0, longArray0, 2178, 0);
		DescendingImmutableSortedMultiset<Object> descendingImmutableSortedMultiset0 = new DescendingImmutableSortedMultiset<Object>(regularImmutableSortedMultiset0);
		Multiset<Object> multiset0 = Synchronized.multiset((Multiset<Object>) descendingImmutableSortedMultiset0, (Object) regularImmutableSortedSet0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
