package com.google.common.collect;

import com.google.common.collect.BoundType;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.EmptyContiguousSet;
import com.google.common.collect.EmptyImmutableBiMap;
import com.google.common.collect.EmptyImmutableListMultimap;
import com.google.common.collect.MapMaker;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableSortedMultiset;
import com.google.common.collect.RegularImmutableSortedSet;
import com.google.common.collect.Synchronized;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		Locale locale0 = Locale.UK;
		Set<String> set0 = locale0.getUnicodeLocaleAttributes();
		Set<String> set1 = Synchronized.set(set0, (Object) set0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
