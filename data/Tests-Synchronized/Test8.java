package com.google.common.collect;

import com.google.common.collect.BoundType;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.EmptyContiguousSet;
import com.google.common.collect.EmptyImmutableBiMap;
import com.google.common.collect.EmptyImmutableListMultimap;
import com.google.common.collect.MapMaker;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableSortedMultiset;
import com.google.common.collect.RegularImmutableSortedSet;
import com.google.common.collect.Synchronized;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		PriorityQueue<Object> priorityQueue0 = new PriorityQueue<Object>();
		Object[] objectArray0 = new Object[3];
		RegularImmutableList<Integer> regularImmutableList0 = new RegularImmutableList<Integer>(objectArray0);
		Comparator<Integer> comparator0 = null;
		RegularImmutableSortedSet<Integer> regularImmutableSortedSet0 = new RegularImmutableSortedSet<Integer>(regularImmutableList0, comparator0);
		int[] intArray0 = new int[10];
		int int0 = (-1491);
		long[] longArray0 = new long[8];
		RegularImmutableSortedMultiset<Integer> regularImmutableSortedMultiset0 = new RegularImmutableSortedMultiset<Integer>(regularImmutableSortedSet0, intArray0, longArray0, intArray0[1], int0);
		Queue<Object> queue0 = Synchronized.queue((Queue<Object>) priorityQueue0, (Object) regularImmutableSortedMultiset0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
