package org.apache.commons.collections4.bag;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NullIsFalsePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<Integer> treeBag0 = new TreeBag<Integer>();
		treeBag0.add((Integer) 1);
		CollectionSortedBag<Integer> collectionSortedBag0 = new CollectionSortedBag<Integer>((SortedBag<Integer>) treeBag0);
		Set<Integer> set0 = collectionSortedBag0.uniqueSet();
		TreeBag<Object> treeBag1 = new TreeBag<Object>((Collection<?>) set0);
		CollectionSortedBag<Object> collectionSortedBag1 = new CollectionSortedBag<Object>((SortedBag<Object>) treeBag1);
		collectionSortedBag1.removeAll(treeBag1);
		boolean boolean0 = collectionSortedBag1.containsAll(collectionSortedBag0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
