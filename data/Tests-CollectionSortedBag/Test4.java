package org.apache.commons.collections4.bag;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NullIsFalsePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<String> treeBag0 = new TreeBag<String>();
		treeBag0.add("Z"F}2&|"k8EFy{o");
		treeBag0.add("Z"F}2&|"k8EFy{o");
		CollectionSortedBag<String> collectionSortedBag0 = new CollectionSortedBag<String>((SortedBag<String>) treeBag0);
		// Undeclared exception!
		try {
		collectionSortedBag0.removeAll(treeBag0);
		
		} catch(ConcurrentModificationException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
