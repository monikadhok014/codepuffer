package org.apache.commons.collections4.bag;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NullIsFalsePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		HashBag<String> hashBag0 = new HashBag<String>();
		TreeBag<String> treeBag0 = new TreeBag<String>((Collection<? extends String>) hashBag0);
		Class<Boolean> class0 = Boolean.class;
		Predicate<Object> predicate0 = InstanceofPredicate.instanceOfPredicate(class0);
		PredicatedSortedBag<String> predicatedSortedBag0 = new PredicatedSortedBag<String>(treeBag0, predicate0);
		CollectionSortedBag<String> collectionSortedBag0 = new CollectionSortedBag<String>((SortedBag<String>) predicatedSortedBag0);
		// Undeclared exception!
		try {
		collectionSortedBag0.add("j^J");
		
		} catch(IllegalArgumentException e) {
		//
		// Cannot add Object 'j^J' - Predicate 'org.apache.commons.collections4.functors.InstanceofPredicate@4' rejected it
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
