package org.apache.commons.collections4.bag;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NullIsFalsePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<Object> treeBag0 = new TreeBag<Object>((Comparator<? super Object>) null);
		Class<Object>[] classArray0 = (Class<Object>[]) Array.newInstance(Class.class, 4);
		Class<Object> class0 = Object.class;
		InvokerTransformer<Object, Boolean> invokerTransformer0 = new InvokerTransformer<Object, Boolean>("<9 ]w"5ipjZ_>", (Class<?>[]) classArray0, (Object[]) classArray0);
		TransformedSortedBag<Object> transformedSortedBag0 = new TransformedSortedBag<Object>(treeBag0, invokerTransformer0);
		CollectionSortedBag<Object> collectionSortedBag0 = new CollectionSortedBag<Object>((SortedBag<Object>) transformedSortedBag0);
		// Undeclared exception!
		try {
		collectionSortedBag0.add((Object) class0);
		
		} catch(RuntimeException e) {
		//
		// InvokerTransformer: The method '<9 ]w"5ipjZ_>' on 'class java.lang.Class' does not exist
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
