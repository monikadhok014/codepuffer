package org.apache.commons.collections4.bag;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.CollectionBag;
import org.apache.commons.collections4.bag.CollectionSortedBag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bag.PredicatedSortedBag;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.commons.collections4.bag.TransformedSortedBag;
import org.apache.commons.collections4.bag.TreeBag;
import org.apache.commons.collections4.functors.InstanceofPredicate;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.NullIsFalsePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		TreeBag<String> treeBag0 = new TreeBag<String>((Comparator<? super String>) null);
		TreeBag<InputStream> treeBag1 = new TreeBag<InputStream>((Comparator<? super InputStream>) null);
		CollectionBag<InputStream> collectionBag0 = new CollectionBag<InputStream>((Bag<InputStream>) treeBag1);
		SynchronizedSortedBag<InputStream> synchronizedSortedBag0 = new SynchronizedSortedBag<InputStream>(collectionBag0, treeBag0);
		CollectionSortedBag<InputStream> collectionSortedBag0 = new CollectionSortedBag<InputStream>((SortedBag<InputStream>) synchronizedSortedBag0);
		PipedInputStream pipedInputStream0 = new PipedInputStream();
		DataInputStream dataInputStream0 = new DataInputStream((InputStream) pipedInputStream0);
		// Undeclared exception!
		try {
		collectionSortedBag0.add((InputStream) dataInputStream0, 1776);
		
		} catch(ClassCastException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
