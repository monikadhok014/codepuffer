package org.jfree.data;

import java.awt.Canvas;
import java.util.List;
import org.jfree.data.ComparableObjectItem;
import org.jfree.data.ComparableObjectSeries;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.Quarter;
import org.jfree.data.time.Week;
import org.jfree.data.time.Year;
import org.jfree.data.xy.VectorDataItem;
import org.jfree.data.xy.VectorSeries;
import org.jfree.data.xy.XIntervalDataItem;
import org.jfree.data.xy.XIntervalSeries;
import org.jfree.data.xy.XYIntervalDataItem;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.YIntervalSeries;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		VectorSeries vectorSeries0 = new VectorSeries((Comparable) 20);
		VectorDataItem vectorDataItem0 = new VectorDataItem((double) 20, (-824.0), (-1881.96), (-824.0));
		vectorSeries0.add(vectorDataItem0, ",XU8S$5xTU", true);
		vectorSeries0.clear();

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
