package org.apache.lucene.search;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import org.apache.lucene.search.BooleanTopLevelScorers;
import org.apache.lucene.search.ConjunctionScorer;
import org.apache.lucene.search.DisiPriorityQueue;
import org.apache.lucene.search.DisjunctionDISIApproximation;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.FakeScorer;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		float[] floatArray0 = new float[7];
		FakeScorer fakeScorer0 = new FakeScorer();
		BooleanTopLevelScorers.CoordinatingConjunctionScorer booleanTopLevelScorers_CoordinatingConjunctionScorer0 = new BooleanTopLevelScorers.CoordinatingConjunctionScorer((Weight) null, floatArray0, fakeScorer0, 1, fakeScorer0);
		fakeScorer0.score = Float.POSITIVE_INFINITY;
		float float0 = booleanTopLevelScorers_CoordinatingConjunctionScorer0.score();
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
