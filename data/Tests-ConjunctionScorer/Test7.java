package org.apache.lucene.search;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import org.apache.lucene.search.BooleanTopLevelScorers;
import org.apache.lucene.search.ConjunctionScorer;
import org.apache.lucene.search.DisiPriorityQueue;
import org.apache.lucene.search.DisjunctionDISIApproximation;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.FakeScorer;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		DisiPriorityQueue disiPriorityQueue0 = new DisiPriorityQueue(0);
		DisjunctionDISIApproximation disjunctionDISIApproximation0 = new DisjunctionDISIApproximation(disiPriorityQueue0);
		ConjunctionScorer.DocsAndFreqs conjunctionScorer_DocsAndFreqs0 = new ConjunctionScorer.DocsAndFreqs(disjunctionDISIApproximation0);
	
	
	long stop = System.currentTimeMillis();
	System.out.println(" Total time taken : "+ (stop - start));
		}
}
