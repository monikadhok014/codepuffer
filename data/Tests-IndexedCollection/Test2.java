package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		UniquePredicate<Object> uniquePredicate0 = new UniquePredicate<Object>();
		PredicateTransformer<Object> predicateTransformer0 = new PredicateTransformer<Object>((Predicate<? super Object>) uniquePredicate0);
		Boolean boolean0 = predicateTransformer0.transform(uniquePredicate0);
		ConstantFactory<Boolean> constantFactory0 = new ConstantFactory<Boolean>(boolean0);
		FactoryTransformer<Object, Boolean> factoryTransformer0 = new FactoryTransformer<Object, Boolean>((Factory<? extends Boolean>) constantFactory0);
		Object object0 = new Object();
		linkedList0.offerFirst(object0);
		IndexedCollection<Boolean, Object> indexedCollection0 = IndexedCollection.nonUniqueIndexedCollection((Collection<Object>) linkedList0, (Transformer<Object, Boolean>) factoryTransformer0);
		Collection<Object> collection0 = indexedCollection0.values(boolean0);

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
