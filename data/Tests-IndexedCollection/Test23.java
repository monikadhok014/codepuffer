package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Integer> linkedList0 = new LinkedList<Integer>();
		ConstantFactory<String> constantFactory0 = new ConstantFactory<String>("The method to invoke must not be null");
		Integer integer0 = new Integer((-1));
		linkedList0.add(integer0);
		FactoryTransformer<Integer, String> factoryTransformer0 = new FactoryTransformer<Integer, String>((Factory<? extends String>) constantFactory0);
		IndexedCollection<String, Integer> indexedCollection0 = IndexedCollection.uniqueIndexedCollection((Collection<Integer>) linkedList0, (Transformer<Integer, String>) factoryTransformer0);
		Integer integer1 = indexedCollection0.get("The method to invoke must not be null");

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
