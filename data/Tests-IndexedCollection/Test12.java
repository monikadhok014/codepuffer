package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		linkedList0.add((Object) linkedList0);
		Class<Object>[] classArray0 = (Class<Object>[]) Array.newInstance(Class.class, 3);
		InvokerTransformer<Object, Integer> invokerTransformer0 = new InvokerTransformer<Object, Integer>("org.apache.commons.collections4.collection.IndexedCollection", (Class<?>[]) classArray0, (Object[]) classArray0);
		// Undeclared exception!
		try {
		IndexedCollection.nonUniqueIndexedCollection((Collection<Object>) linkedList0, (Transformer<Object, Integer>) invokerTransformer0);
		
		} catch(RuntimeException e) {
		//
		// InvokerTransformer: The method 'org.apache.commons.collections4.collection.IndexedCollection' on 'class java.util.LinkedList' does not exist
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
