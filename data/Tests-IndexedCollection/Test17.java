package org.apache.commons.collections4.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.collection.IndexedCollection;
import org.apache.commons.collections4.functors.ConstantFactory;
import org.apache.commons.collections4.functors.ConstantTransformer;
import org.apache.commons.collections4.functors.FactoryTransformer;
import org.apache.commons.collections4.functors.InstantiateFactory;
import org.apache.commons.collections4.functors.InvokerTransformer;
import org.apache.commons.collections4.functors.PredicateTransformer;
import org.apache.commons.collections4.functors.UniquePredicate;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		LinkedList<Object> linkedList0 = new LinkedList<Object>();
		Class<Object>[] classArray0 = (Class<Object>[]) Array.newInstance(Class.class, 1);
		InvokerTransformer<Object, String> invokerTransformer0 = new InvokerTransformer<Object, String>("O,xkB0WAH4ZSn,'AvUI", (Class<?>[]) classArray0, (Object[]) classArray0);
		IndexedCollection<String, Object> indexedCollection0 = IndexedCollection.uniqueIndexedCollection((Collection<Object>) linkedList0, (Transformer<Object, String>) invokerTransformer0);
		// Undeclared exception!
		try {
		indexedCollection0.contains(linkedList0);
		
		} catch(RuntimeException e) {
		//
		// InvokerTransformer: The method 'O,xkB0WAH4ZSn,'AvUI' on 'class java.util.LinkedList' does not exist
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
