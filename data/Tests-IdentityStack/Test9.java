package org.apache.tools.ant.util;

import java.util.Stack;
import org.apache.tools.ant.util.IdentityStack;


public class Test{
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		IdentityStack identityStack0 = new IdentityStack();
		Object object0 = new Object();
		// Undeclared exception!
		try {
		identityStack0.indexOf(object0, (-1));
		
		} catch(ArrayIndexOutOfBoundsException e) {
		//
		// no message in exception (getMessage() returned null)
		//
		}

		 long stop = System.currentTimeMillis();
		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
