#!/bin/bash 
usage() { echo -e "Usage: $0 \n a - Generate tests using random test generator  \n b - Run tests with soot analysis \n c - Derive constraints \n d - Generate performance tests \n e - Compile and execute generate tests \n secondParameter - packageName \n thirdParameter - className 
without .class" 1>&2; exit 1; }
track=0
p1=p2=p3=p4=0
while getopts ":Rabcde:" o; do
        case "${o}" in
                R)
                        p1=1
                        p2=1
                        p3=1
                        p4=1
                        p5=1
                        echo " Run all phases"
                        ;;
                a)
                        p1=1
                        echo " Phase 1 : Generate tests using random test generator"
                        ;;
                b)
                        echo " Phase 2 : Run tests with soot analysis"
                        p2=1
                        ;;
                c)
                        echo " Phase 3 : Derive constraints"
                        p3=1
                        ;;
                d)
                        echo " Phase 4 : Generate performance tests"
                        p4=1
                        ;;
                e)
                        echo " Phase 5 : Compile and execute generate tests"
                        p5=1
                        ;;
                *)
                        usage
                        ;;
        esac
done

function generateTest 
{
	rm -rf tests
	mkdir tests
	java -classpath randoop-2.1.3.jar:../benchmark/:. randoop.main.Main gentests --testclass=$1 --small-tests=true  --testsperfile=1  --no-regression-assertions=true --junit-output-dir=tests --outputlimit=2000
	#java -classpath randoop-2.1.3.jar:../benchmark/:. randoop.main.Main gentests --testclass=$1 --small-tests=true  --testsperfile=1  --no-regression-assertions=true --junit-output-dir=tests
}

function deriveConstraints
{
	cd ../sootAnalysis
	ant -f build.xml
	cp src/benchmarks/Summary.txt classes/.
	cd classes
	java -cp ../lib/soot.jar:.:../../benchmark/. javato.activetesting.DeriveConstraints $1
}

function generateTests
{
	folderName=`tr '.' '/' <<< "$2"`
	ls "../../benchmark/"$folderName > list
	params=""
	while read line
	do
		params=$params" "$2"."${line%.*}
	done < list
	echo $params
	java -cp ../lib/soot.jar:.:../../benchmark javato.activetesting.GetCandidateMethods $1 $params 
	cd ../..
	
}

function check
{
        base=`echo $1 | tr . /`
        data="../perfTests/"$2"/*"
        data="../perfTests/all/*"
        addr="../benchmark/"$base"/"
        counter=0;
        for file in $data
        do
                cp $file $addr
                cd ../benchmark
                name=`basename $file`
                echo -e "\t\t\t***********Compiling $base/$name ****************"       
                javac $base"/"$name
                if [ $? -ne 0 ] 
                then

                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
                        echo -e "\t \t \t COMPILATION FAILED FOR $base/$name "
                        echo -e "\t \t \t Removing the file $file"
                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
                        rm $file
                        rm $addr`basename $file`
                        cd ../evosuite
                        continue;
                fi
                filename="${file%.*}"
                extension="${filename##*/}"
                java $1"."$extension 10
                if [ $? -ne 0 ]
                then

                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
                        echo -e "\t \t \t EXECUTION FAILED FOR $base/$name "
                        echo -e "\t \t \t Removing the file $file"
                        echo -e "\t \t \t ++++++++++++++++++++++++++++++ "
                        rm $file
                        rm $addr`basename $file`
                        cd ../evosuite
                        continue;
                else
                        echo -e "\t\t\t***********Execution successful for $base/$name ****************"      
                fi

                #rm $addr`basename $file`       
                (( counter++ ))
                cd ../evosuite
        done
        echo -e "\t \t \t ----------------------------------------------- "
        echo -e "\t \t \t Total number of tests generated : $counter"
        echo -e "\t \t \t ---------------------------------------------- "
}

packageName=$2
name=$3
className=$packageName"."$name

echo -e "1\n" | sudo update-alternatives --config java	
java -version
if [[ $p1 == 1 ]]
then
	echo "................................ Processing $name ..........................................."
	generateTest $className
	echo "................................ Parsing $name"_ESTest.java" ................................."
	./scripts/parseRandoopTests.sh
	rm -rf tests
fi
if [[ $p2 == 1 ]]
then
	pwd
	echo "................. Running soot analysis on tests of  $name"_ESTest.java" ..........................."
	rm -f ../sootAnalysis/src/benchmarks/Summary.txt  ../sootAnalysis/classes/Summary.txt  ../sootAnalysis/classes/revisedSummary.txt
	./scripts/run-soot.sh $className
fi
if [[ $p3 == 1 ]]
then
	echo "............................... Deriving constraints & generating tests of $name ..........................."
	deriveConstraints $className 
fi
if [[ $p4 == 1 ]]
then
	echo "............................... Deriving constraints & generating tests of $name ..........................."
	generateTests $className 
fi
if [[ $p5 == 1 ]]
then
        echo "............................... Generating tests for $name ..........................."
        check $packageName $name 
fi

#./Puffer.sh className
