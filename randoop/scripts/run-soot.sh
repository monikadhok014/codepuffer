#./parseTest.sh ListOrderedSet_ESTest.java
#./run-soot.sh org.apache.commons.collections.set org.apache.commons.collections.set.ListOrderedSet

name=`tr '.' '/' <<< "$1"`
target="../../benchmark"
dir=modifiedTests
counter=0

if find "$dir" -maxdepth 0 -empty | read;
then
	echo "$dir empty."
	echo "No tests generated!!."
	exit 127
else
	cd $fsehome/randoop/modifiedTests
	for filename in *
	do

		cd $fsehome/randoop/modifiedTests
		echo " ##################### Processing : " $filename " ############################# "		#Processing message"
		pwd
		cp $filename $target"/"$filename					#Copy the file from EvoTests to location in benchmark folder
		echo "copied $filename to $target"

		cd ../../benchmark									#Go in to the benchmark folder and compile it
		echo "Shifted in to benchmark folder"

		echo "Compiling  $filename"
		javac $filename											#Compile the file thus copied
		if [ $? -ne 0 ]
		then
			echo -e "\t \t \t ***************ERROR: Most probably versions do not match for Evosuite and soot.*****************"
			echo -e  "\t \t \t ***************ERROR: Deleting the result for this test.***********"
			cd ../randoop
			continue;
		fi

		echo "Running soot analysis on ${filename%.*}"
		cd ../sootAnalysis											#Go in to sootAnalysis
		ant  -Djavato.app.name=$2 -Djavato.app.main.class=${filename%.*} -f run.xml genSummary 			#Process it for generating summaries
		#ant  -Djavato.app.name=$2 -Djavato.app.main.class=${filename%.*} -Djavato.app.testIdx=$filename -f run.xml Puffer 			#Process it for generating summaries
		
		((counter++))	
	done;
fi

