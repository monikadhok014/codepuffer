package org.apache.pdfbox.cos;
import java.util.Collection;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
public class Test16
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.cos.COSArray p1 = new org.apache.pdfbox.cos.COSArray();
		for (int i = 0; i < largeNumber; i++) {			org.apache.pdfbox.pdmodel.common.COSStreamArray p2 = new org.apache.pdfbox.pdmodel.common.COSStreamArray( p1);
			 p1.add(p2);
		}

		/* Create first parameter. */
		ArrayList p3= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p3.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p1.removeAll( p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
