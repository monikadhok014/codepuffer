package org.apache.pdfbox.cos;
import java.util.Collection;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
public class Test15
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.cos.COSArray p13 = new org.apache.pdfbox.cos.COSArray();
		for (int i = 0; i < largeNumber * 0.5; i++) {			org.apache.pdfbox.pdmodel.common.COSStreamArray p14 = new org.apache.pdfbox.pdmodel.common.COSStreamArray( p13);
			 p13.add(p14);
		}

		/* Create first parameter. */
		ArrayList p15= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p15.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p13.retainAll( p15);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
