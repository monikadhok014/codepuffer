package org.apache.pdfbox.pdmodel.common;
import java.util.Collection;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
public class Test21
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.pdmodel.common.COSArrayList p7 = new org.apache.pdfbox.pdmodel.common.COSArrayList();

		/* Create first parameter. */
		ArrayList p8= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p8.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p9 = (int)Math.random();
		 p7.addAll( p9,  p8);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
