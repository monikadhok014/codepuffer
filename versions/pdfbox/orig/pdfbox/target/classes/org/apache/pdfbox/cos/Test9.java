package org.apache.pdfbox.cos;
import java.util.Collection;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
public class Test9
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.cos.COSArray p10 = new org.apache.pdfbox.cos.COSArray();
		for (int i = 0; i < largeNumber; i++) {			org.apache.pdfbox.pdmodel.common.COSStreamArray p11 = new org.apache.pdfbox.pdmodel.common.COSStreamArray( p10);
			 p10.add(p11);
		}

		/* Create first parameter. */
		ArrayList p12= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p12.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p10.removeAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
