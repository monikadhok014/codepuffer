package org.apache.pdfbox.cos;
import java.util.Collection;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
public class Test12
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.cos.COSArray p4 = new org.apache.pdfbox.cos.COSArray();
		for (int i = 1; i < largeNumber; i++) {			org.apache.pdfbox.pdmodel.common.COSStreamArray p5 = new org.apache.pdfbox.pdmodel.common.COSStreamArray( p4);
			 p4.add(p5);
		}

		/* Create first parameter. */
		ArrayList p6= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p6.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p4.retainAll( p6);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
