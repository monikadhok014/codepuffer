package org.apache.pdfbox.pdmodel.common;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.pdfbox.pdmodel.common.COSArrayList p1 = new org.apache.pdfbox.pdmodel.common.COSArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p2 = (int)Math.random();
		 p1.remove( p2);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
