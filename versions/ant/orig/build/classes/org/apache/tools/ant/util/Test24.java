package org.apache.tools.ant.util;
import java.util.Collection;
import org.apache.tools.ant.util.VectorSet;
import java.util.ArrayList;
public class Test24
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.tools.ant.util.VectorSet p11 = new org.apache.tools.ant.util.VectorSet();
		for (int i = 0; i < largeNumber; i++) {
			 p11.add(new Object()); 
		}

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		ArrayList p12 = new ArrayList();
		 p11.addAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
