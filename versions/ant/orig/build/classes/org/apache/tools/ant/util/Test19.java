package org.apache.tools.ant.util;
import java.util.Collection;
import org.apache.tools.ant.util.VectorSet;
import java.util.ArrayList;
public class Test19
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.tools.ant.util.VectorSet p7 = new org.apache.tools.ant.util.VectorSet();
		for (int i = 0; i < largeNumber; i++) {
			 p7.add((int)Math.random()); 
		}

		/* Create first parameter. */
		ArrayList p8= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p8.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p7.retainAll( p8);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
