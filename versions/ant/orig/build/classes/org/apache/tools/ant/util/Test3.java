package org.apache.tools.ant.util;
import java.util.Collection;
import org.apache.tools.ant.util.IdentityStack;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.tools.ant.util.IdentityStack p5 = new org.apache.tools.ant.util.IdentityStack();
		for (int i = 0; i < largeNumber; i++) {
			 p5.add(new Object()); 
		}

		/* Create first parameter. */
		ArrayList p6= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p6.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p5.removeAll( p6);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
