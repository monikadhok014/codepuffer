package groovy.util;
import java.util.Collection;
import groovy.util.ObservableList;
import java.util.ArrayList;
public class Test
{
	public static void main(String[] args) 
	{

		groovy.util.ObservableList p1 = new groovy.util.ObservableList();
		for (int i = 0; i < 8000; i++) 
			 p1.add(i); 
		ArrayList p2= new ArrayList();
		for (int i = 0; i < 8000; i++) 
			 p2.add(i); 
		long start = System.currentTimeMillis();
		 p1.retainAll( p2);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
