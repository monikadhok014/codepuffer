package org.apache.commons.collections4.multimap;
import java.util.Collection;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap.WrappedCollection;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.multimap.ArrayListValuedHashMap p1= new org.apache.commons.collections4.multimap.ArrayListValuedHashMap( ) ;
		for (int i = 0; i < largeNumber; i++) {
			 p1.put(i, i); 
		}
		java.lang.Object p2= new java.lang.Object() ;
		org.apache.commons.collections4.multimap.AbstractMultiValuedMap.WrappedCollection p3=  p1.new WrappedCollection( p2 ) ;

		/* Create first parameter. */
		ArrayList p4= new ArrayList();
		for (int i = largeNumber; i < 2*largeNumber; i++) {
			 p4.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p3.retainAll( p4);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
		System.out.print("\n");
		System.out.print(largeNumber);
		System.out.print(" : ");
		System.out.print(stop-start);
	}	
}
