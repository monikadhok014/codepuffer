package org.apache.commons.collections4;
import java.util.Collection;
import java.util.Collection;
import org.apache.commons.collections4.CollectionUtils;
import java.util.ArrayList;
public class Test2
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create first parameter. */
		ArrayList p1= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		ArrayList p2= new ArrayList();
		for (int i = largeNumber; i < 2*largeNumber; i++) {
			 p2.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.apache.commons.collections4.CollectionUtils.removeAll( p1,  p2);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
		System.out.print("\n");
		System.out.print(largeNumber);
		System.out.print(" : ");
		System.out.print(stop-start);
	}	
}
