package org.apache.commons.collections4.collection;
import java.util.Collection;
import org.apache.commons.collections4.collection.IndexedCollection;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		java.util.LinkedList p1 = new java.util.LinkedList();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		org.apache.commons.collections4.map.MultiValueMap p2 = new org.apache.commons.collections4.map.MultiValueMap();

		org.apache.commons.collections4.collection.IndexedCollection p3 =  new org.apache.commons.collections4.collection.IndexedCollection( p1, p1, p2,false);

		/* Create first parameter. */
		ArrayList p4= new ArrayList();
		for (int i = largeNumber; i < 2*largeNumber; i++) {
			 p4.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p3.retainAll( p4);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
		System.out.print("\n");
		System.out.print(largeNumber);
		System.out.print(" : ");
		System.out.print(stop-start);
	}	
}
