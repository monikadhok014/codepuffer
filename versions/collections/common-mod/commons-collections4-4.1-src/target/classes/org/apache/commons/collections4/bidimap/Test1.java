package org.apache.commons.collections4.bidimap;
import java.util.Collection;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.bidimap.DualHashBidiMap p1= new org.apache.commons.collections4.bidimap.DualHashBidiMap( ) ;
		for (int i = 0; i < largeNumber; i++) {
			 p1.put(i, i); 
		}
		org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet p2= new org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet( p1 ) ;

		/* Create first parameter. */
		ArrayList p3= new ArrayList();
		for (int i = largeNumber; i < 2*largeNumber; i++) {
			 p3.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p2.retainAll( p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
		System.out.print("\n");
		System.out.print(largeNumber);
		System.out.print(" : ");
		System.out.print(stop-start);
	}	
}
