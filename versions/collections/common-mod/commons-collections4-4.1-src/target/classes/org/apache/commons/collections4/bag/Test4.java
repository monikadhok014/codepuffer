package org.apache.commons.collections4.bag;
import java.util.Collection;
import org.apache.commons.collections4.bag.CollectionBag;
import java.util.ArrayList;
public class Test4
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		org.apache.commons.collections4.bag.TreeBag p10 = new org.apache.commons.collections4.bag.TreeBag();
		for (int i = 1; i < largeNumber; i++) {
			 p10.add(alpha(i)); 
		}

		org.apache.commons.collections4.bag.CollectionBag p11 =  new org.apache.commons.collections4.bag.CollectionBag( p10);

		/* Create first parameter. */
		ArrayList p12= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p12.add(alpha(i)); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p11.retainAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
		System.out.print("\n");
		System.out.print(largeNumber);
		System.out.print(" : ");
		System.out.print(stop-start);
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
