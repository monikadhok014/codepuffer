package org.apache.commons.collections4.bag;
import java.util.Collection;
import org.apache.commons.collections4.bag.CollectionBag;
import java.util.ArrayList;
public class Test14
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		org.apache.commons.collections4.bag.TreeBag p10 = new org.apache.commons.collections4.bag.TreeBag();
		for (int i = 0; i < largeNumber; i++) {
			 p10.add((int)Math.random()); 
		}

		org.apache.commons.collections4.bag.CollectionBag p11 =  new org.apache.commons.collections4.bag.CollectionBag( p10);

		/* Create first parameter. */
		ArrayList p12= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p12.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p11.retainAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
