package org.apache.commons.collections4.bag;
import java.util.Collection;
import org.apache.commons.collections4.bag.CollectionBag;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		org.apache.commons.collections4.bag.TreeBag p16 = new org.apache.commons.collections4.bag.TreeBag();
		for (int i = 0; i < largeNumber; i++) {
			 p16.add(i); 
		}

		org.apache.commons.collections4.bag.CollectionBag p17 =  new org.apache.commons.collections4.bag.CollectionBag( p16);

		/* Create first parameter. */
		ArrayList p18= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p18.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p17.retainAll( p18);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
