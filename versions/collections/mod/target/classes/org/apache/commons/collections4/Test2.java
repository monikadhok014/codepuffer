package org.apache.commons.collections4;
import java.util.Collection;
import java.util.Collection;
import org.apache.commons.collections4.CollectionUtils;
import java.util.ArrayList;
public class Test2
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p3= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p3.add(alpha(i).toString()); 
		}

		ArrayList p4= new ArrayList();
		for (int i = 1; i < largeNumber; i++) {
			 p4.add(alpha(i).toString()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.apache.commons.collections4.CollectionUtils.containsAny( p4,  p3);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
	private static char[] vs; 
	static { 
		vs = new char['Z' - 'A' + 1]; 
		for(char i='A'; i<='Z';i++) vs[i - 'A'] = i; 
	} 
	private static StringBuilder alpha(int i){  
		assert i > 0;  
		char r = vs[--i % vs.length];  
		int n = i / vs.length;  
		return n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  
	}  
}
