package org.apache.commons.collections4.bag;
import java.util.Collection;
import org.apache.commons.collections4.bag.CollectionBag;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		org.apache.commons.collections4.bag.TreeBag p7 = new org.apache.commons.collections4.bag.TreeBag();
		for (int i = 0; i < largeNumber * 0.5; i++) {
			 p7.add(i); 
		}

		org.apache.commons.collections4.bag.CollectionBag p8 =  new org.apache.commons.collections4.bag.CollectionBag( p7);

		/* Create first parameter. */
		ArrayList p9= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p9.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p8.retainAll( p9);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
