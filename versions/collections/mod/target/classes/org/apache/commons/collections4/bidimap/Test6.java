package org.apache.commons.collections4.bidimap;
import java.util.Collection;
import org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.bidimap.DualHashBidiMap p16= new org.apache.commons.collections4.bidimap.DualHashBidiMap( ) ;
		org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet p17= new org.apache.commons.collections4.bidimap.AbstractDualBidiMap.EntrySet( p16 ) ;

		/* Create first parameter. */
		ArrayList p18= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p18.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p17.retainAll( p18);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
