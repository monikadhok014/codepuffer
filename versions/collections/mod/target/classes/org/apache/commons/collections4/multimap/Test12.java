package org.apache.commons.collections4.multimap;
import java.util.Collection;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap.WrappedCollection;
import java.util.ArrayList;
public class Test12
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.multimap.ArrayListValuedHashMap p21= new org.apache.commons.collections4.multimap.ArrayListValuedHashMap( ) ;
		java.lang.Object p22= new java.lang.Object() ;
		org.apache.commons.collections4.multimap.AbstractMultiValuedMap.WrappedCollection p23=  p21.new WrappedCollection( p22 ) ;

		/* Create first parameter. */
		ArrayList p24= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p24.add((int)Math.random()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p23.retainAll( p24);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
