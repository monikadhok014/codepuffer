package org.apache.commons.collections4;
import java.util.Collection;
import java.util.Collection;
import java.util.ArrayList;

public class Test
{
	public static void main(String[] args)
	{
		int largeNumber = 3;
		/* Create first parameter.*/
		ArrayList var1= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			var1.add(i);
		}
		/* Create second parameter.*/
		java.util.ArrayList var2= new java.util.ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			var2.add(i);
		}

		/* Invoke the method on the created object.*/
		long start = System.currentTimeMillis();
		org.apache.commons.collections4.CollectionUtils.containsAny(var1,var2);
		long stop = System.currentTimeMillis();

		 System.out.println(" Total time taken : "+ (stop - start));
		}
}
