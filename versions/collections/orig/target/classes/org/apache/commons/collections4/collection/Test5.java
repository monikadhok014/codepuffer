package org.apache.commons.collections4.collection;
import java.util.Collection;
import org.apache.commons.collections4.collection.IndexedCollection;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		java.util.ArrayList p21 = new java.util.ArrayList();
		for (int i = 0; i < largeNumber * 0.5; i++) {
			 p21.add(i); 
		}

		org.apache.commons.collections4.Transformer p22 = org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();

		org.apache.commons.collections4.map.MultiValueMap p23 = new org.apache.commons.collections4.map.MultiValueMap();

		org.apache.commons.collections4.collection.IndexedCollection p24 =  new org.apache.commons.collections4.collection.IndexedCollection( p21, p22, p23,false);

		/* Create first parameter. */
		ArrayList p25= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p25.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p24.retainAll( p25);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
