package org.apache.commons.collections4.set;
import java.util.Collection;
import org.apache.commons.collections4.set.ListOrderedSet;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.set.ListOrderedSet p11 = new org.apache.commons.collections4.set.ListOrderedSet();
		for (int i = 0; i < largeNumber; i++) {
			 p11.add(i); 
		}

		/* Create first parameter. */
		ArrayList p12= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p12.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p11.retainAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
