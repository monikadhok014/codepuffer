package org.apache.commons.collections4;
import java.util.Collection;
import java.util.Collection;
import org.apache.commons.collections4.CollectionUtils;
import java.util.ArrayList;
public class Test18
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p11= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p11.add(i); 
		}

		ArrayList p12= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p12.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.apache.commons.collections4.CollectionUtils.containsAny( p12,  p11);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
