package org.apache.commons.collections4.multimap;
import java.util.Collection;
import org.apache.commons.collections4.multimap.AbstractMultiValuedMap.WrappedCollection;
import java.util.ArrayList;
public class Test11
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.multimap.ArrayListValuedHashMap p17= new org.apache.commons.collections4.multimap.ArrayListValuedHashMap( ) ;
		java.lang.Object p18= new java.lang.Object() ;
		org.apache.commons.collections4.multimap.AbstractMultiValuedMap.WrappedCollection p19=  p17.new WrappedCollection( p18 ) ;

		/* Create first parameter. */
		ArrayList p20= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p20.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p19.retainAll( p20);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
