package org.apache.commons.collections4.list;
import java.util.Collection;
import org.apache.commons.collections4.list.SetUniqueList;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		org.apache.commons.collections4.list.CursorableLinkedList p7 = new org.apache.commons.collections4.list.CursorableLinkedList();
		for (int i = 0; i < largeNumber; i++) {
			 p7.add(new Object()); 
		}

		org.apache.commons.collections4.list.SetUniqueList p8 = org.apache.commons.collections4.list.SetUniqueList.setUniqueList( p7);

		/* Create first parameter. */
		ArrayList p9= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p9.add(new Object()); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p8.retainAll( p9);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
