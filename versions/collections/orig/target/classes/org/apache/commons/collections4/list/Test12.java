package org.apache.commons.collections4.list;
import java.util.Collection;
import org.apache.commons.collections4.list.NodeCachingLinkedList;
import java.util.ArrayList;
public class Test12
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.apache.commons.collections4.list.NodeCachingLinkedList p11 = new org.apache.commons.collections4.list.NodeCachingLinkedList();
		for (int i = 0; i < largeNumber; i++) {
			 p11.add(i); 
		}

		/* Create first parameter. */
		ArrayList p12= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p12.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p11.removeAll( p12);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
