package org.apache.commons.collections4;
import java.util.Collection;
import java.util.Collection;
import org.apache.commons.collections4.CollectionUtils;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		/* Create first parameter. */
		ArrayList p1= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p1.add(i); 
		}

		ArrayList p2= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p2.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.apache.commons.collections4.CollectionUtils.containsAny( p2,  p1);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
