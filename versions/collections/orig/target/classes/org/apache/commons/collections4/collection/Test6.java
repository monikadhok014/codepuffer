package org.apache.commons.collections4.collection;
import java.util.Collection;
import org.apache.commons.collections4.collection.IndexedCollection;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		java.util.ArrayList p26 = new java.util.ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p26.add(i); 
		}

		org.apache.commons.collections4.Transformer p27 = org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();

		org.apache.commons.collections4.map.MultiValueMap p28 = new org.apache.commons.collections4.map.MultiValueMap();

		org.apache.commons.collections4.collection.IndexedCollection p29 =  new org.apache.commons.collections4.collection.IndexedCollection( p26, p27, p28,false);

		/* Create first parameter. */
		ArrayList p30= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p30.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p29.retainAll( p30);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
