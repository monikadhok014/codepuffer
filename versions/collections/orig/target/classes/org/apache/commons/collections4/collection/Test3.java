package org.apache.commons.collections4.collection;
import java.util.Collection;
import org.apache.commons.collections4.collection.IndexedCollection;
import java.util.ArrayList;
public class Test3
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */

		java.util.LinkedList p11 = new java.util.LinkedList();
		for (int i = 0; i < largeNumber * 0.5; i++) {
			 p11.add(i); 
		}

		org.apache.commons.collections4.Transformer p12 = org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();

		org.apache.commons.collections4.map.MultiValueMap p13 = new org.apache.commons.collections4.map.MultiValueMap();

		org.apache.commons.collections4.collection.IndexedCollection p14 =  new org.apache.commons.collections4.collection.IndexedCollection( p11, p12, p13,false);

		/* Create first parameter. */
		ArrayList p15= new ArrayList();
		for (int i = 0; i < largeNumber; i++) {
			 p15.add(i); 
		}


		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		 p14.retainAll( p15);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
