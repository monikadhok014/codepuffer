package org.apache.commons.collections4;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class Test {
    public static void main(String[] args) {
        int largeNumber = 300000;
        List<Integer> one = new ArrayList<Integer>();
        for(int i = 0; i < largeNumber; i++) {
            one.add(i+5000);
        }
        List<Integer> two = new ArrayList<Integer>();
        for(int i = 0; i < largeNumber; i++) {
            two.add(i+7000);
        }
        
        long start = System.currentTimeMillis();
        ListUtils.union(one, two); // problem manifests here
        long stop = System.currentTimeMillis();
        System.out.println("Time is " + (stop - start));
    }
}
