package org.jfree.data;
import org.jfree.data.ComparableObjectSeries;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
			Comparable p15= new Comparable() { 
			@Override
			public int compareTo(Object o)
				{
				return 0;
				}
			};

		org.jfree.data.ComparableObjectSeries p16 =  new org.jfree.data.ComparableObjectSeries( p15);

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p17 = (int)Math.random();
		 p16.setMaximumItemCount( p17);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
