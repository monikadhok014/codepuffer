package org.jfree.data;
import org.jfree.data.ComparableObjectSeries;
import java.util.ArrayList;
public class Test1
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
			Comparable p1= new Comparable() { 
			@Override
			public int compareTo(Object o)
				{
				return 0;
				}
			};

		org.jfree.data.ComparableObjectSeries p2 =  new org.jfree.data.ComparableObjectSeries( p1);
		for (int i = 0; i < largeNumber; i++) {			Comparable p3= new Comparable() { 
			@Override
			public int compareTo(Object o)
				{
				return 0;
				}
			};

			int p4 = (int)Math.random();
			 p2.add(p3,p4);
		}
		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p5 = (int)Math.random();
		 p2.setMaximumItemCount( p5);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
