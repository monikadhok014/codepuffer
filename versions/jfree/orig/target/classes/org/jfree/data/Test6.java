package org.jfree.data;
import org.jfree.data.ComparableObjectSeries;
import java.util.ArrayList;
public class Test6
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
			Comparable p18= new Comparable() { 
			@Override
			public int compareTo(Object o)
				{
				return 0;
				}
			};

		org.jfree.data.ComparableObjectSeries p19 =  new org.jfree.data.ComparableObjectSeries( p18);

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p20 = (int)Math.random();
		 p19.setMaximumItemCount( p20);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
