package org.jfree.data;
import org.jfree.data.ComparableObjectSeries;
import java.util.ArrayList;
public class Test2
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
			Comparable p6= new Comparable() { 
			@Override
			public int compareTo(Object o)
				{
				return 0;
				}
			};

		org.jfree.data.ComparableObjectSeries p7 =  new org.jfree.data.ComparableObjectSeries( p6);

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();

		int p8 = (int)Math.random();
		 p7.setMaximumItemCount( p8);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
