package org.jfree.chart.plot;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import java.util.ArrayList;
public class Test18
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = 22000;
		/* Create a constructor to invoke the object. */
		org.jfree.chart.plot.CategoryPlot p1 = new org.jfree.chart.plot.CategoryPlot();
		for (int i = 0; i < largeNumber; i++) {			int p2 = i;
			org.jfree.data.category.DefaultCategoryDataset p3 = new org.jfree.data.category.DefaultCategoryDataset();
			p1.setDataset( p2,p3);
		}
		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.jfree.chart.axis.CategoryAxis p4 = new org.jfree.chart.axis.CategoryAxis();
		 p1.getCategoriesForAxis( p4);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
