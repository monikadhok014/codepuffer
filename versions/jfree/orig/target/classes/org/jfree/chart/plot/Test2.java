package org.jfree.chart.plot;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import java.util.ArrayList;
public class Test2
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.jfree.chart.plot.CategoryPlot p5 = new org.jfree.chart.plot.CategoryPlot();
		for (int i = 0; i < largeNumber; i++) {			int p6 = i;
			org.jfree.data.category.DefaultCategoryDataset p7 = new org.jfree.data.category.DefaultCategoryDataset();
			p5.setDataset( p6,p7);
		}
		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.jfree.chart.axis.CategoryAxis p8 = new org.jfree.chart.axis.CategoryAxis();
		 p5.getCategoriesForAxis( p8);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
