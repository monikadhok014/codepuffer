package org.jfree.chart.plot;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import java.util.ArrayList;
public class Test5
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.jfree.chart.plot.CategoryPlot p17 = new org.jfree.chart.plot.CategoryPlot();
		for (int i = 0; i < largeNumber; i++) {			int p18 = i;
			org.jfree.data.category.DefaultCategoryDataset p19 = new org.jfree.data.category.DefaultCategoryDataset();
			p17.setDataset( p18,p19);
		}
		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.jfree.chart.axis.CategoryAxis p20 = new org.jfree.chart.axis.CategoryAxis();
		 p17.getCategoriesForAxis( p20);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
