package org.jfree.chart.plot;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import java.util.ArrayList;
public class Test10
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{

		int largeNumber = Integer.parseInt(args[0]);
		/* Create a constructor to invoke the object. */
		org.jfree.chart.plot.CategoryPlot p9 = new org.jfree.chart.plot.CategoryPlot();

		/* Create first parameter. */

		/* Invoke the method on the created object. */
		long start = System.currentTimeMillis();
		org.jfree.chart.axis.CategoryAxis p10 = new org.jfree.chart.axis.CategoryAxis();
		 p9.getCategoriesForAxis( p10);
		long stop = System.currentTimeMillis(); 
		System.out.println(" Total time taken : "+ (stop - start));
	}	
}
