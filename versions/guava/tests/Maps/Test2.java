package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import com.google.common.collect.AbstractMapBasedMultimap;

public class Test {
	public static void main(String[] args) {
		int largeNumber = 300000;

		HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
		for(int i = 0; i < largeNumber; i++) {
			hashMap.put(i,(int)Math.random());
		}
		
		Maps.KeySet maps_keyset = new Maps.KeySet(hashMap);

		List<Integer> two = new ArrayList<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			two.add(i);
		}
		
		long start = System.currentTimeMillis();
	        boolean boolean1 =  maps_keyset.retainAll(two);
		long stop = System.currentTimeMillis();
		System.out.println("Time is " + (stop - start));

	}
}
