package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Synchronized;
import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentSkipListSet;
import com.google.common.collect.HashMultimap;

public class Test_abstractMap {
	public static void main(String[] args) {
		int largeNumber = 300000;
		HashMultimap<Integer, Integer> hashMultiMap = HashMultimap.create();
		ConcurrentSkipListSet<Integer> concurrentSkipList = new ConcurrentSkipListSet<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			concurrentSkipList.add(i);
		}

		HashMultimap.WrappedNavigableSet wrappedNavigableSet  = hashMultiMap.new WrappedNavigableSet(null, concurrentSkipList,null);
		
		List<Integer> two = new ArrayList<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			two.add(i);
		}
		
		long start = System.currentTimeMillis();
	        boolean boolean1 = wrappedNavigableSet.retainAll(two);
		long stop = System.currentTimeMillis();
		System.out.println("Time is " + (stop - start));

	}
}
