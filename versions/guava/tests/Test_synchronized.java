package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Synchronized;
import java.util.HashSet;

public class Test_synchronized {
	public static void main(String[] args) {
		int largeNumber = 300000;

		

		HashSet<Integer> set = new HashSet<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			set.add(i);
		}
		Synchronized.SynchronizedSet syncSet = new Synchronized.SynchronizedSet(set, null);

		List<Integer> two = new ArrayList<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			two.add(i);
		}
		
		long start = System.currentTimeMillis();
	        boolean boolean1 = syncSet.retainAll(two);
		long stop = System.currentTimeMillis();
		System.out.println("Time is " + (stop - start));

	}
}
