package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Iterables;

public class Test {
	public static void main(String[] args) {
		int largeNumber = 300000;
		List<Integer> one = new ArrayList<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			one.add(i);
		}

		List<Integer> two = new ArrayList<Integer>();
		for(int i = 0; i < largeNumber; i++) {
			two.add(i);
		}
		
		long start = System.currentTimeMillis();
	        boolean boolean1 = Iterables.retainAll(one,two);
		long stop = System.currentTimeMillis();
		System.out.println("Time is " + (stop - start));

	}
}
