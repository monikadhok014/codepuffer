#!/usr/bin/env gnuplot
#
# Creates a version of a plot, which looks nice for inclusion on web pages
#
# Original author: Hagen Wierstorf
# Modified by: Irshad Pananilath <pmirshad+code@gmail.com>

reset

load 'style.gnu.in'

out_file = 'fig10'
data_file = 'fig10'

# To be loaded after setting the file names
load 'heat_settings.gnu.in'


plot data_file.'.dat'  u 1:2 t 'B1' w lp ls 2, \
     '' u 1:3 t 'B2' w lp ls 3, \
     '' u 1:4 t 'B3' w lp ls 4, \
     '' u 1:5 t 'B4' w lp ls 5, \
     '' u 1:6 t 'B5' w lp ls 6, \
     '' u 1:7 t 'B6' w lp ls 7, \
     '' u 1:8 t 'B7' w lp ls 8
