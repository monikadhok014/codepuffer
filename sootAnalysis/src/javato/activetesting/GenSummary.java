/* 
 * Generates summary for each executed method.
 */

package javato.activetesting;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javato.activetesting.T.AdderMethsMap;
import javato.activetesting.T.ClassSummaryMap;
import javato.activetesting.T.ConstructorInfoMap;
import javato.activetesting.T.Detail;
import javato.activetesting.T.LoopDepthMap;
import javato.activetesting.T.LoopDetailsMap;
import javato.activetesting.T.MethodDetail;
import javato.activetesting.T.MethodDetailMap;
import javato.activetesting.T.PerMethodSummaryMap;
import javato.activetesting.analysis.AnalysisImpl;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.options.Options;


public class GenSummary extends AnalysisImpl {

	boolean debug_flag = false;

	String className; /* Generates class summary of this class */
	String testName; /* PerMethodSummary generated using this test */
	ClassSummaryMap classSummary = new ClassSummaryMap();
	PerMethodSummaryMap summary = new PerMethodSummaryMap();

	LoopDetailsMap loopDetails;
	LoopDepthMap loopDepth;
	MethodDetailMap methodsInvoked;

	/* Method was invoked using this constructor in the original test*/
	String refMethod = null; 
	ArrayList<Integer> loopIdxSequence;
	ArrayList<Integer> loopIds;

	ArrayList<String> methodSeqs = new ArrayList<String>();
	AdderMethsMap adderMeths = new AdderMethsMap();
	/* Used for adder methods */
	HashMap<String, Integer> loopCnts = new HashMap<String, Integer>(); 
	Boolean adderMethodDebug = true;

	ObjectFileStore mof = new ObjectFileStore();

	//Flag to avoid nested loops 
	boolean flag = false; 
	String curMethodName = "";
	
	PerMethodSummary constructorSummary = new PerMethodSummary(loopDetails, loopDepth, methodsInvoked, testName, "");
	String methodNameForConstructor = "";
	public void initialize() 
	{
		printDebugMsg("initialize()");
		className = System.getProperty("javato.app.name");
		testName = System.getProperty("javato.app.testIdx");
		reset();
	}

	public void reset()
	{
		//Reset everything since method exits.
		loopDetails = new LoopDetailsMap();
		loopDepth  = new LoopDepthMap();
		methodsInvoked  = new MethodDetailMap(); 
		loopIdxSequence = new ArrayList<Integer>();
		loopIds = new ArrayList<Integer>();
	}

	/* * * * * * * * * * * *  Method invocation * * * * * * * * * * * */
	/* * * * * * *line number, parameters, method name * * * * * * * * */
	public void methInvoke(Integer iid, Object list, String str) 
	{
		printDebugMsg("methInvoke("+javato.activetesting.analysis.Observer.getIidToLine(iid)+")" + list);
		ArrayList<Integer> intList = createParamList(list);
		if(methodSeqs.size() == 1)
		{
			MethodDetailMap mapd = constructorSummary.methodsInvoked;
			MethodDetail md = new MethodDetail(str, intList);
			mapd.put(methodNameForConstructor, md); 
			constructorSummary.methodsInvoked = mapd;
		}
		
		if(methodSeqs.size() ==0 ) return;

		String tmpMethodName = methodSeqs.get(methodSeqs.size() - 1);
		if(tmpMethodName.contains("access$")) return;

		
		int index = methodSeqs.indexOf(curMethodName);
		int size = methodSeqs.size() - 1;

		//Only recent method calls are allowed.
		if(size - index == 1)
			handle1LevelMethods(tmpMethodName, str, intList);
		else 
			handle2LevelMethods(str, intList, tmpMethodName);
	}

	public void handle1LevelMethods(String tmpMethodName, String str, ArrayList<Integer> intList)
	{
		if ((!curMethodName.equals(tmpMethodName)))
		{
			MethodDetail md = new MethodDetail(str, intList);
			methodsInvoked.put(tmpMethodName, md); 
		}
		int curLoopId = 0;
		if(loopIds.size() > 0)
			curLoopId = loopIds.get(loopIds.size()-1);

		if(curLoopId != 0 && loopIds.size() > 0 && loopDetails.get(curLoopId) != null  ){
			Detail d = loopDetails.get(curLoopId); 
			MethodDetail md = new MethodDetail(str, intList);
			d.methodsInvokedInLoop.put(tmpMethodName, md);
			loopDetails.put(curLoopId,d);
		}
	}

	private void handle2LevelMethods(String str, ArrayList<Integer> intList, String tmpMethodName)
	{
		if(methodSeqs.size() > 1)
		{
			if(summary.get(methodSeqs.get(methodSeqs.size()-2)) == null)
			{
				MethodDetailMap invoked = new MethodDetailMap();
				invoked.put(tmpMethodName, new MethodDetail(str, intList));
				summary.put(methodSeqs.get(methodSeqs.size()-2), new PerMethodSummary(new LoopDetailsMap(), 
						new LoopDepthMap(), invoked , testName, ""));
			}
			else if(summary.get(methodSeqs.get(methodSeqs.size()-2)) != null)
			{
				PerMethodSummary invokeSummary = summary.get(methodSeqs.get(methodSeqs.size()-2));				
				invokeSummary.methodsInvoked.put(tmpMethodName, new MethodDetail(str, intList));

				if(invokeSummary.loopDepth.size() != 0) 
				{
					Set<Integer> itr = invokeSummary.loopDetails.keySet();
					Integer i = itr.iterator().next();
					Detail d1 = invokeSummary.loopDetails.get(i);
					if(d1 != null)
					{
						MethodDetail md1 = new MethodDetail(str, intList);
						d1.methodsInvokedInLoop.put(tmpMethodName, md1);
						invokeSummary.loopDetails.put(i, d1);
					}
					summary.put(methodSeqs.get(methodSeqs.size()-2), invokeSummary);
				}
			}
		}
	}

	/* * * * * * * * * * * *  Method Entry and Exit * * * * * * * * * * * */
	/* * * * * * *line number, method name, parameters, reference method name * * * * * * * * */

	public void methodEnterBefore1(Integer iid, Object name, Object o, Object refMethod) 
	{
		printDebugMsg("methodEnterBefore("+javato.activetesting.analysis.Observer.getIidToLine(iid)+")" + name);
		if(name.toString().indexOf("junit") > -1 ||name.toString().indexOf("<java.io") > -1 ) return;

		if(methodSeqs.isEmpty())
			methodNameForConstructor = name.toString();
		//Constructor Summary
		// Adder methods enable
		if(!name.toString().contains("iterator()"))
		{
			Integer loopCnt = recorder(name.toString(), o, false);
			loopCnts.put(name.toString(), loopCnt);
		}

		if(methodSeqs.size() == 0)	
			curMethodName = name.toString();

		methodSeqs.add(name.toString());
	}

	public void methodExitAfter1(Integer iid, Object name, Object o, Object refMethod) 
	{
		printDebugMsg("methodExitAfter("+javato.activetesting.analysis.Observer.getIidToLine(iid)+")" + name);
		if(name.toString().indexOf("junit") > -1 ||name.toString().indexOf("<java.io") > -1 ) return;

		// Adder methods enable
		if(!name.toString().contains("iterator()"))
			verifyLoopCounts(name.toString(),o);

		//Remove one element from method list.
		methodSeqs.remove(methodSeqs.size() - 1);
		if(methodSeqs.size() == 0)
		{
			//Do not include useless summaries.
			if(loopDetails.size() > 0 || loopDepth.size() > 0 || methodsInvoked.size() > 0)
				summary.put(curMethodName, new PerMethodSummary(loopDetails, loopDepth, methodsInvoked, testName, refMethod.toString()));
			reset();
			refMethod = refMethod.toString();
		}
	}

	private void verifyLoopCounts(String name, Object o)
	{
		HashMap<String, Integer> newMap = new HashMap<String, Integer>();	
		for(String str : loopCnts.keySet())
			if(str.startsWith(o.getClass().getName()) && str.contains(name))
				newMap.put(str, loopCnts.get(str));

		Integer afterLoopCnt = recorder(name, o, true);
		if(afterLoopCnt != -1){
			Integer beforeLoopCnt = loopCnts.get(name.toString());
			if(afterLoopCnt - beforeLoopCnt == 1){
				String className = name.substring(1, name.toString().indexOf(":"));
				HashSet<String> set =  new HashSet<String>();
				if(adderMeths.get(className) != null) 
					set.addAll(adderMeths.get(className));
				set.add(name.toString());
				adderMeths.put(className, set);
			}
		} else {
			for(String str : loopCnts.keySet())
			{
				if(str.startsWith(o.getClass().getName()) && str.contains(name.toString()))
				{
					if(loopCnts.containsKey(str) && newMap.containsKey(str) && loopCnts.get(str) > newMap.get(str)){
						String part1 = str.substring(0, str.indexOf(":"));
						String part2 = str.substring(str.indexOf(":")+1);
						HashSet<String> strs = new HashSet<String>();
						strs.add(part2);
						adderMeths.put(part1, strs);
					}
				}
			}

		}
	}

	/* Tracking number of loops executed after and before each method. (deriving adder methods) */
	public Integer recorder(String name, Object o, boolean flag) 
	{
		java.lang.reflect.Method method = null;
		Integer loopCnt = 0;
		if(o == null) return -1;
		java.lang.reflect.Method[] meths = o.getClass().getMethods();
		if(name.contains("decorated") || name.contains("int size()>")) return 0;
		if(name.contains("Abstract") || name.contains("abstract")) return 0;
		if(name.startsWith("<java.util.List:")) return 0;
		
		for(int counter = 0 ; counter < meths.length; counter++)
		{
			if(meths[counter].toString().contains("size()"))   
			{
				method = meths[counter];
				try {
					loopCnt = (Integer)method.invoke(o,new Object[] {});
				} catch (IllegalAccessException e) {
					System.out.println("Exception 1");
					System.exit(0);
				} catch (InvocationTargetException e) {
					System.out.println("Exception 3");
					System.exit(0);
				}
				return loopCnt;
			}
			else if (!o.getClass().getName().startsWith("java"))
			{
				Options.v().set_no_bodies_for_excluded(true);
				Options.v().set_whole_program(true);
				Options.v().set_allow_phantom_refs(true);
				updateCountsForClassProps(name, o);
				return -1;
			}
		}
		return loopCnt;
	}

	private void updateCountsForClassProps(String name, Object o)
	{
		java.lang.reflect.Field[] fields = o.getClass().getDeclaredFields();
		for(int outer = 0; outer < fields.length; outer++)
		{
			java.lang.reflect.Field field = fields[outer];
			field.setAccessible(true);
			try {
				Object val = (Object)field.get(o);
				if(val == null) continue;
				java.lang.reflect.Method[] innerMeths = val.getClass().getMethods();
				for(int inner = 0; inner < innerMeths.length; inner++)
				{
					if(innerMeths[inner].toString().contains("size()")) 
					{
						int innerLoopCnt = (Integer)innerMeths[inner].invoke(val,new Object[] {});
						String key  = o.getClass().getName()+"."+field.getName() +":" + name; 
						loopCnts.put(key, innerLoopCnt);
					}
				}
			} catch (IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				e1.printStackTrace();
			}catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	/* * * * * * * * *  Loop before and after * * * * * * * * */
	public void loopBefore(Integer iid,  Object o, Integer loopId, Integer idx) 
	{
		printDebugMsg("loop Before("+javato.activetesting.analysis.Observer.getIidToLine(iid)+")" + loopId + " " + idx);

		// Happening in the main function 
		if(methodSeqs.size() == 0) return;

		//other method or uninteresting loop
		String tmpMethodName = methodSeqs.get(methodSeqs.size() - 1);
		if( !curMethodName.equals(tmpMethodName)) {
			Detail d = new Detail(idx, o.toString(), new MethodDetailMap());
			if(summary.get(tmpMethodName) == null)
				initialiseSummary(loopId, d, tmpMethodName);
			else if(summary.get(tmpMethodName) != null)
				updateSummary(loopId, d, tmpMethodName, idx);
			return;
		}

		//This loop details are already added
		if( loopDetails.get(loopId) != null )  return; 

		String name = className.substring(className.lastIndexOf(".")+1, className.length());
		if(!javato.activetesting.analysis.Observer.getIidToLine(iid).contains(name)) return; 

		Detail d  = new Detail(idx, o.toString(), new MethodDetailMap());
		loopDetails.put(loopId, d); 
		if(flag || loopIdxSequence.isEmpty()) flag = false;
		loopIdxSequence.add(idx);
		loopIds.add(loopId);
	}

	private void initialiseSummary(Integer loopId, Detail d, String tmpMethodName)
	{
		LoopDetailsMap lDetails =  new LoopDetailsMap();
		lDetails.put(loopId, d); 
		LoopDepthMap lDepth  = new LoopDepthMap();
		PerMethodSummary perSummary = new PerMethodSummary(lDetails, lDepth, new MethodDetailMap(), tmpMethodName,"");
		summary.put(tmpMethodName, perSummary);
	}

	private void updateSummary(Integer loopId, Detail d, String tmpMethodName, Integer idx)
	{
		PerMethodSummary tmpSummary = summary.get(tmpMethodName);
		MethodDetailMap mInvoked  = tmpSummary.methodsInvoked;
		if(tmpSummary.loopDetails.containsKey(loopId)) return;
		tmpSummary.loopDetails.put(loopId, d);

		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(idx);
		ArrayList<ArrayList<Integer>> listOflist =  new ArrayList<ArrayList<Integer>>();
		listOflist.add(list);
		tmpSummary.loopDepth.put(1,listOflist);
		tmpSummary.methodsInvoked = mInvoked;
		summary.put(tmpMethodName, tmpSummary);
	}

	public void loopAfter(Integer iid,  Object o, Integer loopId, Integer idx) 
	{
		printDebugMsg("loop After("+javato.activetesting.analysis.Observer.getIidToLine(iid)+")" + loopId + " " + idx);
		if(methodSeqs.size() == 0) return;

		//other method or uninteresting loop
		String tmpMethodName = methodSeqs.get(methodSeqs.size() - 1);
		if(!curMethodName.equals(tmpMethodName)) return; 

		String name = className.substring(className.lastIndexOf(".")+1, className.length());
		if(!javato.activetesting.analysis.Observer.getIidToLine(iid).contains(name)) return; 

		if(!flag)
		{	
			loopDepth.clear();
			if(loopIdxSequence.size() > 0)
			{
				if(loopDepth.get(loopIdxSequence.size()) == null)
				{
					ArrayList<ArrayList<Integer>> a  = new ArrayList<ArrayList<Integer>>();
					loopDepth.put(loopIdxSequence.size(), a);
				}
				ArrayList<ArrayList<Integer>> b  = loopDepth.get(loopIdxSequence.size());
				b.add(loopIdxSequence);
				loopDepth.put(loopIdxSequence.size(), b);
				loopIdxSequence = new  ArrayList<Integer>(loopIdxSequence.subList(0, loopIdxSequence.size() - 1));
				loopIds = new ArrayList<Integer> (loopIds.subList(0, loopIds.size() - 1));
			}
			// Already added this nested loop sequence; do not add it again. 
			flag = true;
		}
	}

	/* * * * * * * * * * * * * *  Wrapping up * * * * * * * * * * * */
	public void finish() 
	{
		System.out.println("All the interesting methods are : ");
		System.out.println(adderMeths);
		printDebugMsg("finish()");
		summary.put("constructor", constructorSummary);
		Iterator<String> keys = summary.keySet().iterator();
		while(keys.hasNext())
		{
			String	key = keys.next();
			handleAbnormalLoopExits(key);
		} 

	
		PerMethodSummaryMap localSummary = getLocalSummary();
		ClassSummary cs;
		if(localSummary == null)
			cs = new ClassSummary(summary, new ConstructorInfoMap(), new HashMap<String, String>(), adderMeths);
		else 
			cs = new ClassSummary(localSummary, new ConstructorInfoMap(), new HashMap<String, String>(), adderMeths);

		classSummary.put(className,cs);
		mof.storeObject(classSummary, "Summary.txt");
	}

	/* Complete the summary of key when loop exits abnormally */
	private void handleAbnormalLoopExits(String key)
	{
		System.out.println("==================" + key + "======================");
		PerMethodSummary perSummary = summary.get(key);

		if(perSummary.loopDepth.size() == 0 && perSummary.loopDetails.size() > 0)
		{
			Iterator<Integer> details = perSummary.loopDetails.keySet().iterator();
			while(details.hasNext())
			{
				Detail d = perSummary.loopDetails.get(details.next());
				if(d!=null){
					ArrayList<ArrayList<Integer>> a  = new ArrayList<ArrayList<Integer>>();
					ArrayList<Integer> b = new ArrayList<Integer>();
					b.add(d.idx); a.add(b);
					perSummary.loopDepth.put(1, a);
				}
			}
			System.out.println("Loop exited abnormally.!!");
		}
		summary.put(key, perSummary);
		summary.get(key).print();
	}

	/* Returns the combined existing summary and current summary*/
	private PerMethodSummaryMap getLocalSummary()
	{
		File f = new File(P.summaryFile);
		PerMethodSummaryMap localSummary = null;
		if(f.exists())
		{
			classSummary = mof.returnObject(f.getName());

			adderMeths.putAll(classSummary.get(className).adderMeths);

			localSummary = classSummary.get(className).summary;
			Iterator<String> iterator = summary.keySet().iterator();
			while (iterator.hasNext()) 
			{
				String key = iterator.next().toString();
				if(localSummary.get(key) == null)
					localSummary.put(key, summary.get(key));
				else 
				{
					PerMethodSummary oldS = localSummary.get(key);
					PerMethodSummary newS = summary.get(key);
					if(newS.loopDepth.size() > oldS.loopDepth.size() || 
							(newS.loopDepth.size() == 0 && oldS.loopDepth.size() == 0 
							&& newS.methodsInvoked.size() > oldS.methodsInvoked.size()) 
							||  (!oldS.methodsInvoked.containsKey(P.removeAll)
									&& newS.methodsInvoked.containsKey(P.removeAll)))
						localSummary.put(key, newS);
				}
			}
		}
		return localSummary;
	}

	/* Converts the object list in to the list of integer indices */
	private ArrayList<Integer> createParamList(Object list)
	{
		ArrayList<Integer> intList = new ArrayList<Integer>();
		String s1 = list.toString();
		String s2 = s1.substring(1,s1.length() -1);

		if(s2.length() > 0)
		{
			String[] parts = s2.split(", ");
			Integer i;
			if(!s2.toString().equals(""))
			{
				for (String o : parts)
				{
					try{
						i = Integer.parseInt(o);
					} catch (NumberFormatException e) {
						i = 0;
					}	
					intList.add(i+1);
				}
			}
		}
		return intList;
	}

	private void printDebugMsg(String str)
	{
		if(debug_flag) System.out.println(str);
	}
}
