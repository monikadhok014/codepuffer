/*
 * Provides ways to perform operations on generated callgraph.
 */


package javato.activetesting;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.jimple.toolkits.callgraph.CHATransformer;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.TopologicalOrderer;
import soot.options.Options;

public class PufferCallGraph implements  java.io.Serializable {

	String mainClassName;
	ArrayList<String> listOfDerivedClasses;
	ArrayList<SootClass> listOfClasses = new ArrayList<SootClass>();
	SootClass mainClass;
	
	/* returns the main class of this graph*/
	public SootClass getMainClassName()
	{
		return mainClass;
	}
	
	/*Constructor*/
	public PufferCallGraph(String mainClassName, ArrayList<String> listOfDerivedClasses)
	{
		this.mainClassName = mainClassName;
		this.listOfDerivedClasses = listOfDerivedClasses;
	}
	
	public PufferCallGraph(String mainClassName)
	{
		this.mainClassName = mainClassName;
		this.listOfDerivedClasses = null;
	}

	public void config()
	{
		Options.v().set_no_bodies_for_excluded(true);
		Options.v().set_whole_program(true);
		Options.v().set_allow_phantom_refs(true);
	}

	/*Generates the call graph */
	public CallGraph generateCallGraph(String mainClassName)
	{
		this.config();
		if(listOfDerivedClasses != null)
		{
			SootClass klass = null;
			for(String str:listOfDerivedClasses)
			{
				if(str.contains("$")||str.equals(mainClassName)) continue;
				klass = Scene.v().loadClassAndSupport(str);
				listOfClasses.add(klass);
			}
		}
		
		mainClass = Scene.v().loadClassAndSupport(mainClassName);
		mainClass.setApplicationClass();
		Scene.v().loadNecessaryClasses();
		
		//no inner methods
		ArrayList<SootMethod> entrypoints = getAllMethods(mainClassName);
		Scene.v().setEntryPoints(entrypoints);
		CHATransformer.v().transform();
		CallGraph callGraph = Scene.v().getCallGraph();
		return callGraph;
	}

	public ArrayList<SootClass> getInnerClasses()
	{
		ArrayList<SootClass> innerClasses = new ArrayList<SootClass>();
		
		Iterator<SootClass> klasses = Scene.v().getClasses().iterator();
		while(klasses.hasNext())
		{
			SootClass k = klasses.next();
			if(k.toString().contains(mainClassName))
				innerClasses.add(k);
		}
		return innerClasses;
	}
	/*Generates the call graph */
	public soot.Hierarchy generateHierarchy(String mainClassName)
	{
		if(listOfDerivedClasses != null)
		{
			SootClass klass = null;
			for(String str:listOfDerivedClasses)
			{
				if(str.contains("$")||str.equals(mainClassName)) continue;
				klass = Scene.v().loadClassAndSupport(str);
				listOfClasses.add(klass);
			}
		}
		SootClass mainClass = Scene.v().loadClassAndSupport(mainClassName);
		mainClass.setApplicationClass();
		Scene.v().loadNecessaryClasses();

		soot.Hierarchy h = new soot.Hierarchy();
		return h;
	}
	
	/* Get all the methods of the given class name */
	public ArrayList<SootMethod> getAllMethods(String klassName) 	
	{
		ArrayList<SootMethod> entrypoints = new ArrayList<SootMethod>();
		Scene.v().forceResolve(klassName, SootClass.SIGNATURES);
		SootClass klass = Scene.v().getSootClass(klassName);

		for (SootMethod m : klass.getMethods()) {
			if (!m.isAbstract()) {
				entrypoints.add(m);
			}
		}
		return entrypoints;
	}

	public List<SootMethod> getTopologicalOrder(CallGraph g)
	{
		TopologicalOrderer to = new TopologicalOrderer(g);
		to.go();
		return to.order();
	}

	/* Returns soot method corresponding to methodName in class: mainClassName*/
	public SootMethod getMethod(String methodName, String mainClassName)
	{
		SootMethod method = null;
		ArrayList<SootMethod> entrypoints = getAllMethods(mainClassName);
		for(SootMethod tmpMethod : entrypoints)	
		{
			if(methodName.equals(tmpMethod.toString()))	
			{
				method = tmpMethod;
				break;
			}
		}
		return method;
	}

	/* Finds appropriate constructor to invoke the method*/
	public SootClass getAppropriateConstructor(SootMethod method)
	{
		SootClass klass = null;
		for(SootClass kclass:listOfClasses)
		{
			List<SootMethod> meths = kclass.getMethods();
			if(meths == null) return null;
			if(kclass.getSuperclass() != null 
					&& kclass.getSuperclass().toString().equals(mainClassName) 
						&& !meths.contains(method))
			{
				if(kclass.isPublic())
					return kclass;
			}
		}
		return null;
	}

}
