/*
 * Maintains the summary of each method 
 * MethodName ->  PerMethodSummary
 * PerMethodSummary -> HashMap<Integer, ArrayList<ArrayList<Integer>>> loopDepth; : list of lists of loopIds for specific depth
 *						HashMap<String, MethodDetail> methodsInvoked; : MethodDetail has base object and list of indices for inputs  
 * 						HashMap<Integer, Detail> loopDetails;
 */

package javato.activetesting;

import java.util.*;

import javato.activetesting.T.Detail;
import javato.activetesting.T.LoopDepthMap;
import javato.activetesting.T.LoopDetailsMap;
import javato.activetesting.T.MethodDetail;
import javato.activetesting.T.MethodDetailMap;

public class PerMethodSummary implements java.io.Serializable{

	LoopDepthMap loopDepth;
	MethodDetailMap methodsInvoked;   
	LoopDetailsMap loopDetails;
	String testName = ""; //test number used to derive this summary. 
	String refName = ""; //Create object using this method if class is abstract.

	//Constructor to create the object.
	public PerMethodSummary(LoopDetailsMap loopDetails, LoopDepthMap loopDepth, MethodDetailMap methodsInvoked, 
						String testName, String refName) {
		this.loopDepth = loopDepth;
		this.loopDetails = loopDetails;
		this.methodsInvoked = methodsInvoked;
		this.testName = testName; 
		this.refName = refName; 
	}

	// Prints the current summary object.
	public void print() {
		System.out.println("\n============================================================\n");
		System.out.println(" Test used is : " + testName);
		System.out.println("Ref name is :" + refName);
		System.out.println(" ------------------------ Loop Depth --------------------------");
		System.out.println(loopDepth);
		System.out.println(" --------------------Methods Invoked ---------------------------");
		Iterator iterator = methodsInvoked.keySet().iterator();
		while (iterator.hasNext()) {
			String key3 = iterator.next().toString();
			MethodDetail value3 = methodsInvoked.get(key3);

			System.out.println(key3 + " type " + value3.type + " list params  " + value3.list);
		}
		System.out.println(" ---------------------- Loop Details ----------------------------");


		Iterator iterator1 = loopDetails.keySet().iterator();
		while (iterator1.hasNext()) {
			
			Integer key4 = Integer.parseInt(iterator1.next().toString());
			Detail value4 = loopDetails.get(key4);

			System.out.println("+++++++++++++++++++++ NEW ENTRY ++++++++++++++++++++++++++++");
			System.out.println(key4 + " " + value4);
			System.out.println("idx " + value4.idx + " var " + value4.type);
			System.out.println(" ----- Methods Invoked From Loop ----");
			Iterator iterator2 = value4.methodsInvokedInLoop.keySet().iterator();
			while (iterator2.hasNext()) {
				String key5 = iterator2.next().toString();
				MethodDetail value5 = value4.methodsInvokedInLoop.get(key5);

				System.out.println(key5+ " type " + value5.type+ " list params  "+ value5.list);
			}
			System.out.println("+++++++++++++++++++++ ENTRY ENDED +++++++++++++++++++++++++++");
		}
	}

}
