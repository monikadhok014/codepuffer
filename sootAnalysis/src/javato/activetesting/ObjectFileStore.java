/* 
 *  Provides ways to store the generated summaries in to files. 
 */

package javato.activetesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;

import javato.activetesting.T.ClassSummaryMap;
import javato.activetesting.T.PerfTest;

public class ObjectFileStore {

	//Stores the given summary in to specified file.
	public void storeObject(ClassSummaryMap summary, String fileName){
		OutputStream ops = null;
		ObjectOutputStream objOps = null;
		ClassSummaryMap localSummary = new ClassSummaryMap();
		try {
			File f = new File(fileName);
			if(!f.exists())
			{
				f.createNewFile();
				localSummary = summary;
			}
			else
			{
				InputStream fileIs = null;
				ObjectInputStream objIs = null;
				fileIs = new FileInputStream(fileName);
				objIs = new ObjectInputStream(fileIs);
				try {
					localSummary = (ClassSummaryMap) objIs.readObject();
					Iterator<String> iterator = summary.keySet().iterator();
					while (iterator.hasNext()) {
						String key3 = iterator.next().toString();
						System.out.println(key3);
						localSummary.put(key3, summary.get(key3));	
					}

				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			ops = new FileOutputStream(fileName);
			objOps = new ObjectOutputStream(ops);
			objOps.writeObject(localSummary);
			objOps.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{
				if(objOps != null) objOps.close();
			} catch (Exception ex){

			}
		}
	}

	// Displays the summary object from the specified file
	public void displayObjects(String fileName){

		ClassSummaryMap localSummary = this.returnObject(fileName);
		Iterator<String> iterator = localSummary.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next().toString();
			System.out.println("\n===============" + key + "==============");
			ClassSummary value3 = localSummary.get(key);
			value3.print();
		}
	}

	//Returns the summary object from the specified file.
	public ClassSummaryMap returnObject(String fileName){

		InputStream fileIs = null;
		ObjectInputStream objIs = null;
		try {
			fileIs = new FileInputStream(fileName);
			objIs = new ObjectInputStream(fileIs);
			return (ClassSummaryMap) objIs.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if(objIs != null) objIs.close();
			} catch (Exception ex){

			}
		}
		return null;
	}

	//Stores the given summary in to specified file.
	public void store(ClassSummaryMap summary, String fileName){

		OutputStream ops = null;
		ObjectOutputStream objOps = null;
		try {
			ops = new FileOutputStream(fileName);
			objOps = new ObjectOutputStream(ops);
			objOps.writeObject(summary);
			objOps.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{
				if(objOps != null) objOps.close();
			} catch (Exception ex){

			}
		}
	}

	public void init(String fileName)
	{
		OutputStream ops = null;
		ObjectOutputStream objOps = null;
		HashMap<String, PerfTest> localTests = new HashMap<String, PerfTest>();
		try {
			File f = new File(fileName);
			if(!f.exists()) { 
				f.createNewFile();
				PerfTest localPT = new PerfTest(0, 1);
				localTests.put("<org.apache.commons.collections4.collection.AbstractCollectionDecorator: boolean retainAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Set: boolean retainAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Set: boolean removeAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Set: boolean containsAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.List: boolean retainAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Collection: boolean retainAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Collection: boolean removeAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Collection: boolean containsAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.List: boolean removeAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.List: boolean containsAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.AbstractCollection: boolean containsAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.AbstractCollection: boolean retainAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.AbstractCollection: boolean removeAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Vector: boolean removeAll(java.util.Collection)>", localPT); 
				localTests.put("<java.util.Vector: boolean retainAll(java.util.Collection)>", localPT);
				localTests.put("<java.util.Vector: boolean containsAll(java.util.Collection)>", localPT);
				ops = new FileOutputStream(fileName);
				objOps = new ObjectOutputStream(ops);
				objOps.writeObject(localTests);
				objOps.flush();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{
				if(objOps != null) objOps.close();
			} catch (Exception ex){

			}
		}
	}
	//Stores the given method with input indices in to specified file.
	public void storePerfTest(String methodName, PerfTest pt, String fileName){
		OutputStream ops = null;
		ObjectOutputStream objOps = null;
		HashMap<String, PerfTest> localTests = new HashMap<String, PerfTest>();
		try {
			File f = new File(fileName);
			if(!f.exists())
			{
				f.createNewFile();
				localTests.put(methodName, pt);
			}
			else
			{
				InputStream fileIs = null;
				ObjectInputStream objIs = null;
				fileIs = new FileInputStream(fileName);
				objIs = new ObjectInputStream(fileIs);
				try {
					localTests = (HashMap<String, PerfTest>) objIs.readObject();
					localTests.put(methodName, pt);

				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			ops = new FileOutputStream(fileName);
			objOps = new ObjectOutputStream(ops);
			objOps.writeObject(localTests);
			objOps.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{
				if(objOps != null) objOps.close();
			} catch (Exception ex){

			}
		}
	}

	// Displays the perfTests object from the specified file
	public void displayPerfTests(String fileName){

		HashMap<String, PerfTest> localTests = this.returnPerfTests(fileName);
		Iterator<String> iterator = localTests.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next().toString();
			System.out.println("\n=============" + key + "=============================");
			PerfTest value3 = localTests.get(key);
			value3.print();
		}
	}

	//Returns the perfTests object from the specified file.
	public HashMap<String, PerfTest> returnPerfTests(String fileName){

		InputStream fileIs = null;
		ObjectInputStream objIs = null;
		try {
			fileIs = new FileInputStream(fileName);
			objIs = new ObjectInputStream(fileIs);
			return (HashMap<String, PerfTest>) objIs.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if(objIs != null) objIs.close();
			} catch (Exception ex){

			}
		}
		return null;
	}
}
