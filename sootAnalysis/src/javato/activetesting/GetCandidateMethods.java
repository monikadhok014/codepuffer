/*
 *  Performs call graph traversal and finds methods with possible potential 
 *  inefficient methods. 
 */

package javato.activetesting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javato.activetesting.T.AdderMethsMap;
import javato.activetesting.T.LoopDepthMap;
import javato.activetesting.T.LoopDetailsMap;
import javato.activetesting.T.MethodDetail;
import javato.activetesting.T.MethodDetailMap;
import javato.activetesting.T.PerMethodSummaryMap;
import javato.activetesting.T.PerfTest;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.jimple.toolkits.callgraph.Targets;

public class GetCandidateMethods {

	boolean debug_flag = false;
	static CallGraph cg;
	ClassSummary details;
	static PufferCallGraph graph;
	static SootClass klass;

	PerMethodSummaryMap emp = new PerMethodSummaryMap();
	AdderMethsMap adderMeths =  new AdderMethsMap();
	ObjectFileStore mof = new ObjectFileStore();

	public static void main(String[] args) {
		GetCandidateMethods driver = new GetCandidateMethods();

		driver.updateDataStructures("revisedSummary.txt");
		//driver.updateDataStructures("BaseSummaries.txt");

		ArrayList<String> subList = new ArrayList<String>(Arrays.asList(args));
		graph = new PufferCallGraph(args[0], subList);
		cg = graph.generateCallGraph(args[0]);
		klass = graph.getMainClassName();
		
		if(klass.getName().startsWith("org.apache.lucene"))
			driver.handleConstructorMethods();
		driver.computeAndProcessAllMethodsOfClass();

		ArrayList<SootClass> list = graph.getInnerClasses();
		for(SootClass k:list.subList(0, list.size()))
		{
			subList = new ArrayList<String>(Arrays.asList(args));
			graph = new PufferCallGraph(k.toString(), subList);
			cg = graph.generateCallGraph(k.toString());
			klass = graph.getMainClassName();

			driver.computeMethodsInTopOrder();
		} 
		
	}

	private void handleConstructorMethods()
	{
		printDebugMsg("checkIfCalleeHasNestedLoopForFixedMeths");
		mof.init(P.methodsFile);
		HashMap<String, PerfTest> pTests =  mof.returnPerfTests(P.methodsFile);
		Set<String> methods = pTests.keySet();
		if(!emp.containsKey("constructor")) return;
		PerMethodSummary constructorSummary = emp.get("constructor");
		Set<String> methsOfConstructor = constructorSummary.methodsInvoked.keySet();
	
		for(String mName:methods)
			if(methsOfConstructor.contains(mName))
			{
				printDebugMsg("calls" + mName);
				int idx1 = pTests.get(mName).idx1;
				int idx2 = pTests.get(mName).idx2;
				SootMethod meth = null;
				printDebugMsg("The class name is " + klass);
				for (SootMethod m : klass.getMethods())
				{
					
					printDebugMsg("\t\t " + m);
					if(m.isConstructor()){
							meth = m; break;}
				}
				printDebugMsg("inside handleConstructorMethod " + meth);
				if(meth != null)
					writeInput(meth, meth.getDeclaringClass().toString(), 2, 3, false);
				break;
			}
		
	}
	
	/* Update the data structure */
	private void updateDataStructures(String filename){
		Iterator<String> tmp1 = mof.returnObject(filename).keySet().iterator();

		while (tmp1.hasNext()) {
			String key3 = tmp1.next().toString();
			ClassSummary value3 = mof.returnObject(filename).get(key3);
			adderMeths.putAll(value3.adderMeths);
			emp.putAll(value3.summary);
			if(!filename.equals("BaseSummaries.txt"))
				details = value3;
		}
	}

	/* * * * * * * * * * * * * * * * * * * * Handle inheritance * * * * * * * * * * * * * * * */
	private void computeAndProcessAllMethodsOfClass()
	{
		printDebugMsg("computeAndProcessAllMethodsOfClass");
		ArrayList<SootMethod> methsOfClass = new ArrayList<SootMethod>();		
		methsOfClass.addAll(klass.getMethods());
		SootClass tmpClass = klass;
		while(tmpClass.hasSuperclass())
		{
			tmpClass = tmpClass.getSuperclass();
			methsOfClass = club((ArrayList<SootMethod>)tmpClass.getMethods(), methsOfClass);
		}
		methsOfClass.removeAll(klass.getMethods());
		checkIfCalleeHasNestedLoopForFixedMeths(methsOfClass);
	}

	/* Check if any of the meths method contains nested loop */
	private void checkIfCalleeHasNestedLoopForFixedMeths(ArrayList<SootMethod> meths)
	{
		printDebugMsg("checkIfCalleeHasNestedLoopForFixedMeths");
		mof.init(P.methodsFile);
		HashMap<String, PerfTest> pTests =  mof.returnPerfTests(P.methodsFile);
		Set<String> methods = pTests.keySet();
		ArrayList<String> methsStr = new ArrayList<String>();
		for(SootMethod m: meths)
			methsStr.add(m.toString());
		for(String mName:methods)
			if(methsStr.contains(mName))
			{
				int idx1 = pTests.get(mName).idx1;
				int idx2 = pTests.get(mName).idx2;
				inputGenerator(meths.get(methsStr.indexOf(mName)), idx1, idx2, true);
			}
	}

	/* Handle inheritance :  clubs methods from l1 to l2 if not present */
	private static ArrayList<SootMethod> club(ArrayList<SootMethod> l1, ArrayList<SootMethod> l2)
	{
		ArrayList<SootMethod> list = new ArrayList<SootMethod>(l2);
		for(Iterator<SootMethod> listItr1 = l1.iterator(); listItr1.hasNext();)
		{
			SootMethod tmp = listItr1.next();
			Boolean valid = true;
			for(Iterator<SootMethod> listItr2 = l2.iterator(); listItr2.hasNext();)
			{
				if(!listItr2.next().getName().equals(tmp.getName())) valid &= true;
				else valid = false;
			}
			if(valid) { list.add(tmp); }
		}
		return list;
	}

	/* * * * * * * * * * * * * * * Calling for each method in topological reverse order. * * * * * * * * */
	private void computeMethodsInTopOrder()
	{
		Iterator<SootMethod> meth_iterator = graph.getTopologicalOrder(cg).iterator(); 
		while (meth_iterator.hasNext()) 
		{
			SootMethod meth = meth_iterator.next();

			if(!meth.toString().contains("<java") && !meth.toString().contains("<sun")
					&& !meth.toString().contains("<init>") 
					&& meth.toString().contains(klass.toString()+":"))
				getValidCandidates(meth);
		}
	}

	/* Check if tmp has nested loop? */
	private void getValidCandidates(SootMethod tmp){
		printDebugMsg("***************" + tmp + "*****************");
		PerMethodSummary perMethodSummary = emp.get(tmp.toString());
		if(perMethodSummary != null)
		{
			if(perMethodSummary.loopDepth.size() == 0)
				checkIfCalleeHasNestedLoop(perMethodSummary, tmp);
			else if(perMethodSummary.loopDepth.size() == 1 && 
				   (tmp.getDeclaringClass().getName().startsWith("groovy.util") || 
					tmp.getDeclaringClass().getName().startsWith("org.apache.pdfbox.pdmodel.common.COSArrayList"))){
				checkIfCalleeHasNestedLoop(perMethodSummary, tmp);
			}
			else analyseLoops(perMethodSummary, tmp);
		}
	}

	/* Check of tmp calls a method already present in the fixed list of methods */
	private boolean checkIfCalleeHasNestedLoop(PerMethodSummary perMethodSummary, SootMethod tmp)
	{
		printDebugMsg("checkIfCalleeHasNestedLoop");
		mof.init(P.methodsFile);
		Set<String> methods = mof.returnPerfTests(P.methodsFile).keySet();
		MethodDetailMap invokedmethods = perMethodSummary.methodsInvoked;
		for(String methodName:methods)
		{
			if(invokedmethods.containsKey(methodName))
			{
				noLoopButMethodWithNestedLoop(tmp, invokedmethods, methodName);
				return true;
			}
		}
		return false;
	}

	private void noLoopButMethodWithNestedLoop(SootMethod tmp, MethodDetailMap invoked, String macro)
	{
		ObjectFileStore mof = new ObjectFileStore();
		MethodDetail d = invoked.get(macro);
		int val, val1, val2;
		val = mof.returnPerfTests(P.methodsFile).get(macro).idx1;
		if(val > 0)
			val1 = d.list.get(val-1);
		else 
			val1 = 0;
		val = mof.returnPerfTests(P.methodsFile).get(macro).idx2;
		if(val > 0)
			val2 = d.list.get(val-1);
		else 
			val2 = 0;
		if(tmp.getParameterCount() > 1)
			inputGenerator(tmp, 1, 2, true);
		else if (val1 == 0 && val1 == val2)
			inputGenerator(tmp, val1, 1, true); //Hack for collect.maps
		else
			inputGenerator(tmp, val1, val2, true);
	}

	private void analyseLoops(PerMethodSummary perMethodSummary, SootMethod tmp){
		printDebugMsg("Analysing loops !!");
		LoopDepthMap loopDepth = perMethodSummary.loopDepth;
		for(Iterator<Integer> li = loopDepth.keySet().iterator();li.hasNext();)
		{
			Integer key3 = Integer.parseInt(li.next().toString());
			if(key3 > 1)
				printVal(tmp, null, loopDepth.get(key3), null);
			else if(key3 == 1)
				getMultiLevelLoop(tmp, perMethodSummary);
		}
	}

	/* tmp calls a method which further has loop? */
	private void getMultiLevelLoop(SootMethod tmp, PerMethodSummary summary){
		printDebugMsg("Inside multilevel loops");
		LoopDetailsMap loopdetails = summary.loopDetails;
		for (Iterator<Integer> di = loopdetails.keySet().iterator(); di.hasNext();)
		{
			Integer d = Integer.parseInt(di.next().toString());
			MethodDetailMap methodsinloop = loopdetails.get(d).methodsInvokedInLoop;
			if(methodsinloop.size() > 0){
				for (Iterator<String> mi = methodsinloop.keySet().iterator(); mi.hasNext();)
				{
					String mName = mi.next().toString(); //methodName is invoked from the loop.
					if(mName.startsWith("<java.lang.Object: ")) continue;
					if(emp.get(mName) == null || emp.get(mName).loopDepth.get(1) == null)
					{
						printDebugMsg("Method called from loop has no loop");
						if(handleLoopMethodNoLoop(mName, tmp, methodsinloop, tmp) == null)
						{
							SootMethod tmpMethod = graph.getMethod(mName, klass.toString());
							int index = traverseTheChain(tmpMethod, emp.get(mName), tmp);
							if(index > -1)  inputGenerator(tmp, index, 0, true);
						}
					}
					else if(emp.get(mName)!= null && emp.get(mName).loopDepth.get(1) != null) 
					{    
						printDebugMsg("Method called from loop has loop" + mName + tmp);
						MethodDetail d1 = methodsinloop.get(mName);
						List<Integer> val = d1.list;
						writeInput(tmp, mName, methodsinloop, null, "", val.get(0));
					}
				}
			} 
		}
	}

	private Integer traverseTheChain(SootMethod tmp, PerMethodSummary summary, SootMethod tMethod){
		if(tmp == null || tMethod == null || summary == null ) return -1;

		MethodDetailMap methodsinloop = summary.methodsInvoked;
		if(methodsinloop == null || methodsinloop.keySet().size() == 0) 
			return -1;

		for (Iterator<String> mi = methodsinloop.keySet().iterator(); mi.hasNext();)
		{
			String mName = mi.next().toString(); 
			if(mName.contains(P.arraycopy)) return 0;

			if(emp.get(mName) == null || emp.get(mName).loopDepth.get(1) == null)
			{
				printDebugMsg("Method called from loop has no loop");
				if(handleLoopMethodNoLoop(mName, tmp, methodsinloop, tMethod) == null){
					SootMethod tmpMethod = graph.getMethod(mName,  klass.toString());
					if(emp.get(mName) != null && tmpMethod != null) {
						if(mName.equals(tmp.toString())) return -1;	
						if(traverseTheChain(tmpMethod, emp.get(mName), tMethod) == 0) 
							return 0;
					}
				}
			}
		}
		return -1;
	}

	private SootMethod handleLoopMethodNoLoop(String mName, SootMethod tmp, MethodDetailMap methodsinloop, SootMethod tMethod)
	{
		printDebugMsg("\n \t \t \t ========= Before get multi level loop ==========");
		SootMethod m = findSibling(mName, tmp);
		printDebugMsg("\t \t \t =========== After get multi level loop ==========" + m);

		if(m!= null)
			generateInput(tmp , mName, methodsinloop ,m.toString(), tMethod);
		else if(P.arrayMethods.contains(mName))
		{
			generateInput(tmp , mName, methodsinloop ,mName, tMethod);
		}
		return m;
	}

	private String siblingPartOne(String str)
	{
		return str.substring(str.indexOf(":") + 1, str.indexOf(">"));
	}

	private String siblingPartTwo(String str)
	{
		return str.substring(0, str.substring(0, str.indexOf(":")).lastIndexOf("."));
	}

	private SootMethod findSibling(String methodName, SootMethod tmp){
		printDebugMsg("\t \t \tFinding siblings of : " + methodName);
		if(methodName.contains("java.util.Iterator") || tmp == null) return null;
		for(Iterator tars = new Targets(cg.edgesOutOf(tmp)); tars.hasNext();)
		{
			SootMethod target = (SootMethod) tars.next();
			if(methodName.equals(target.toString())) continue;

			if(!target.toString().contains("java.util.Iterator iterator()>")  && 
					!target.toString().contains("boolean hasNext()>") 
					&& !target.toString().contains("java.lang.Object next()>")){
				String t = siblingPartOne(target.toString());
				String m = siblingPartOne(methodName);
				String t1 = siblingPartTwo(target.toString());
				String m1 = siblingPartTwo(methodName);

				if(t.equals(m) && t1.equals(m1)){
					if(P.arrayMethods.contains(target.toString()))
						return returnTarget(methodName, target);
					else if(emp.get(target.toString()) != null)
					{
						MethodDetailMap mInvoked = emp.get(target.toString()).methodsInvoked;
						if(emp.get(target.toString()).loopDepth.get(1) != null || 
								mInvoked.containsKey(P.arraycopy))
							return returnTarget(methodName, target);
					}
				}	
			}
		}
		printDebugMsg("\t \t \tEnd of resultFromChildSibling_loop ");
		return null;
	}

	/* * * * * * * * Related to test generation process * * * * * * * * */
	private void inputGenerator(SootMethod tmp, int index1, int index2, boolean hasNoLoop)
	{
		printDebugMsg("inputGenerator - 326");
		if(details.constructorPropMap.keySet().size() == 0)
		{
			String constructorName = null;
			if(!tmp.isStatic())
			{
				SootClass klass = graph.getAppropriateConstructor(tmp);
				if(klass != null)
					constructorName = klass.toString();
				else
					constructorName = emp.get(tmp.toString()).refName;
			}

			if(constructorName != null)
				writeInput(tmp, constructorName.toString() ,index1, index2, tmp.isStatic());
			else
				writeInput(tmp, tmp.getDeclaringClass().toString(), index1, index2, tmp.isStatic());
		}

		//Single constructor with loop
		else if(details.constructorPropMap.keySet().size() == 1)
		{
			printDebugMsg("================================ 2222");
			writeInput(tmp, klass.toString(), index1, index2, tmp.isStatic());
		}
		else if(hasNoLoop)
		{
			printDebugMsg("================================ 3333");
			writeInput(tmp, klass.toString(), index1, index2, tmp.isStatic());
		}
		else
		{//Constructor with constraints.
			printDebugMsg("================================ 4444");
			for(String str:details.constructorPropMap.keySet())
				if(details.constructorPropMap.get(str).constraints.size() == 0)
				{
					int val = emp.get(tmp.toString()).loopDepth.get(1).get(0).get(0);
					writeInput(tmp, str, val, index1, tmp.isStatic());
				}
		}
	}

	private void writeInput(SootMethod tmp, String mName, MethodDetailMap methodsinloop, String m, String str,  Integer index)
	{
		boolean isStatic = tmp.isStatic();
		PerMethodSummary summary = emp.get(tmp.toString());
		if(summary.loopDepth.size() <= 0) return;
		int val = summary.loopDepth.get(1).get(0).get(0);
		GenInputs input = new GenInputs(details, getPublicCaller(tmp), adderMeths, klass.toString());
		Integer idx1 = (index == null)? 0: index;
		if(m == null) 
		{
			String thisType = generateThisType(tmp, val, summary);
			if(summary.refName.length() != 1 && idx1 == 0)
				input.generate_multiLevel(idx1, summary.refName, val, P.ARRAY, isStatic, true, "360", thisType);
			else
				input.generate_multiLevel(idx1, P.ARRAY, val, P.ARRAY, isStatic, true, "360", thisType);
		}
		else if(P.arrayMethods.contains(m.toString()))
			calleePresentInArrayMeths(mName, methodsinloop, summary, tmp, isStatic, input);
		else
		{
			mof.storePerfTest(tmp.toString(), new PerfTest(val,idx1), P.methodsFile);
			input =  new GenInputs(details, tmp,adderMeths, str);
			input.generate(val, isStatic, false);
		}
	}

	private void calleePresentInArrayMeths(String mName, MethodDetailMap methodsinloop, PerMethodSummary summary, SootMethod tmp, boolean isStatic, GenInputs input)
	{
		printDebugMsg("calleePresentInArrayMeths");
		int val = summary.loopDepth.get(1).get(0).get(0);
		MethodDetail dt = methodsinloop.get(mName);
		if(isInteger(dt.type) &&  Integer.parseInt(dt.type) >= 0)
		{
			int idx  = Integer.parseInt(dt.type) + 1;
			if(summary.refName.length() > 2)
				input =  new GenInputs(details, tmp, adderMeths, summary.refName);

			if(idx == 0)
				writeInputExtender(tmp, idx, P.ARRAY, val, P.ARRAY, isStatic, "445", input);
			else
				writeInputExtender(tmp, val, P.ARRAY, idx, P.ARRAY, isStatic, "445", input);
		}
		else if(summary.loopDepth != null && summary.loopDepth.get(1) != null 
				&& summary.loopDepth.get(1).get(0) != null)
			writeInputExtender(tmp, 0, summary.refName, val, P.ARRAY, isStatic, "379", input);
	}

	private void generateInput(SootMethod tmp, String mName, MethodDetailMap methodsinloop, String m, SootMethod tMethod)
	{
		if( emp.get(tmp.toString()).methodsInvoked.get(mName).type.contains("this"))
			generateTest(mName, tmp, tMethod);
		else 
		{
			if(details.constructorPropMap.keySet().size() == 0)
			{
				String constructorName = null;
				if(!tmp.isStatic())
				{
					if(graph.getAppropriateConstructor(tmp) != null)
						constructorName = graph.getAppropriateConstructor(tmp).toString();
					else if (emp.get(mName) != null)
						constructorName = emp.get(mName).refName;
				}
				writeInput(tmp, mName, methodsinloop, m, null, null);
			}
			else if(details.constructorPropMap.keySet().size() == 1)
				writeInput(tMethod,mName, methodsinloop ,m, klass.toString(), null);
			else 
				for(String str:details.constructorPropMap.keySet())
					if(details.constructorPropMap.get(str).constraints.size() == 0)
						writeInput(tmp,  mName, methodsinloop, m, str, null);
		}
	}

	//(//yacha sibling interesting, //methodName called from tmp, //top method)
	private void generateTest(String methodName, SootMethod tmp, SootMethod topMethod)
	{
		PerMethodSummary summary= emp.get(tmp.toString()); 
		String name =  summary.methodsInvoked.get(methodName).type.toString(); 

		String propertyName = name.substring(name.lastIndexOf(" ")+1, name.length()-1);
		for(String str:details.classPropMap.keySet())
			if(propertyName.equals(str))
			{
				GenInputs input =  new GenInputs(details, tmp,adderMeths, str);
				writeInputExtender(topMethod, 0, str, 1, P.ARRAY, topMethod.isStatic(), "491",input);
			}
	}

	private void writeInputExtender(SootMethod tmp, int v1, String s1, int v2, String s2, boolean isStatic, String line, GenInputs input)
	{
		printDebugMsg( " \n\n\n\t\t\t*************************************** ");
		printDebugMsg(" \t\t\t" +tmp);
		printDebugMsg( " \t\t\t********************************************* ");
		mof.storePerfTest(tmp.toString(), new PerfTest(v1, v2), P.methodsFile);
		input.generate_multiLevel(v1, s1, v2, s2, isStatic, false, line, "");
	}

	/* Method to be invoked, Constructor name, first index and second index */
	private void writeInput(SootMethod tmp, String str, int idx1, int idx2, boolean isStatic)
	{
		printDebugMsg("write input 517");
		GenInputs input =  new GenInputs(details, tmp,adderMeths, str);
		if(idx1 == 0)
			writeInputExtender(tmp, idx1, str, idx2, P.ARRAY, isStatic, "443",input);
		else
			writeInputExtender(tmp, idx1,P.ARRAY, idx2, P.ARRAY, isStatic, "445",input);
	}

	private SootMethod getPublicCaller(SootMethod tmp)
	{
		if(tmp.isPublic()) return tmp;
		Iterator<Edge> ed = cg.edgesInto(tmp);
		if(ed.hasNext())
		{
			Edge e = ed.next();
			return e.getSrc().method();
		}
		return null;
	}

	private String generateThisType(SootMethod tmp, int val, PerMethodSummary summary)
	{
		mof.storePerfTest(tmp.toString(), new PerfTest(0, val), P.methodsFile);
		Integer in = summary.loopDetails.keySet().iterator().next();
		String type = summary.loopDetails.get(in).type;
		if(type.length() == 0) return "";
		String thisType = type.substring(type.indexOf("<") + 1, type.indexOf(":")) + "." +
				type.substring(type.lastIndexOf(" ") + 1, type.length()-1);
		if(type.contains("java.util.ListIterator") || type.contains("java.util.List")) //second one for groovy
			return "";
		return thisType;
	}

	/* * * * * * * * * * * * Helper methods * * * * * * * * * */
	private void printPerfTests()
	{
		for(String str: mof.returnPerfTests(P.methodsFile).keySet())
		{
			printDebugMsg(str);
			mof.returnPerfTests(P.methodsFile).get(str).print();
		} 
	}

	private void printVal(SootMethod tmp, String type, ArrayList<ArrayList<Integer>> val, String target )
	{
			printDebugMsg("\n**************  Sibling case *****************");
			printDebugMsg(" Candidate method Name: "+ tmp);
			printDebugMsg(" Special type is: " + type + " of " + target);
			printDebugMsg(" Indices are: " + val);
			ArrayList<Integer> val0 = val.get(0);
			int val1 = val0.get(0);
			int val2 = val0.get(1);

			GenInputs input = new GenInputs(details, getPublicCaller(tmp), adderMeths, klass.toString());
			input.generate_multiLevel(val1, P.ARRAY, val2, P.ARRAY, tmp.isStatic(), true, "360", "");
			printDebugMsg("*************************************************");
	}

	private SootMethod returnTarget(String methodName, SootMethod target)
	{
		printDebugMsg("\t \t \t *****************Candidate siblings are :**********");
		printDebugMsg("\t \t \t Started with original :" + methodName);
		printDebugMsg("\t \t \t Found the candidate :" + target);
		printDebugMsg("\t \t \tEnd of resultFromChildSibling_loop ");
		return target;
	}

	private static boolean isInteger(String s) {
		try { 
			Integer.parseInt(s); 
		} catch(NumberFormatException e) { 
			return false; 
		} catch(NullPointerException e) {
			return false;
		}
		return true;
	}

	private void printDebugMsg(String str)
	{
		if(debug_flag) System.out.println(str);
	}
}
