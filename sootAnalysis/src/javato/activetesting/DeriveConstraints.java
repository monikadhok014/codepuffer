/* Computes: 
 * 			i) ClassPropMap : Map of "class property name" to the corresponding "types"
 * 			ii) constructorPropMap :  Map of "constructor method" to "ConstructorInfo"
 * 					ConstructorInfo contains 
 * 									i) constraints in the constructor 
 * 									ii) map of class property to parameters
 * 									iii) list of parameter types
 */	

package javato.activetesting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javato.activetesting.T.ClassSummaryMap;
import javato.activetesting.T.ConstructorInfo;
import javato.activetesting.T.ConstructorInfoMap;
import soot.Body;
import soot.PatchingChain;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;

public class DeriveConstraints {

	static SootClass mainClass;
	static CallGraph cg;
	boolean debug_flag = false;

	HashMap<Value, Value> defUse = new HashMap<Value, Value>(); 
	static ConstructorInfoMap constructorPropMap = new ConstructorInfoMap();
	static ArrayList<Body> methBodies = new ArrayList<Body>();

	public static void main(String[] args) {
		DeriveConstraints driver = new DeriveConstraints();
		PufferCallGraph graph =  new PufferCallGraph(args[0]);
		cg = graph.generateCallGraph(args[0]);
		mainClass = Scene.v().loadClassAndSupport(args[0]);
		
		driver.deriveConstructorInfo(graph.getTopologicalOrder(cg));
		updateClassSummary(updateClassProps(graph));
		//print();
	}

	private static HashMap<String, String> updateClassProps(PufferCallGraph graph){
		
		HashMap<String, String> classPropMap = new HashMap<String, String>();
		
		//Deriving class properties type constraints.
		Iterator<SootField> iterator1 = mainClass.getFields().iterator();
		while (iterator1.hasNext()) {
			SootField field = iterator1.next();
			String fieldName = field.getName();
			classPropMap.put(fieldName, field.getType().toString());
		}		
		return classPropMap;
	}

	private static void print(){
		for(String str:constructorPropMap.keySet())
		{
			System.out.println("=========================================");
			System.out.println(str);
			System.out.println("\t \t   Constraints: " + constructorPropMap.get(str).constraints);  
			System.out.println("\t \t   Constraints indices: " + constructorPropMap.get(str).constraints_indices);  
			System.out.println("\t \t   Parameters are: " + constructorPropMap.get(str).parameterTypes);
		}
	}

	//Update the class summary stored with constructor details.
	private static void updateClassSummary(HashMap<String, String> classPropMap){
		ObjectFileStore mof = new ObjectFileStore();

		ClassSummaryMap classSummary = mof.returnObject("Summary.txt");
		ClassSummaryMap summary =  new ClassSummaryMap();

		for(Iterator<String> iterator = classSummary.keySet().iterator(); iterator.hasNext(); )
		{
			String key3 = iterator.next().toString();
			ClassSummary cs = classSummary.get(key3);
			ClassSummary cs_new = new ClassSummary(cs.summary, constructorPropMap,  classPropMap, cs.adderMeths);
			summary.put(key3, cs_new);
		}
		
		mof.store(summary, "revisedSummary.txt");
		//mof.displayObjects("revisedSummary.txt"); 
	}

	//Finds correct parameter input index.
	private Value findOriginalParameterIdx(Value v, ArrayList<Body> bodies, SootMethod m){
		Value returnValue = null; Integer parameterIdx =  null; 
		for (ListIterator<Body> li = bodies.listIterator(bodies.size()); li.hasPrevious(); )
		{
			HashMap<Value, Value> defUse_map = new HashMap<Value, Value>();
			Body body = li.previous();
			generateChain(body,defUse_map);
			if(v.toString().indexOf("parameter") > 0) parameterIdx = getParamIndex(v);

			for(Iterator<Unit> i = body.getUnits().iterator(); i.hasNext();)
			{
				Stmt s = (Stmt)i.next();
				if(s.toString().contains(m.toString()))
				{
					String str = s.toString().substring(s.toString().lastIndexOf("(")+1, s.toString().lastIndexOf(")"));
					String value = null;
					if(str.contains(",")) 	value = getValue(str, parameterIdx);
					else value = new String(str);

					for (Iterator<Value> vals = defUse_map.keySet().iterator(); vals.hasNext(); )
					{
						Value v1 = vals.next();
						Value v2 = defUse_map.get(v1);
						if(value != null && value.replaceAll("\\s","").equals(v1.toString())){
							returnValue = v2;
							if(v2.toString().indexOf("parameter") > 0) parameterIdx = getParamIndex(v2);
						}
					}
				}
			}
			m = body.getMethod();
			defUse_map = new HashMap<Value, Value>();
		}	
		return returnValue;
	}

	// tracks constructor chain recursively to derive constraints indices.
	private void trackChain(Iterator<Edge> callee, SootMethod meth){
		while(callee.hasNext())
		{
			Edge e = callee.next();
			SootMethod m = e.getTgt().method();
			if(!m.toString().startsWith("<java.util") && !m.toString().startsWith("<java.lang") && m.toString().contains("<init>") )
			{
				HashMap<Value, Value> defUse_tmp = new HashMap<Value, Value>();
				generateChain(m.getActiveBody(), defUse_tmp);
				for(Iterator<Value> i = defUse_tmp.keySet().iterator(); i.hasNext();)
				{
					Value v = (Value) i.next();

					if(v.toString().startsWith("r0.<"))
					{
						Value v2 = defUse_tmp.get(defUse_tmp.get(v));
						String str1 = v.toString().substring(v.toString().lastIndexOf(" "), v.toString().length()-1) ;
						if(v2.toString().contains("parameter"))
						{
							printDebugMsg("-==================>>>>	"+str1 +"has reference to the parameter as per :" + v2 + " inside " + m);
							Value origValue = findOriginalParameterIdx(v2,methBodies, m);
							if(origValue != null && origValue.toString().contains("parameter"))
								updateConstraintsAndIndices(origValue, str1, meth);
							else if(v2 != null && v2.toString().contains("parameter") && methBodies.size() == 1)
								updateConstraintsAndIndices(v2, str1, meth);
							//Otherwise not local.
						}
					}
				}
				methBodies.add(m.getActiveBody());
				trackChain(cg.edgesOutOf(e.getTgt().method()), meth);
			}
		}
		methBodies = new ArrayList<Body>();		
	}

	//Update constraints and indices.
	private void updateConstraintsAndIndices(Value origValue, String str, SootMethod meth) {
		int startIdx = origValue.toString().indexOf("parameter");
		Integer parameterIdx = Integer.parseInt(origValue.toString().substring(startIdx+9, origValue.toString().indexOf(":")).replaceAll("\\s",""));
		String parameterType = origValue.toString().substring(origValue.toString().indexOf(":") + 2, origValue.toString().length());
		printDebugMsg("Parameter index is : "+ parameterIdx + " and type is " + parameterType);
		ConstructorInfo info = constructorPropMap.get(meth.toString());
		HashMap<String, String> constraints = info.constraints;
		HashMap<String, Integer> constraints_indices = info.constraints_indices;
		constraints.put(str, parameterType);
		constraints_indices.put(str, parameterIdx);
		ConstructorInfo info_new = new ConstructorInfo(info.parameterTypes, info.className, constraints, constraints_indices);
		constructorPropMap.put(meth.toString(),info_new);
	}

	//Traverse through all the methods of the class, find constructors.
	//If public, fine.. If not public find if someone else calls them up.
	private void deriveConstructorInfo(List<SootMethod> list){
		for (Iterator<SootMethod> iterator = list.iterator(); iterator.hasNext();)	
		{
			SootMethod meth = iterator.next();
			if(meth.toString().contains("<init>") && meth.toString().contains(mainClass.toString()+":"))
			{
				if(meth.isPublic())
				{
					constructorPropMap.put(mainClass.toString(), 
							new ConstructorInfo(converter(meth.getParameterTypes()), mainClass.toString(), 
									new HashMap<String, String>(),new HashMap<String, Integer>()));
				}
				else 
				{
					for (Iterator<Edge> callers = cg.edgesInto(meth); callers.hasNext();)	
					{
						Edge e = callers.next();	
						if(e.getSrc().method().isPublic())
						{
							constructorPropMap.put(e.getSrc().method().toString(), 
									new ConstructorInfo(converter(e.getSrc().method().getParameterTypes()),
											mainClass.toString(), new HashMap<String, String>(),new HashMap<String, Integer>()));
							if(e.getSrc().method().getParameterCount() > 0)
							{
								methBodies.add(e.getSrc().method().getActiveBody());
								trackChain(cg.edgesOutOf(e.getSrc().method()), e.getSrc().method());
							}
						}
					}
				}
			}
		}
	}

	/************************************* Helper methods ****************************************/
	// Adds elements to def-use chain in the map.
	private void addDefUse(HashMap<Value, Value> map, Value v1, Value v2){
		if(map.get(v1) == null)
		{	
			map.put(v1, v2);
			printDebugMsg("Putting " + v2 + " for " + v1);
		}
	}

	//Generates def-use map for body
	private void generateChain(Body body, HashMap<Value, Value> map) {
		PatchingChain<Unit> units = body.getUnits();
		for(Iterator<Unit> i = units.iterator(); i.hasNext();)
		{
			Stmt s = (Stmt)i.next();
			if(s instanceof soot.jimple.internal.JAssignStmt)
			{
				AssignStmt assign = (AssignStmt) s;
				addDefUse(map, assign.getLeftOp(), assign.getRightOp());
			}
			else if (s instanceof soot.jimple.internal.JIdentityStmt)
			{
				IdentityStmt id = (IdentityStmt) s;
				addDefUse(map, id.getLeftOp(), id.getRightOp());
			}
		}
	}
	
	/* gets value at the parameter index */
	private String getValue(String str, Integer parameterIdx) {
		String value="";
		String list[] = str.split(",");
		int count = 0;
		if(parameterIdx != null)
			for(String st: list)
			{
				if(count == parameterIdx)
				{
					value = new String(st);
					break;
				}
				count++;
			}
		return value;
	}
	
	//Converts the array of method.getParameterTypes to list of strings.
	private ArrayList<String> converter(List list){
		ArrayList<String> new_list = new ArrayList<String>();
		for(Object o: list)
			new_list.add(o.toString());
		return new_list;
	}

	private Integer getParamIndex(Value v2){
		return Integer.parseInt(v2.toString().substring( v2.toString().indexOf("parameter")+9, 
				v2.toString().indexOf(":")).replaceAll("\\s",""));
	}
	
	private void printDebugMsg(String str){
		if(debug_flag) System.out.println(str);
	}
}
