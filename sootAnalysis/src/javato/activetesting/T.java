/* 
 * Other definitions.
 */

package javato.activetesting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class T{

	public static class AdderMethsMap extends HashMap<String, HashSet<String>> implements  java.io.Serializable {}

	public static class ClassSummaryMap extends HashMap<String, ClassSummary> implements  java.io.Serializable {}

	public static class ConstructorInfoMap extends HashMap<String, ConstructorInfo> implements  java.io.Serializable {}

	public static class LoopDepthMap extends HashMap<Integer, ArrayList<ArrayList<Integer>>> implements  java.io.Serializable{}

	public static class LoopDetailsMap extends HashMap<Integer, Detail> implements  java.io.Serializable {}

	public static class MethodDetailMap extends HashMap<String, MethodDetail> implements  java.io.Serializable {}

	public static class PerMethodSummaryMap extends HashMap<String, PerMethodSummary> implements  java.io.Serializable {}

	public static class ConstructorInfo implements  java.io.Serializable {

		ArrayList<String> parameterTypes;
		String className;
		
		//Map from property name to its type in that constructor.
		HashMap<String, String> constraints = new HashMap<String, String>();
		
		//Map from property name to parameter idx for this constructor.
		HashMap<String, Integer> constraints_indices = new HashMap<String, Integer>();

		public ConstructorInfo(ArrayList<String>  parameterTypes,  String className, HashMap<String, String> constraints, 				HashMap<String, Integer> constraints_indices)
		{
			this.parameterTypes = parameterTypes;
			this.constraints  = constraints;
			this.className  = className;
			this.constraints_indices = constraints_indices;
		}
	}
	
	public static class Detail implements  java.io.Serializable {

		int idx;
		String type;
		MethodDetailMap methodsInvokedInLoop;
	
		//Constructor
		public Detail(int idx, String type,  MethodDetailMap map) {
			this.idx = idx;
			this.type = type;
			this.methodsInvokedInLoop = map;
		}
	}	
	
	public static class MethodDetail implements  java.io.Serializable {

		String type;
		List<Integer> list;

		//Constructor.
		public MethodDetail(String type, List<Integer> list ) {
			this.type = type;
			this.list = list;
		}
	}

	public static class PerfTest implements  java.io.Serializable {

		int idx1;
		int idx2;
	
		//Constructor
		public PerfTest(int idx1, int idx2)
		{
			this.idx1 = idx1;
			this.idx2 = idx2;
		}
	
		public void print()
		{
			System.out.println("First index is : " + idx1);
			System.out.println("Second index is :" + idx2);
		}	
	}	
}
