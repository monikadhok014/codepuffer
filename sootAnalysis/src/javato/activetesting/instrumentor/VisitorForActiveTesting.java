package javato.activetesting.instrumentor;

import java.util.LinkedList;
import java.util.Iterator;
import java.util.*;

import javato.instrumentor.UnknownASTNodeException;
import javato.instrumentor.Visitor;
import javato.instrumentor.contexts.*;
import javato.activetesting.common.Parameters;
import soot.*;
import soot.jimple.*;
import soot.util.Chain;

/**
 * Copyright (c) 2007-2008
 * Pallavi Joshi	<pallavi@cs.berkeley.edu>
 * Koushik Sen <ksen@cs.berkeley.edu>
 * All rights reserved.
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * <p/>
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * <p/>
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * <p/>
 * 3. The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
public class VisitorForActiveTesting extends Visitor {

	public static final String openDeterministicBlockSig
	= "<edu.berkeley.cs.detcheck.Determinism: void openDeterministicBlock()>";

	public static final String closeDeterministicBlockSig
	= "<edu.berkeley.cs.detcheck.Determinism: void closeDeterministicBlock()>";

	// Horrible hack to add tracking of locals only to methods which
	// call {open,close}DeterministicBlock.  This is needed for
	// performance, because tracking locals is very expensive.
	private boolean containsDeterministicBlock = false;

	HashMap useDef = new HashMap(); //Def use map -- Monika
	//static int loopId = 1; //Increment this loop identifier at each loop entry point -- Monika
	boolean debugFlag = false; // -- Monika

	public VisitorForActiveTesting(Visitor visitor) {
		super(visitor);
	}

	public int getStSize() {
		return st.getSize();
	}

	private Stmt getStmtToBeInstrumented(Chain units, AssignStmt assignStmt, Value leftOp) {
		Stmt cur = assignStmt;
		Stmt succ;
		Stmt last = (Stmt) units.getLast();

		while (cur != last) {
			succ = (Stmt) units.getSuccOf(cur);
			if ((succ != null) && (succ instanceof InvokeStmt)) {
				InvokeExpr iExpr = succ.getInvokeExpr();
				if (iExpr.getMethod().getSubSignature().indexOf("<init>") != -1) {
					if (((InstanceInvokeExpr) iExpr).getBase() == leftOp) {
						return succ;
					}
				}
			}
			cur = succ;
		}
		return assignStmt;
	}

	private Stmt getStmtToBeInstrumented2(SootMethod sm, Chain units, AssignStmt assignStmt,
			Value objectOnWhichMethodIsInvoked, Stmt currStmtToBeInstrumented) {
		Stmt cur = assignStmt;
		Stmt succ;
		Stmt last = (Stmt) units.getLast();

		if (sm.getSubSignature().indexOf("<init>") != -1 && objectOnWhichMethodIsInvoked != null) {
			while (cur != last) {
				succ = (Stmt) units.getSuccOf(cur);
				if ((succ != null) && (succ instanceof InvokeStmt)) {
					InvokeExpr iExpr = succ.getInvokeExpr();
					if (iExpr.getMethod().getSubSignature().indexOf("<init>") != -1) {
						if (((InstanceInvokeExpr) iExpr).getBase() == objectOnWhichMethodIsInvoked) {
							return succ;
						}
					}
				}
				cur = succ;
			}
		}
		return currStmtToBeInstrumented;
	}

	private Value getMethodsTargetObject(SootMethod sm, Chain units) {
		Value objectOnWhichMethodIsInvoked = null;
		if (!sm.isStatic()) {
			Stmt startStmt = (Stmt) units.getFirst();
			if (startStmt instanceof IdentityStmt) {
				objectOnWhichMethodIsInvoked = ((IdentityStmt) startStmt).getLeftOp();
			} else {
				System.err.println("First statement within a non-static method is not an IdentityStmt");
				System.exit(-1);
			}
		}
		return objectOnWhichMethodIsInvoked;
	}

	//Modified by Monika : 
	//Add the parameters in the useDef chain
	public void visitMethodBegin(SootMethod sm, Chain units) {
//		System.out.println("Method name is: " + sm.getSignature());
//		if(sm.getSignature().toString().contains("org.jfree.chart.plot.CategoryPlot"))
//		{
//			System.out.println("Found class " + sm.getDeclaringClass());
//			if(sm.hasActiveBody()) System.out.println(sm.getActiveBody());
//		}
		
		if(debugFlag)
			System.out.println("------------- Before visitMethodBegin -----------------");

		if(!(sm.toString().contains("<init>") || sm.toString().contains("void main(java.lang.String[]")))
		{
			int paramCount = sm.getParameterCount();
			int counter = 0;
			java.util.List<ValueBox> l; ValueBox b; Value v;
			for(Iterator i = units.iterator(); i.hasNext();)
			{
				Stmt s = (Stmt)i.next();
				l = s.getDefBoxes();
				if(l.size() <= 0) break;
				b = l.get(0);
				v = b.getValue();
				if(v.toString().equals("this")){
					paramCount++;
				}
				else if (s.toString().contains("@this:"))
				{
					paramCount++;
				}
				if(useDef.get(v) == null)
					useDef.put(v, s);

				counter ++;
				if(counter == paramCount)
					break;
			}
		}
		if(debugFlag)
			System.out.println("------------- After visitMethodBegin -----------------");

		nextVisitor.visitMethodBegin(sm, units);

		if (!Parameters.trackDeterministicLocals)
			return;

		containsDeterministicBlock = false;

		for (Object u : units) {
			if (u instanceof InvokeStmt) {
				InvokeStmt is = (InvokeStmt)u;
				InvokeExpr ie = is.getInvokeExpr();

				if (!ie.getMethod().isStatic())
					continue;

				String sig = ie.getMethod().getSignature();
				if (sig.equals(openDeterministicBlockSig) || sig.equals(closeDeterministicBlockSig)) {
					containsDeterministicBlock = true;
				}
			}
		}
	}

	public void visitMethodEnd(SootMethod sm, Chain units) {
		nextVisitor.visitMethodEnd(sm, units);

		if (!Parameters.trackLocals
				&& !(Parameters.trackDeterministicLocals && containsDeterministicBlock))
			return;

		if (sm.getName().contains("<clinit>") || sm.getName().contains("<init>"))
			return;

		// Find the first place where it is legal to insert
		// instrumentation calls.
		//
		// (A method beings with some number of identity statements.
		// The first legal place is immediately after the last of
		// these statements.)
		Stmt lastIdStmt = null;
		for (Object u : units) {
			if (!(u instanceof IdentityStmt))
				break;
			lastIdStmt = (Stmt)u;
		}

		// If lastIdStmt is null here, then we can insert
		// instrumentation calls at the beginning of the method body.
		// In this case, however, there are no method parameters (or a
		// "this" variable) to instrument, so we will never
		// dereference lastIdStmt.

		// Insert a call to myWriteAfter for each method parameter.
		Body body = sm.getActiveBody();
		for (int i = sm.getParameterCount() - 1; i >=0 ; i--) {
			Local local = body.getParameterLocal(i);
			if (local == null) {
				System.err.println("Parameter is never assigned to a local in: " + sm);
				System.exit(-1);
			}
			//addCallWithLocalValue(units, lastIdStmt, "myWriteAfter", local, false);
		}

		// Insert a call to myWriteAfter for this.
		if (!sm.isStatic()) {
			Local thisLocal = body.getThisLocal();
			if (thisLocal == null) {
				System.err.println("'this' is never assigned to a local in: " + sm);
				System.exit(-1);
			}
			//addCallWithLocalValue(units, lastIdStmt, "myWriteAfter", thisLocal, false);
		}
	}

	public void visitStmtIf(SootMethod sm, Chain units, IfStmt ifStmt) {
		nextVisitor.visitStmtIf(sm, units, ifStmt);
	}

	//Modified by Monika
	// 1) Find out the object it is iterating on.. 
	// 2) Instrument the loopBefore and loopAfter functionality
	public void visitStmtGoto(SootMethod sm, Chain units, GotoStmt gotoStmt) {
		if(gotoStmt.toString().contains("return"))
		{
			nextVisitor.visitStmtGoto(sm, units, gotoStmt);
			return;
		}
		//System.out.println("*****************************" + sm +"************************************");
		//System.out.println(gotoStmt);
		boolean normalLoop = false;
		Stmt s2 = (Stmt)gotoStmt.getTarget();
		if(s2.toString().contains("if "))
			normalLoop = true;

		Stmt s3 = s2;
		Stmt s4 = null;
		Stmt bk = null;
		java.util.List<ValueBox> l; ValueBox b; Value v; boolean flagLocal = false;
		while(s2 != null){
			if(s2.toString().contains("@this"))
				bk = s4;
			s4 = s2;
			l = s2.getUseBoxes();
			if(normalLoop){
				if(l.size() <= 1) break;
				b = l.get(1);
				v = b.getValue();
				s2 = (Stmt)useDef.get(v);
			}
			else
			{
				if(l.size() <= 0) break;
				b = l.get(0);
				v = b.getValue();
				s2 = (Stmt)useDef.get(v);
			}
			if( s2 == null || s4.toString().equals(s2.toString())) break;
			if(s2 != null && s2.toString().contains("java.lang.Object")) {
				flagLocal = true;
				break;
			}
			if(s2 != null && s2.toString().contains("parameter"))
			{
				flagLocal = false;	
				break;
			}
			if(s4.toString().contains("@this") )
			{
				flagLocal = false;	
				break;
			}
		}
//				System.out.println("---------------------------------------------------");
//				System.out.println(units);
//				System.out.println("---------------------------------------------------");	

		int loopId = gotoStmt.toString().hashCode();
		if(!flagLocal && s2 != null)
		{
			int idx1 =  s2.toString().indexOf("@parameter");
			if(units.toString().startsWith("[this := @this: ") && idx1 > -1)
			{
				String sub =  s2.toString().substring(idx1,s2.toString().length());
				int idx2 =  sub.toString().indexOf(":");
				String param =  s2.toString().substring(idx1+10,idx2+idx1);
				addCallWithObjectIntInt(units, gotoStmt, "myLoopAfter",  StringConstant.v(""), IntConstant.v(loopId), IntConstant.v(Integer.parseInt(param)+1),true);
				addCallWithObjectIntInt(units, (Stmt)gotoStmt.getTarget(), "myLoopBefore", StringConstant.v(""), IntConstant.v(loopId),  IntConstant.v(Integer.parseInt(param)+1),true);
			}
			else if(idx1 > -1){
				String sub =  s2.toString().substring(idx1,s2.toString().length());
				int idx2 =  sub.toString().indexOf(":");
				String param =  s2.toString().substring(idx1+10,idx2+idx1);
				addCallWithObjectIntInt(units, gotoStmt, "myLoopAfter",  StringConstant.v(""), IntConstant.v(loopId), IntConstant.v(Integer.parseInt(param)+1),true);
				addCallWithObjectIntInt(units, (Stmt)gotoStmt.getTarget(), "myLoopBefore", StringConstant.v(""), IntConstant.v(loopId),  IntConstant.v(Integer.parseInt(param)+1),false);
			}
		}
		else if(!flagLocal && s2 == null && bk != null)
		{
			int idx1 =  bk.toString().indexOf("=");
			if(idx1 > -1){
				String paramVal = bk.toString().substring(idx1 + 1, bk.toString().length());
				if(!paramVal.contains("null"))
				{
					addCallWithObjectIntInt(units, gotoStmt, "myLoopAfter",  StringConstant.v(paramVal), IntConstant.v(loopId), IntConstant.v(0),true);
					addCallWithObjectIntInt(units, (Stmt)gotoStmt.getTarget(), "myLoopBefore", StringConstant.v(paramVal), IntConstant.v(loopId),  IntConstant.v(0),false);
				}
			}
		}
//		System.out.println("---------------------------------------------------");
//		System.out.println(units);
//		System.out.println("---------------------------------------------------");
		nextVisitor.visitStmtGoto(sm, units, gotoStmt);
	}


	public void visitStmtAssign(SootMethod sm, Chain units, AssignStmt assignStmt) {
		Value leftOp = assignStmt.getLeftOp();
		Value rightOp = assignStmt.getRightOp();
		if(debugFlag)
			System.out.println("------------- Before visitStmtBegin -----------------");
		if(useDef.get(leftOp) == null)
		{
			useDef.put(leftOp, assignStmt); // Updating the defUse chain -- Monika
		}
		if(debugFlag)
			System.out.println("------------- After visitStmtBegin -----------------");
		if (!Parameters.ignoreAlloc) {
			if ((rightOp instanceof NewExpr)
					|| (rightOp instanceof NewArrayExpr)
					|| (rightOp instanceof NewMultiArrayExpr)) {
				Stmt stmtToBeInstrumented = getStmtToBeInstrumented(units, assignStmt, leftOp);
				Value objectOnWhichMethodIsInvoked = getMethodsTargetObject(sm, units);

				stmtToBeInstrumented = getStmtToBeInstrumented2(sm, units, assignStmt,
						objectOnWhichMethodIsInvoked, stmtToBeInstrumented);

				if (objectOnWhichMethodIsInvoked != null) {
					//	addCallWithObjectObject(units, stmtToBeInstrumented, "myNewExprInANonStaticMethodAfter",
					//			leftOp, objectOnWhichMethodIsInvoked, false);
				} else {
					//	addCallWithObject(units, stmtToBeInstrumented, "myNewExprInAStaticMethodAfter", leftOp, false);
				}
			}
		}
		nextVisitor.visitStmtAssign(sm, units, assignStmt);
	}

	public void visitStmtEnterMonitor(SootMethod sm, Chain units, EnterMonitorStmt enterMonitorStmt) {
		if (!Parameters.ignoreConcurrency) {
			addCallWithObject(units, enterMonitorStmt, "myLockBefore", enterMonitorStmt.getOp(), true);
		}
		nextVisitor.visitStmtEnterMonitor(sm, units, enterMonitorStmt);
	}

	public void visitStmtExitMonitor(SootMethod sm, Chain units, ExitMonitorStmt exitMonitorStmt) {
		if (!Parameters.ignoreConcurrency) {
			addCallWithObject(units, exitMonitorStmt, "myUnlockAfter", exitMonitorStmt.getOp(), false);
		}
		nextVisitor.visitStmtExitMonitor(sm, units, exitMonitorStmt);
	}

	//Introduced by monika.
	private String findIndex(Value val, Chain units)
	{
		String thisOperator = "-1";
		Iterator itr = units.iterator();
		while(itr.hasNext())
		{
			Stmt s = (Stmt) itr.next();
			if(s.toString().contains("specialinvoke "+val+"."))
			{
				//System.out.println("The statement is :" + s);
				soot.jimple.internal.JInvokeStmt invokeExpr = (soot.jimple.internal.JInvokeStmt)s;
				java.util.List<ValueBox> l1; ValueBox b1; Value v11; boolean flagLocal1 = false;
				l1 = invokeExpr.getUseBoxes();
				//System.out.println("Use boxes : " + invokeExpr.getUseBoxes());
				if(l1.size() <= 0) break;
				b1 = l1.get(1);
				v11 = b1.getValue();
				Stmt s22 = (Stmt)useDef.get(v11);
				//System.out.println("Usage : " + v11 + " found " + s22);
			
				if(s22 != null && s22.toString().contains("@parameter")){
					int idx1 =  s22.toString().indexOf("@parameter");
					String sub =  s22.toString().substring(idx1,s22.toString().length());
					int idx2 =  sub.toString().indexOf(":");
					thisOperator =  s22.toString().substring(idx1+10,idx2+idx1);
				}
			}
		}
		return thisOperator;
	}
	
	public void visitInstanceInvokeExpr(SootMethod sm, Chain units, Stmt s, InstanceInvokeExpr invokeExpr, InvokeContext context) {
//		System.out.println("*******************************Inside :" + sm + "**************************************");
//		System.out.println(s);
//		System.out.println("********************************************************************");
//		System.out.println(invokeExpr);
		
//		System.out.println(units);
//		
		if(invokeExpr.toString().contains("org.jfree.ui.TextAnchor")) return;
		if(invokeExpr.toString().contains("java.lang.invoke.MethodHandle")) return;
		Value base = invokeExpr.getBase();
		String sig = invokeExpr.getMethod().getSubSignature();

		if (!Parameters.ignoreConcurrency) {
			if (sig.equals("void wait()")) {
				addCallWithObject(units, s, "myWaitBefore", base, true);
				addCallWithObject(units, s, "myWaitAfter", base, false);
			} else if (sig.equals("void wait(long)") || sig.equals("void wait(long,int)")) {
				addCallWithObject(units, s, "myWaitBefore", base, true);
				addCallWithObject(units, s, "myWaitAfter", base, false);
			} else if (sig.equals("void notify()")) {
				addCallWithObject(units, s, "myNotifyBefore", base, true);
			} else if (sig.equals("void notifyAll()")) {
				addCallWithObject(units, s, "myNotifyAllBefore", base, true);
			} else if (sig.equals("void start()") && isThreadSubType(invokeExpr.getMethod().getDeclaringClass())) {
				addCallWithObject(units, s, "myStartBefore", base, true);
				addCallWithObject(units, s, "myStartAfter", base, false);
			} else if (sig.equals("void join()") && isThreadSubType(invokeExpr.getMethod().getDeclaringClass())) {
				addCallWithObject(units, s, "myJoinAfter", base, false);
			} else if ((sig.equals("void join(long)") || sig.equals("void join(long,int)"))
					&& isThreadSubType(invokeExpr.getMethod().getDeclaringClass())) {
				addCallWithObject(units, s, "myJoinAfter", base, false);
			}
		}
		nextVisitor.visitInstanceInvokeExpr(sm, units, s, invokeExpr, context);
		if (!Parameters.ignoreMethods) {
			if(!invokeExpr.toString().contains("<java.io")
					&& !invokeExpr.toString().contains("<init>") && !invokeExpr.toString().contains("<java.util.List: java.util.Iterator iterator()>")  && !invokeExpr.toString().contains("<java.util.Iterator: boolean hasNext()>") 
					&& !invokeExpr.toString().contains("<java.util.Iterator: java.lang.Object next()>") && !invokeExpr.toString().contains("Iterator") )
			{
				Stmt s22 = s ;
				Stmt s4 = null; Stmt bk = null;
				java.util.List<ValueBox> l1; ValueBox b1; Value v11; boolean flagLocal1 = false;
				String thisOperator="undefined";
				//System.out.println("\t\t>> S22: " + s22);
				while(s22 != null){
					if(s22.toString().contains("@this"))
						bk = s4;
					s4 = s22;
					l1 = s22.getUseBoxes();
					if(l1.size() <= 0) break;
					b1 = l1.get(0);
					v11 = b1.getValue();	
					//System.out.println("\t\tLooking for : " + v11);
					s22 = (Stmt)useDef.get(v11);
					//System.out.println("\t\tFound : " + s22);
					if( s22 != null && s22.toString().contains("new java.util.ArrayList"))
					{
						
						thisOperator = findIndex(v11, units);
						break;
					}
					if( s22 == null || s4.toString().equals(s22.toString())) break;
					if(s22 != null && s22.toString().contains("java.lang.Object")) {
						flagLocal1 = true;
						break;
					}
					if(s22 != null && s22.toString().contains("parameter"))
					{
						flagLocal1 = false;	
						break;
					}
					if(s4.toString().contains("@this") )
					{
						flagLocal1 = false;	
						break;
					}
				}
				//System.out.println("\t\t>> S22: " + s22);
				if(!flagLocal1 && s22 != null)
				{
					int idx1 =  s22.toString().indexOf("@parameter");
					if(idx1 > -1){
						String sub =  s22.toString().substring(idx1,s22.toString().length());
						int idx2 =  sub.toString().indexOf(":");
						thisOperator =  s22.toString().substring(idx1+10,idx2+idx1);
					}
				}
				else if(!flagLocal1 && s22 == null && bk != null && thisOperator.equals(""))
				{
					int idx1 =  bk.toString().indexOf("=");
					if(idx1 > -1){
						thisOperator = bk.toString().substring(idx1 + 1, bk.toString().length());
					}
				}
				
			//	System.out.println("Instrumenting : " +  invokeExpr.getMethod());
			//	System.out.println(invokeExpr.getMethodRef());
			//	System.out.println("********************************************************************");
//				addCallWithObjectObject(units, s, "myMethodEnterBefore1",StringConstant.v(invokeExpr.getMethod().toString()),base, true);
//				addCallWithObjectObject(units, s, "myMethodExitAfter1",StringConstant.v(invokeExpr.getMethod().toString()), base, false);
				
				addCallWithObjectObjectObject(units, s, "myMethodEnterBefore1",StringConstant.v(invokeExpr.getMethod().toString()),base, StringConstant.v(invokeExpr.getMethodRef().toString()),  true);
				addCallWithObjectObjectObject(units, s, "myMethodExitAfter1",StringConstant.v(invokeExpr.getMethod().toString()), base, StringConstant.v(invokeExpr.getMethodRef().toString()), false);
				
				List<Integer> seq = new ArrayList<Integer>();
				java.util.List<ValueBox> l; ValueBox b; Value v; Stmt s2=null; boolean localVar = false;
				int counter = 0;
				for(Iterator i = invokeExpr.getArgs().iterator(); i.hasNext();)
				{
					counter++;
					Value v1 = (Value)i.next();
					while(true){
						if(v1.toString().indexOf("$") > -1) break;
						s2 = (Stmt)useDef.get(v1);
						if(s2 == null || s2.toString().contains("staticinvoke <java.util")){
							localVar = true;
							break;
						}
						if(s2 != null && s2.toString().contains("parameter")) {
							localVar = false;
							break;
						}
						l = s2.getUseBoxes();
						b = l.get(0);
						v1 = b.getValue();
						s2 = (Stmt)useDef.get(v1);
					}
					if(s2 != null && !localVar)
					{
						int idx1 =  s2.toString().indexOf("@parameter");
						if(idx1 > -1){
							String sub =  s2.toString().substring(idx1,s2.toString().length());
							int idx2 =  sub.toString().indexOf(":");
							if(idx1 > -1 && idx2 > -1 && idx1+10 < idx1+idx2){
								String param =  s2.toString().substring(idx1+10,idx2+idx1);
								seq.add(Integer.parseInt(param));
							}
							else 
								seq.add(-1);
						}
						else 
							seq.add(-1);
					}
					else
						seq.add(-1);

				}
				Value v3 = StringConstant.v(seq.toString());
				//System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. And this operator is : " + thisOperator);
				addCallWithObjectString(units, s, "myMethInvoke", v3, StringConstant.v(thisOperator), true); 
				if(debugFlag)
					System.out.println("------------- Before visitInvokeAfter -----------------"); 
			}
		}

		if (sig.indexOf("<init>") == -1) {
			if (!Parameters.ignoreConcurrency) {
				String ssig = sig.substring(sig.indexOf(' ') + 1);
				Value sig2 = StringConstant.v(ssig);
				addCallWithObjectString(units, s, "myLockBefore", base, sig2, true);
				// t = t.syncMethod() is problematic, so do not pass t
				addCall(units, s, "myUnlockAfter", false);
			}

		} else if (Parameters.trackLocals ||
				(Parameters.trackDeterministicLocals && containsDeterministicBlock)) {
			// Call to <init> -- add instrumentation call to myWriteAfter().
			//
			// NOTE: This captures assignments to locals of newly
			// allocated objects.  In the bytecode, such assignments
			// look like:
			//     local = new Object;
			//     local.<init>(...);
			if (base instanceof Local) {
				String name = ((Local)base).getName();
				// Must be an InvokeStmt (to be an init call).
				if (!(s instanceof InvokeStmt)) {
					throw new UnknownASTNodeException();
				}
				//if (!name.startsWith("$") && !name.equals("this"))
				//	addCallWithLocalValue(units, s, "myWriteAfter", (Local)base, false);
			}
		}
	}

	// Modified by Monika
	// 1) Instrument methodEnter and methodExit along with method names. 
	// 2) Instrument method invoked with list of parameter ids.
	public void visitStaticInvokeExpr(SootMethod sm, Chain units, Stmt s, StaticInvokeExpr invokeExpr, InvokeContext context) {
		nextVisitor.visitStaticInvokeExpr(sm, units, s, invokeExpr, context);
		if (!Parameters.ignoreMethods) {
			if(!invokeExpr.toString().contains("staticinvoke <java.util"))
			{	
				addCallWithObjectObjectObject(units, s, "myMethodEnterBefore1",StringConstant.v(invokeExpr.getMethod().toString()),StringConstant.v(" "), StringConstant.v(" "), true);
				addCallWithObjectObjectObject(units, s, "myMethodExitAfter1",StringConstant.v(invokeExpr.getMethod().toString()), StringConstant.v(" "), StringConstant.v(" "),  false);
				List<Integer> seq = new ArrayList<Integer>();
				java.util.List<ValueBox> l; ValueBox b; Value v; Stmt s2=null; boolean localVar = false;
				int counter = 0;
				for(Iterator i = invokeExpr.getArgs().iterator(); i.hasNext();)
				{
					counter++;
					Value v1 = (Value)i.next();
					while(true){
						if(v1.toString().indexOf("$") > -1) break;
						s2 = (Stmt)useDef.get(v1);
						if(s2 == null || s2.toString().contains("staticinvoke <java.util")){
							localVar = true;
							break;
						}
						if(s2 != null && s2.toString().contains("parameter")) {
							localVar = false;
							break;
						}
						l = s2.getUseBoxes();
						b = l.get(0);
						v1 = b.getValue();
						s2 = (Stmt)useDef.get(v1);
					}
					if(s2 != null && !localVar)
					{
						int idx1 =  s2.toString().indexOf("@parameter");
						if(idx1 > -1){
							String sub =  s2.toString().substring(idx1,s2.toString().length());
							int idx2 =  sub.toString().indexOf(":");
							if(idx1 > -1 && idx2 > -1 && idx1+10 < idx1+idx2){
								String param =  s2.toString().substring(idx1+10,idx2+idx1);
								seq.add(Integer.parseInt(param));
							}
							else 
								seq.add(-1);
						}
						else 
							seq.add(-1);
					}
					else
						seq.add(-1);

				}
				Value v3 = StringConstant.v(seq.toString());
				addCallWithObjectString(units, s, "myMethInvoke", v3, StringConstant.v(""), true); 
				if(debugFlag)
					System.out.println("------------- Before visitInvokeAfter -----------------");
			}

		}

		if (invokeExpr.getMethod().isSynchronized() && !Parameters.ignoreConcurrency) {
			addCallWithIntString(units, s, "myLockBefore",
					IntConstant.v(st.get(invokeExpr.getMethod().getDeclaringClass().getName())),
					StringConstant.v(invokeExpr.getMethod().getDeclaringClass().getName()),true);
			addCallWithInt(units, s, "myUnlockAfter",
					IntConstant.v(st.get(invokeExpr.getMethod().getDeclaringClass().getName())), false);
		}

		String sig = invokeExpr.getMethod().getSignature();
		if (sig.equals(openDeterministicBlockSig)) {
			//addCall(units, s, "myOpenDeterministicBlock", true);
		} else if (sig.equals(closeDeterministicBlockSig)) {
			//addCall(units, s, "myCloseDeterministicBlock", true);
		}
	}


	public void visitArrayRef(SootMethod sm, Chain units, Stmt s, ArrayRef arrayRef, RefContext context) {
		if (!Parameters.ignoreArrays) {
			if (context == RHSContextImpl.getInstance()) {
				//	addCallWithObjectInt(units, s, "myReadBefore", arrayRef.getBase(), arrayRef.getIndex(), true);
			} else {
				//	addCallWithObjectInt(units, s, "myWriteBefore", arrayRef.getBase(), arrayRef.getIndex(), true);
			}
		}
		nextVisitor.visitArrayRef(sm, units, s, arrayRef, context);
	}

	public void visitInstanceFieldRef(SootMethod sm, Chain units, Stmt s, InstanceFieldRef instanceFieldRef, RefContext context) {
		if (!Parameters.ignoreFields) {
			if ((!sm.getName().equals("<init>") || !instanceFieldRef.getField().getName().equals("this$0"))
					&& (!sm.getName().equals("<init>") || !instanceFieldRef.getField().getName().startsWith("val$")))
			{
				Value v = IntConstant.v(st.get(instanceFieldRef.getField().getName()));
				if (Modifier.isVolatile(instanceFieldRef.getField().getModifiers())) {
					if (context == RHSContextImpl.getInstance()) {
						//		addCallWithObjectInt(units, s, "myVReadBefore", instanceFieldRef.getBase(), v, true);
					} else {
						//		addCallWithObjectInt(units, s, "myVWriteBefore", instanceFieldRef.getBase(), v, true);
					}
				} else {
					if (context == RHSContextImpl.getInstance()) {
						//	addCallWithObjectInt(units, s, "myReadBefore", instanceFieldRef.getBase(), v, true);
					} else {
						//	addCallWithObjectInt(units, s, "myWriteBefore", instanceFieldRef.getBase(), v, true);
					}
				}
			}
		}
		nextVisitor.visitInstanceFieldRef(sm, units, s, instanceFieldRef, context);
	}

	public static void addCallWithLocalValue(Chain units, Stmt s, String methodName, Local l, boolean before) {
		StringConstant localName = StringConstant.v(l.getName());
		Type type = l.getType();

		if (type instanceof PrimType) {
			addCallWithType(units, s, methodName, localName, l, type.toString(), before);
		} else if (type instanceof RefType) {
			addCallWithType(units, s, methodName, localName, l, "java.lang.Object", before);
		} else if (type instanceof ArrayType) {
			addCallWithType(units, s, methodName, localName, l, "java.lang.Object", before);
		} else if (type.toString().equals("null_type")) {
			addCallWithType(units, s, methodName, localName, l, "java.lang.Object", before);
		}
	}

	public static void addCallWithType(Chain units, Stmt s, String methodName,
			Value v1, Value v2,
			String typeName, boolean before) {
		SootMethodRef mr;
		LinkedList<Value> args = new LinkedList<Value>();
		args.addLast(IntConstant.v(getAndIncCounter()));
		args.addLast(v1);
		args.addLast(v2);

		if (typeName.equals("java.lang.Object")) {
			args.addLast(StringConstant.v(v2.getType().toString()));
			typeName = "java.lang.Object,java.lang.String";
		}

		mr = Scene.v().getMethod("<" + observerClass + ": void " + methodName
				+ "(int,java.lang.String," + typeName + ")>").makeRef();

		if (before) {
			units.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mr, args)), s);
		} else {
			units.insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mr, args)), s);
		}
	}

	public void visitLocal(SootMethod sm, Chain units, Stmt s, Local local, LocalContext context) {
		if (((context == LHSContextImpl.getInstance())
				// || ((context == ThisRefContextImpl.getInstance()) && (sm.getSubSignature().indexOf("<init>") == -1))
				// || (context == ParameterRefContextImpl.getInstance())
				|| (context == NewArrayContextImpl.getInstance())
				|| (context == NewMultiArrayContextImpl.getInstance()))
				&& (s instanceof DefinitionStmt)) {

			DefinitionStmt ds = (DefinitionStmt) s;
			Value left = ds.getLeftOp();
			Value right = ds.getRightOp();

			if (!(left instanceof Local)) {
				throw new UnknownASTNodeException();
			}

			if (right instanceof NewExpr) {
				// Skip
			} else if (Parameters.trackLocals ||
					(Parameters.trackDeterministicLocals && containsDeterministicBlock)) {
				if (local.getName().charAt(0) != '$') {
					//addCallWithLocalValue(units, s, "myWriteAfter", local, false);
				}
			}
		}
		nextVisitor.visitLocal(sm, units, s, local, context);
	}

	public void visitStaticFieldRef(SootMethod sm, Chain units, Stmt s, StaticFieldRef staticFieldRef, RefContext context) {
		if (!Parameters.ignoreFields) {
			Value v1 = IntConstant.v(st.get(staticFieldRef.getField().getDeclaringClass().getName()));
			Value v2 = IntConstant.v(st.get(staticFieldRef.getField().getName()));
			if (Modifier.isVolatile(staticFieldRef.getField().getModifiers())) {
				if (context == RHSContextImpl.getInstance()) {
					//	addCallWithIntInt(units, s, "myVReadBefore", v1, v2, true);
				} else {
					//	addCallWithIntInt(units, s, "myVWriteBefore", v1, v2, true);
				}
			} else {
				if (context == RHSContextImpl.getInstance()) {
					//	addCallWithIntInt(units, s, "myReadBefore", v1, v2, true);
				} else {
					//	addCallWithIntInt(units, s, "myWriteBefore", v1, v2, true);
				}
			}
		}
		nextVisitor.visitStaticFieldRef(sm, units, s, staticFieldRef, context);
	}


}
