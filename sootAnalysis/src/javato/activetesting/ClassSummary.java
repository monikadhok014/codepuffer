/* 
 * ClassSummary definition.
 */

package javato.activetesting;

import java.util.*;

import javato.activetesting.T.AdderMethsMap;
import javato.activetesting.T.ConstructorInfoMap;
import javato.activetesting.T.PerMethodSummaryMap;

public class ClassSummary implements  java.io.Serializable {

	PerMethodSummaryMap summary;
	ConstructorInfoMap constructorPropMap;
	
	//Type constraints of this class.
	HashMap<String, String> classPropMap;
	
	// How to add elements to the instance of this class.
	AdderMethsMap adderMeths; 

	public ClassSummary(PerMethodSummaryMap summary, ConstructorInfoMap constructorPropMap, 
			HashMap<String, String> classPropMap, AdderMethsMap adderMeths) 
	{
		this.summary = summary;
		this.constructorPropMap = constructorPropMap;
		this.classPropMap = classPropMap;
		this.adderMeths = adderMeths;
	}

	public void print()
	{
		System.out.println(" ****************** All adder methods are  *******************");
		System.out.println(adderMeths);

		System.out.println(" ****************** Per method summary is  *******************");

		Iterator<String> iterator = summary.keySet().iterator();
		while (iterator.hasNext()) {
			String key3 = iterator.next().toString();
			System.out.println("\n=====================" + key3 + "=======================");
			PerMethodSummary value3 = summary.get(key3);
			value3.print();
		}
		for(String str:classPropMap.keySet())
		{
			System.out.println(str  + " : " + classPropMap.get(str));  
		} 

		for(String str:constructorPropMap.keySet())
		{
			System.out.println("=========================================");
			System.out.println(str);
			System.out.println("\t\tConstraints:"+ constructorPropMap.get(str).constraints);  
			System.out.println("\t\tIndices:"+ constructorPropMap.get(str).constraints_indices);  
			System.out.println("\t\tParameters:"+ constructorPropMap.get(str).parameterTypes);
		}
	}
}
