//Did not yet start working on this.
package javato.activetesting;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javato.activetesting.T.AdderMethsMap;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;

public class GenInputs {

	boolean debug_flag = false;
	static String PATH = "/home/user/Desktop/puffer/benchmark/";
	SootMethod method; //Method to be invoked.
	AdderMethsMap adderMeths; //To populate objects
	String constructor; //Constructor name
	SootMethod construct = null; //Constructor soot method
	static int counter = 0; //Counter for files
	int pattern_counter = 1;
	Boolean pattern_enable = true;
	ClassSummary details; 
	int var_counter = 0;
	boolean isInitPopulated=false;
	String thisType = "";
	
	String post="";
	
	String testtxt = "";
	Integer bas_val = 1000;
	int tmp = 1;
	ArrayList<String> params = new ArrayList<String>(); 

	int test_generation_counter = 0;
	public GenInputs(ClassSummary details, SootMethod method, AdderMethsMap adderMeths, String constructor)
	{
		this.method = method; 
		this.details = details;
		this.adderMeths = adderMeths;

		for (int i = 0; i <= method.getParameterCount(); i++) params.add(null);

		construct = getConstructObject();
		if(constructor != null && constructor.indexOf(":") > -1)
			this.constructor = constructor.substring(1, constructor.indexOf(":"));
		else 
			this.constructor  = constructor;
		/*if((constructor != null && constructor.contains("benchmarks.testcases")) || 
				(construct != null && construct.toString().contains("benchmarks.testcases")))
			pattern_enable = false;*/

		printDebugMsg("Construct is : " + construct);
	}

	public SootMethod getConstructObject()
	{
		SootMethod retMeth = null;
		SootClass sootclass = method.getDeclaringClass();
		ArrayList<SootMethod> listOfMethods = (ArrayList<SootMethod>)sootclass.getMethods();
		for(Iterator<SootMethod> it = listOfMethods.listIterator();it.hasNext();) 
		{
			SootMethod meth = (SootMethod)it.next();
			if(meth.getName().toString().equals("<init>") && meth.isPublic()){
				if(retMeth == null || meth.getParameterCount() < retMeth.getParameterCount()){
					retMeth = meth;
					break;
				}
			}
			if(details.constructorPropMap.keySet().contains(meth.toString())){
				retMeth = meth;
				break;
			}
		}
		if(details.constructorPropMap.keySet().contains(constructor)){
			if(details.constructorPropMap.get(constructor).parameterTypes.size() == 0)
				retMeth = null;
		}
		return retMeth;
	}

	/* returns the class corresponding to type - type */		
	private SootClass loadParameterClass(SootMethod construct, String type)
	{
		printDebugMsg("================= load parameter class ==================" + type);
		File dir = new File(PATH);
		ArrayList<String> list = showFiles(dir.listFiles());
		PufferCallGraph p = new PufferCallGraph(type, list);
		soot.Hierarchy hierarchy = p.generateHierarchy(type);
		SootClass c = Scene.v().getSootClass(type);
		Iterator itr = hierarchy.getImplementersOf(c).iterator();
		while(itr.hasNext()){
			SootClass klass = (SootClass) itr.next();
			if(klass.isPublic() && !klass.isAbstract()){
				SootMethod m = getConstructor(klass);
				if(m.getParameterCount() == 0)
				{
					System.out.println("\t " + klass);
					return klass;
				}
			}
		}
		printDebugMsg("================= end load parameter class ==================" );
		return null;
	}

	private void generateInitWhenConstructIsNotNullWithParams()
	{
		String type = "";  String paramStr = "";
		if(!construct.toString().contains("$"))
			type = construct.getParameterType(0).toString();
		else if (construct.getParameterCount() > 1) 
			createInitObjectHelper(true);
		if(!type.equals("java.util.ArrayList") && !type.equals("java.lang.Comparable")
				&& loadParameterClass(construct,type) != null){
			SootClass ret = loadParameterClass(construct,type);
			SootMethod paramMethod = getConstructor(ret);
			if(paramMethod.isConstructor() && paramMethod.getParameterCount() == 0 && !ret.isAbstract())
				testtxt += "\n\t\t" + ret.toString() + getVariable() +" = new " + ret.toString() + "();\n";
			getTxtIterator(getCurrVariable(), ret.toString());
			isInitPopulated = true;
			paramStr =  getCurrVariable() + populateParametersToConstructor();
		}else if(type.equals("java.lang.Comparable")){
			testtxt += "\t\t\tComparable"+  getVariable()  + "= new Comparable() { \n\t\t\t@Override\n\t\t\tpublic int compareTo(Object o)\n\t\t\t\t{\n\t\t\t\treturn 0;\n\t\t\t\t}\n\t\t\t};\n";
			paramStr += getCurrVariable();
		}else{
			generate(type);
			paramStr += getCurrVariable();
		}
		if(construct.isStatic())
			testtxt+= "\n\t\t" + constructor+ getVariable() +" = "+ constructor + "."+ construct.getName() +"(" + paramStr + ");\n";
		else
			testtxt+= "\n\t\t" + constructor+ getVariable() +" =  new " + constructor +"(" + paramStr + ");\n";
		bas_val = var_counter;
		if(!isInitPopulated)
			populateConstructorObject();
	}

	private String populateParametersToConstructor()
	{
		String params = "";
		for(int counter = 1; counter < construct.getParameterCount(); counter++)
		{
			if(construct.getParameterType(counter).toString().equals("boolean"))
				params += ",false";
			else
			{
				String type = construct.getParameterType(counter).toString();
				SootClass cl = Scene.v().getSootClass(type);
				PufferCallGraph p = new PufferCallGraph(cl.toString(), null);
				SootClass kt =  getNonAbstractSubClass(cl, cl.toString(), p);
				if(kt != null)
				{
					for (SootMethod paramMethod : kt.getMethods())
					{
						if(paramMethod.isConstructor() && !kt.isAbstract() && paramMethod.isPublic() )
						{
							if(paramMethod.getParameterCount() == 0)
								testtxt += "\n\t\t" + kt.toString() + getVariable() +" = new " + kt.toString() + "();\n";
							else 
								testtxt += "\n\t\torg.apache.commons.collections4.Transformer"  + getVariable() +" = " + "org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();\n";
							break;
						}
					}
					params += "," + getCurrVariable();
				}
				else  params += ",nullxxx";
			}
		}
		return params;
	}

	private void populateConstructorObject()
	{
		String tmpStr = getAdderMethod(constructor);
		SootClass cl = Scene.v().getSootClass(constructor);

		SootMethod addM = null;
		for(SootMethod meth : cl.getMethods()){
			if(meth.toString().contains(tmpStr))
			{
				addM = meth;
				break;
			}
		}
		getPattern();
		if(addM != null )
		{
			for(Object l : addM.getParameterTypes())
				generate(l.toString());
			testtxt += "\t\t\t" + getVariableAt(bas_val)+"." + addM.getName() +"(" + passParams(addM) + ");\n";
		}else 
			testtxt += "\t\t\t" + getVariableAt(bas_val)+".add(i);\n";

		testtxt += "\t\t}";
	}

	private void HandleNonNullThisType()
	{
		String adderMeth = adderMeths.get(thisType).iterator().next();
		SootClass adderSootClass = Scene.v().getSootClass(constructor);
		SootMethod reqMeth = null;
		for(SootMethod meth : adderSootClass.getMethods())
		{
			if(meth.toString().equals(adderMeth))
			{
				reqMeth = meth;
				break;
			}
		}
		if(reqMeth != null)
		{
			getPattern();
			populateParametersToThisType(reqMeth);
			testtxt += "\t\t\tp" + bas_val +"." +reqMeth.getName() +"( " + passParams(reqMeth)+");\n";
			testtxt += "\t\t}";
		}
	}

	private void populateParametersToThisType(SootMethod reqMeth)
	{
		for(Object o : reqMeth.getParameterTypes())
		{
			SootClass localCl = Scene.v().getSootClass(o.toString());
			SootMethod m = null;
			if(!localCl.isInterface() && !localCl.isAbstract())
				m = getConstructor(localCl);
			else {
				PufferCallGraph p = new PufferCallGraph(localCl.toString(), null);
				SootClass innerLocLc = getNonAbstractSubClass(localCl, localCl.toString(), p);
				m = getConstructor(innerLocLc);
			}
			if(m != null)
				testtxt += "\t\t\t" + m.getDeclaringClass() + getVariable() + " = new " + m.getDeclaringClass() + "(" +");\n";
			else
			{
				if(o.toString().equals("int"))
					testtxt += "\t\t\tint" + getVariable() + " = i;\n";
			}
		}
	}

	private void HandleNullThisType(SootClass dum)
	{
		String tmpstr = getAdderMethod(constructor);
		SootMethod meth = null;
		for(SootMethod mi: dum.getMethods()){
			if(mi.getName().equals(tmpstr)){
				if(meth == null || mi.getParameterCount() < meth.getParameterCount())
					meth = mi;
			}
		}
		if(meth != null)
		{
			populateNullThisType(meth, tmpstr);
		}else if(tmpstr.equals("add")){
			if(!constructor.equals("org.jfree.chart.plot.CategoryPlot"))
				getTxtIterator(getCurrVariable(), constructor);
		}
	}

	private void populateNullThisType(SootMethod meth, String tmpstr)
	{
		for(Object o: meth.getParameterTypes()){
			if(o.toString().equals("java.lang.Object")) {
				getTxtIterator(getCurrVariable(), constructor);
				break;
			}
			SootClass cl = Scene.v().getSootClass(o.toString());
			PufferCallGraph p = new PufferCallGraph(cl.toString(), null);
			SootClass cll = getNonAbstractSubClass(cl, cl.toString(), p);
			if(cll == null) return;
			SootMethod mt = getConstructor(cll);
			if(mt.getParameterCount() == 0){
				getPattern();
				testtxt += "\t\t\t" + tmpstr + "( new " + cll.getName() + "());\n";
				testtxt += "\t\t}";
			}
			else if(mt.getParameterCount() == 1){
				getPattern();
				String variable = getCurrVariable();

				String para = passParams(mt);
				testtxt += "\t\t\t" + cll.getName()  + getVariable() +" = new " + cll.getName() +"( "+ para +");\n";
				testtxt += "\t\t\t"+ variable + "." + tmpstr + "(" + passParams(meth) + ");\n";
				testtxt += "\t\t}\n";
			}
		}
	}

	private Boolean getValueForType(String l, String subClassName, PufferCallGraph p){
		if(!l.startsWith("java.util")){
			SootClass tmp = Scene.v().getSootClass(l);
			if(!tmp.isAbstract()){
				if(tmp.toString().equals("java.lang.Object"))
					testtxt += "\t\t" + "java.lang.Object" + getVariable() +"= new " + "java.lang.Object" + "() ;\n";
				else{
					printDebugMsg("GetValueForType ===============================3");
					if(tmp.isPublic()){
						SootMethod m = getConstructor(tmp);
						if(m.getParameterCount() == 0)
							testtxt += "\t\t" + tmp.getName() + getVariable() +" = new " + tmp.getName() +"();\n";
						else	
							testtxt += tmp.getName().replace("$", ".") + getVariable() +" = null;\n";
					}
					else{
						printDebugMsg("GetValueForType ===============================4");
						SootClass cl = getNonAbstractSubClass(tmp, tmp.getName(), p);
						if(cl != null)
							testtxt += cl.getName().replace("$", ".") + getVariable() +" = null;\n";
						else return false;
					}
				}
			}
			else{
				printDebugMsg("GetValueForType ===============================5");
				SootClass consClass = getNonAbstractSubClass(tmp, subClassName, p);
				dummy(consClass, subClassName, p, true);
			}
		}
		else 
			generate(l);
		return true;
	}

	private void dummy(SootClass k, String subClassName, PufferCallGraph p, boolean toPopulate)
	{
		SootMethod m = getConstructor(k);

		String tmppara = "";
		for(Object l1 : m.getParameterTypes())
		{
			boolean b = getValueForType(l1.toString(), subClassName, p);
			if(!b)	tmppara += ",null";
			else 	tmppara += "," + getCurrVariable();
		}
		String[] tmpParaArray = tmppara.split(",");
		String para = "";
		for(int c = tmpParaArray.length - m.getParameterCount() ; c <= m.getParameterCount(); c++)
		{
			para += tmpParaArray[c];
			if(c != m.getParameterCount()) para += ",";
		}
		String type = k.toString().replace("$", ".");
		if(!m.isConstructor() && m.isStatic()){
			testtxt += "\t\t" + type + getVariable() +"= " + type +"." + m.getName()+"();\n";
			isInitPopulated = true;
		}
		else if(k.toString().contains("$") && !k.toString().contains("org.apache.commons.collections4.bidimap.AbstractDualBidiMap")
				&& !k.toString().contains("com.google.common.collect.Synchronized")
				&& !k.toString().contains("com.google.common.collect.Maps") && !k.toString().contains("benchmarks.testcases.Type4"))
		{//this is a static class; no way to figure out such things.
			para = para.substring(para.indexOf(",") + 1);
			if(para.length() == 3 && para.equals(getVariableAt(tmp)))
				testtxt += "\t\t" + type + getVariable() +"= " +  getVariableAt(tmp) + ".new " + type.substring(type.lastIndexOf(".")+1) + "( ) ;\n";
			else
				testtxt += "\t\t" + type + getVariable() +"= " +  getVariableAt(tmp) + ".new " + type.substring(type.lastIndexOf(".")+1) + "("+ para +" ) ;\n";
		}
		else
			testtxt += "\t\t" + type + getVariable() +"= new " + type + "("+ para +" ) ;\n";
		if(!isInitPopulated)
		{
			if(toPopulate)
				getTxtIterator(getCurrVariable(), type);
			isInitPopulated = true;
		}
		bas_val = var_counter;
	}

	//Assuming only one parameter to adder methods.
	private void createInitObject(boolean toPopulate)
	{
		if(construct != null && !construct.toString().contains(constructor)){
			printDebugMsg(">>>>>>>>>>>>>>>>>>>>>>>>>>> 11");
			testtxt+= "\t\t" + constructor + getVariable() + " = new " + constructor + "(" +");\n";
			getTxtIterator(getCurrVariable(), constructor);
			bas_val = var_counter;
		}
		else if(construct != null && construct.getParameterCount() > 0 &&
				!construct.toString().contains("$"))
		{
			printDebugMsg(">>>>>>>>>>>>>>>>>>>>>>>>>>> 22");
			SootClass cl = Scene.v().getSootClass(constructor);
			SootMethod m = getConstructor(cl);
			if(m.getParameterCount() == 0 && m.isPublic())
			{
				construct = m;
				testtxt+= "\t\t" + constructor + getVariable() + " = new " + constructor + "(" +");\n";
				getTxtIterator(getCurrVariable(), constructor);
				bas_val = var_counter;
				isInitPopulated = true;
			}
			else
				generateInitWhenConstructIsNotNullWithParams();
		}
		else
			createInitObjectHelper(toPopulate);
		isInitPopulated = true;
	}

	private void createInitObjectHelper(boolean toPopulate)
	{
		if(constructor.contains("$"))
		{
			printDebugMsg(">>>>>>>>>>>>>>>>>>>>>>>>>. 22--222" + construct);
			String subClassName = constructor.substring(0, constructor.indexOf("$")-1);
			PufferCallGraph p = new PufferCallGraph(subClassName, null);
			SootClass k = Scene.v().getSootClass(constructor);
			dummy(k,subClassName, p, toPopulate);
		}
		else 
		{
			SootClass dum = Scene.v().getSootClass(constructor);
			if(!dum.isAbstract()){
				testtxt += "\t\t" + constructor + getVariable() + " = new " + constructor + "(" +");\n";
			}
			else{
				PufferCallGraph p = new PufferCallGraph(dum.toString(), null);
				SootClass subDUmClass = getNonAbstractSubClass(dum, dum.toString(), p);
				if(subDUmClass != null)
				{
					SootMethod subDum = getConstructor(subDUmClass);
					testtxt += "\t\t" + subDum.getDeclaringClass() + getVariable() + " = new " + subDum.getDeclaringClass() + "(" +");\n";
				}
			}
			bas_val = var_counter;
			if(!toPopulate) return;
			if(!thisType.equals("")) HandleNonNullThisType();
			else HandleNullThisType(dum);
		}
	}

	// Object should be created in the following manner. Iterator handled in special manner.
	private String generateObject(int counter, String type, boolean isStatic)
	{
		System.out.println("The type is : " + type);
		if(type.equals("java.util.Collection") || type.equals("java.util.ArrayList"))
			testtxt += "\n\t\t" + "ArrayList" + getVariable() + "= new " + "ArrayList" + "();\n";
		else
			testtxt += "\n\t\t" + type + getVariable() +"= new " + type + "();\n";

		if(counter != 0)
		{
			params.set(counter - 1, getCurrVariable());
		}

		getTxtIterator(getCurrVariable(), type.toString());
		return testtxt;
	}

	private String passParams(SootMethod m)
	{
		String ret = "";
		int counter = m.getParameterCount();
		int tmp = var_counter;
		while(counter > 0)
		{
			ret = ",p" + tmp-- + ret;
			counter--;
		}
		if(ret.length() > 1)
			return ret.substring(1);
		else
			return ret;

	}
	private SootClass getNonAbstractSubClass(SootClass tmp, String subClassName, PufferCallGraph p)
	{
		soot.Hierarchy hierarchy = p.generateHierarchy(subClassName);
		List<SootClass> l2 = null;
		if(tmp.isInterface())
			l2 = hierarchy.getImplementersOf(tmp);
		else 
			l2 = hierarchy.getSubclassesOf(tmp);
		for(SootClass l22 : l2)
		{
			SootMethod m = getConstructor(l22);
			if(!l22.isAbstract() && !l22.toString().equals(constructor) && m.isPublic())
				return l22;
		}
		return null;
	}

	private SootMethod getConstructor(SootClass k)
	{
		ArrayList<SootMethod> meths = (ArrayList<SootMethod>)k.getMethods();
		SootMethod retMeth = null;
		for(SootMethod m:meths)
			if(retMeth == null || (m.isConstructor() && retMeth.getParameterCount() > m.getParameterCount()))
				retMeth = m;

		return retMeth;
	}

	private void generate(String type)
	{
		if(type.equals("java.util.Set"))
			testtxt += "\t\t" + "java.util.HashSet" + getVariable() +"= new " + "java.util.HashSet" + "() ;\n";
		else if(type.equals("java.util.Map") || type.equals("java.util.HashMap"))
		{
			testtxt += "\t\t" + "java.util.HashMap" + getVariable() +"= new " + "java.util.HashMap" + "() ;\n";
			if(!isInitPopulated){
				getTxtIterator(getCurrVariable(), "java.util.HashMap");
				isInitPopulated = true;
			}
		}
		else if(type.equals("java.util.NavigableSet"))
		{
			testtxt += "\t\t" + "java.util.TreeSet" + getVariable() +"= new " + "java.util.TreeSet" + "() ;\n";
			getPattern();
			testtxt += "\t\t\t" + getCurrVariable() +".add(i);\n";
			testtxt += "\t\t}\n";
		}
		else if(type.equals("int"))
			testtxt += "\n\t\t" + "int"+ getVariable()  +" = 0";
		else if(type.equals("java.lang.Object"))
			testtxt += "\n\t\t\t" + "int" + getVariable() +" = (int)Math.random();\n";
		else if(type.equals("java.util.Collection") || 
				type.equals("java.util.ArrayList"))
		{
			testtxt += "\n\t\t" + "ArrayList" + getVariable()  + " = new " + "ArrayList" + "();\n";
			getTxtIterator(getCurrVariable() , "ArrayList");
		}
		else if(type.equals("java.lang.Comparable"))
			testtxt += "\t\t\tComparable"+  getVariable()  + "= new Comparable() { \n\t\t\t@Override\n\t\t\tpublic int compareTo(Object o)\n\t\t\t\t{\n\t\t\t\treturn 0;\n\t\t\t\t}\n\t\t\t};\n";
	}

	private String populateInitObject( boolean isListOfObject)
	{
		if(!isListOfObject)
			getTxtIterator(getCurrVariable() ,"ArrayList");
		else
		{
			testtxt+= "\t\t int current = 0;";
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			testtxt += "\n\t\t\tArrayList list = new ArrayList(largeNumber);";
			testtxt += "\n\t\t\tfor (int j = current; i < current + largeNumber; i++) {";
			testtxt += "\n\t\t\t\tlist." +getAdderMethod("ArrayList") +"(i);";
			testtxt += "\n\t\t\t}";
			testtxt += "\n\t\t\tobj."+getAdderMethod("ArrayList") +"(i);";
			testtxt += "\n\t\t}";
		}
		return testtxt;
	}

	//Test synthesis should start from the following content.
	private void createPreamble(int i1, String type1 , int i2, String type2)
	{

		File dir1 = new File("../../perfTests/all");
		int size = 1;
		if(dir1.exists()){
			dir1.mkdir();
			size = dir1.list().length + 1;
		}

		counter++; 
		if(constructor == null) return;
		printDebugMsg("\n\t\t\t\t ======= Following is the generated test =======");

		if(constructor != null && constructor.contains("."))
			testtxt = "package " + constructor.substring(0, constructor.lastIndexOf(".")) +";\n";
		else if (construct != null && construct.toString().contains("."))
			testtxt = "package " + construct.toString().substring(0, constructor.lastIndexOf(".")) +";\n";
		else 
			testtxt = "package " + method.getDeclaringClass().toString().substring(0, method.getDeclaringClass().toString().lastIndexOf(".")) +";\n";

		/************   Generate import statements and type of provided index  ********/
		for(int i = 0; i < method.getParameterCount(); i++)
		{
			if(method.getParameterType(i).toString().contains("."))
				testtxt+= "import " + method.getParameterType(i) + ";\n";
			if(i == i1-1)
				type1 = method.getParameterType(i).toString();
			if(i == i2-1 && type2.equals(""))
				type2 = method.getParameterType(i).toString();
		}

		/************   Default import statements  ********************************/
		if(constructor!=null && constructor.contains("."))
			testtxt+= "import " + constructor.replace("$", ".") + ";\n";
		testtxt+= "import " + "java.util.ArrayList" + ";\n";
		testtxt+= "public class Test" + size +"\n{\n";
		testtxt+= "\t@SuppressWarnings(\"unchecked\")\n";
		testtxt+= "\tpublic static void main(String[] args) \n\t{\n";
		testtxt+= "\n\t\tint largeNumber = Integer.parseInt(args[0]);";
	}

	//Test synthesis should end with the following content.
	private void createPostamble(boolean isStatic)
	{
		/****************  Invoke methods *****************************************/
		testtxt+="\n";

		testtxt+= "\n\t\t/* Invoke the method on the created object. */\n";
		testtxt += "\t\tlong start = System.currentTimeMillis();\n";

		String parameters = parametersToTheFinalInvocation();
		if(method.isConstructor())
			testtxt += "\t\t" + method.getDeclaringClass().getName() + getVariable() + " = new " + method.getDeclaringClass().getName()+"(" + parameters + ");\n";
		else if(!isStatic)
			testtxt += "\t\t" + getVariableAt(bas_val) +"."+method.getName() +"(" + parameters + ");\n";
		else 
			testtxt += "\t\t"+ method.getDeclaringClass() + "."+method.getName() +"(" + parameters + ");\n";

		testtxt += "\t\tlong stop = System.currentTimeMillis(); \n";
		testtxt += "\t\tSystem.out.println(\" Total time taken : \"+ (stop - start));\n";
		testtxt+= "\t}	\n";
		testtxt += post;
		testtxt+= "}\n";

		System.out.println(testtxt);
		toFile(testtxt);
	}

	private String parametersToTheFinalInvocation()
	{
		ArrayList<String> localParams = new ArrayList<String>();
		for(int i = 0; i <  method.getParameterCount(); i++){
			if(params.get(i) == null){
				SootClass localClass = Scene.v().getSootClass(method.getParameterType(i).toString());
				if(method.getParameterType(i).toString().equals("int"))
				{
					testtxt += "\n\t\t" + "int" + getVariable()  + " = (int)Math.random();\n";
					localParams.add(getCurrVariable());
					continue;
				}
				else if(localClass.getName().equals("java.util.Collection"))
					testtxt += "\n\t\t" + "ArrayList" + getVariable()  + " = new " + "ArrayList" + "();\n";
				else
				{
					SootMethod m = getConstructor(localClass);
					if(localClass.isAbstract())
						testtxt+= "\t\t" + localClass.getName() + getVariable() + " = null;\n";
					else if(m != null)
						testtxt+= "\t\t" + localClass.getName() + getVariable() + " = new " + localClass.getName()  + "(" + passParams(m) +");\n";
				}
				localParams.add(getCurrVariable());
			}	
			else
				localParams.add(params.get(i));

		}
		String params = localParams.subList(0, method.getParameterCount()).toString();
		params = params.substring(1, params.length()-1);
		return params;
	}

	private String addStringGenerator()
	{
		return  "\tprivate static char[] vs; \n" + 
				"\tstatic { \n" +
				"\t\tvs = new char['Z' - 'A' + 1]; \n" +
				"\t\tfor(char i='A'; i<='Z';i++) vs[i - 'A'] = i; \n" +
				"\t} \n" 
				+
				"\tprivate static StringBuilder alpha(int i){  \n" + 
				"\t\tassert i > 0;  \n" + 
				"\t\tchar r = vs[--i % vs.length];  \n" + 
				"\t\tint n = i / vs.length;  \n" + 
				"\t\treturn n == 0 ? new StringBuilder().append(r) : alpha(n).append(r);  \n" + 
				"\t}  \n";
	}

	private void multiLevel2(int i1, String t1 , int i2, String t2, boolean isStatic, boolean isListOfObject)
	{
		printDebugMsg("::::::::::::::inside multilevel:::::::::::: " + i1 + i2);
		if(i2-1 < 0 && i2 != 0) return; 
		createPreamble(i1, t1 , i2, t2);

		testtxt += "\n\t\t/* Create a constructor to invoke the object. */\n";

		if(i1 != 0 && i2 != 0 && !method.isConstructor() && !isStatic)
			createInitObject(false);
		else if (!method.isConstructor() && !isStatic)
			createInitObject(true);
		if(i1 == 0 && !isStatic && !isInitPopulated)
			populateInitObject(isListOfObject);

		testtxt+= "\n\t\t/* Create first parameter. */";
		if(i1 != 0)
		{
			System.out.println("Parameter type accordingly is  : " + method.getParameterType(i1-1));
			if(method.getParameterType(i1-1).toString().contains("int"))
				//generateObject(i1, "int", isStatic);
				;
			else	
				generateObject(i1, t1, isStatic);
		}
		if(i2 != 0)
			generateObject(i2, t2, isStatic);

		createPostamble(isStatic);
	}

	//based on one index (ant:collectionUtils:asCollection)
	private void generate2(int idx,boolean isStatic, boolean isListOfObjects)
	{
		printDebugMsg("::::::::::::::::inside generate::::::::::::::::::::::");
		counter++;
		String type = "";
		if(idx == 0)
			type = method.getParameterType(0).toString();
		else
			type = method.getParameterType(idx-1).toString();

		createPreamble(0, "", 0, "");

		testtxt+= "\n\t\t/* Create a constructor to invoke the object. */\n";
		testtxt+= "\t\t" + constructor+ " obj =  new " + constructor +"();\n";
		populateInitObject(isListOfObjects);
		generateObject(1, type, isStatic);
		createPostamble(isStatic);
	}

	public void toFile(String str) {
		BufferedWriter writer = null;
		BufferedWriter writer1 = null;
		BufferedWriter writer2 = null;
		try {
			File dir;
			if(constructor != null)
			{
				String name = constructor.substring(constructor.lastIndexOf(".")+1);
				if(name.contains("$"))
					name = name.substring(0, name.indexOf("$"));
				dir = new File("../../perfTests");
				dir.mkdir();
				dir = new File("../../perfTests/"+ name );
			}
			else
				dir = new File("../../perfTests/Current");
			dir.mkdir();

			File dir1 = new File("../../perfTests/all");
			int size = 1;
			if(dir1.exists()){
				size = dir1.list().length + 1;
			}else
				dir1.mkdir();

			File logFile = new File(dir.getCanonicalPath()+ "/Test" + size +".java");
			System.out.println(logFile.getCanonicalPath());

			writer = new BufferedWriter(new FileWriter(logFile));
			System.out.println("Test generated : " + logFile);
			writer.write(str);

			File logFile1 = new File(dir1.getCanonicalPath()+ "/Test" + size +".java");
			writer1 = new BufferedWriter(new FileWriter(logFile1));
			writer1.write(str);

			if(test_generation_counter == 0)
			{
				File testsOutputFile = new File("../../perfTests/TestsOutput.log");
				if(!testsOutputFile.exists())
					testsOutputFile.createNewFile();
				writer2 = new BufferedWriter(new FileWriter(testsOutputFile, true));
				String str1 = "Tests with different patterns for following case are generated. \n"
						+ "The method \n" + method + " \ninside the class with name " + constructor + "performs possible "+
						"infficient loop execution.\n\n\n";
				writer2.write(str1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
				writer1.close();
				writer2.close();
			} catch (Exception e) {
			}
		}
	}

	public String getAdderMethod(String type)
	{
		if(type.contains("map")) return "put"; //Added space after Map : hack for guava map
		if(type.equals("ArrayList") || type.equals("java.util.HashSet") 
				|| type.equals("java.util.ArrayList")) 
			return "add";

		String adderM="";
		if(adderMeths.containsKey(type))
			adderM = adderMeths.get(type).iterator().next();
		else
		{
			SootClass kclass = Scene.v().getSootClass(type);
			if(kclass.hasSuperclass())
			{
				String superClass = kclass.getSuperclass().toString();
				if(adderMeths.containsKey(superClass))
					adderM = adderMeths.get(superClass).iterator().next();
			}
		}
		if(adderM.equals("")) return "add";
		if(adderM.contains("add")) return "add";
		String ret =  adderM.substring(adderM.lastIndexOf(" ") + 1, adderM.indexOf("("));
		return ret;

	}

	public void getTxtIterator(String var, String type)
	{
		String tmpStr = getAdderMethod(type);
		if(tmpStr == null) return;

		String elem = getPattern();
		if(tmpStr.equals("add") || tmpStr.equals("push"))
			testtxt += "\n\t\t\t" + var + "." + tmpStr +"(" +  elem  + "); " + "\n\t\t}\n";
		else if(tmpStr.equals("put"))
			testtxt += "\n\t\t\t" + var + "." + tmpStr +"(i, " +  elem  + "); " + "\n\t\t}\n";

	}

	public String getPattern()
	{
		switch (pattern_counter) 
		{
		case 1:   
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "i";
		case 2:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "i";
		case 3:   
			testtxt += "\t\tfor (int i = 1; i < largeNumber; i++) {";
			pattern_counter++;
			post = addStringGenerator();
			return "alpha(i).toString()";
		case 4:
			testtxt += "\t\tfor (int i = 1; i < largeNumber; i++) {";
			pattern_counter++;
			if(post.equals(""))
				post = addStringGenerator();
			return "alpha(i).toString()";
		case 5:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "new Object()";
		case 6:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "new Object()";
		case 7:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "(int)Math.random()";
		case 8:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "(int)Math.random()";
		case 9:
			testtxt += "\t\tfor (int i = 0; i < largeNumber * 0.5; i++) {";
			pattern_counter++;
			return "i";
		case 10:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "i";
		default:
			testtxt += "\t\tfor (int i = 0; i < largeNumber; i++) {";
			pattern_counter++;
			return "i";
		}
	}

	public ArrayList<String> showFiles(File[] files) {
		if(files == null) return null;

		ArrayList<String> list = new ArrayList<String>();
		for (File file : files) {
			if (file.isDirectory()) {
				showFiles(file.listFiles()); // Calls same method again.
			} else {
				String path = file.getAbsolutePath();
				if(!path.contains("$"))
				{
					if(path.lastIndexOf(".") > 0)
					{
						String toLoad = path.substring(0, path.lastIndexOf("."));
						toLoad = toLoad.substring(toLoad.indexOf("benchmark") + 11).replace("/",".");					
						list.add(toLoad);
					}
				}
			}
		}
		return list;
	}

	public void generate(int idx, boolean isStatic, boolean isListOfObjects)
	{
		testtxt = "";
		if(pattern_enable)
		{
			for (int counter = 0; counter < 4 ; counter++)	
			{
				tmp = var_counter + 1;
				post="";
				generate2(idx,  isStatic, isListOfObjects);
			}
			tmp = var_counter + 1;
		}
		post="";
		generate2(idx,  isStatic, isListOfObjects);
	}

	public void generate_multiLevel(int i1, String t1, int i2, String t2, boolean stat, boolean isMultiList, String line, String thisType)
	{
		this.thisType = thisType;
		testtxt = "";
		printDebugMsg("Line number is: "+ line);
		if(pattern_enable)
		{
			for (test_generation_counter = 0; test_generation_counter < 4 ; test_generation_counter++)
			{
				tmp = var_counter + 1;
				post="";
				multiLevel2(i1, t1 , i2, t2, stat, isMultiList);
			}
			tmp = var_counter + 1;
		}
		post="";
		multiLevel2(i1, t1, i2, t2, stat, isMultiList);
	}

	private String getVariable() { return " p"+ (++var_counter); }
	private String getCurrVariable() { 	return " p"+var_counter; }
	private String getVariableAt(int count) { return " p"+count; } 

	private void printDebugMsg(String str)
	{
		if(debug_flag) System.out.println(str);
	}
}	
