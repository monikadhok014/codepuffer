package javato.activetesting;

import java.util.*;

import javato.activetesting.T.AdderMethsMap;
import javato.activetesting.T.ConstructorInfoMap;
import javato.activetesting.T.PerMethodSummaryMap;

public class P {
	
	static String summaryFile = "Summary.txt";
	static String methodsFile = "methods.txt";
	static String removeAll = "<java.util.Collection: boolean removeAll(java.util.Collection)>";
	static String arraycopy = "<java.lang.System: void arraycopy(java.lang.Object,int,java.lang.Object,int,int)>";
	static String array_contains = "<java.util.ArrayList: boolean contains(java.lang.Object)>";
	static String array_removeAll = "<java.util.ArrayList: boolean removeAlljava.util.Collection)>";
	static String array_retainAll = "<java.util.ArrayList: boolean retainAll(java.util.Collection)>";
	static String array_containsAll = "<java.util.ArrayList: boolean containsAll(java.util.Collection)>";
	static String array_remove = "<java.util.ArrayList: boolean remove(java.lang.Object)>";
	static String array_remove_nonbool = "<java.util.ArrayList: java.lang.Object remove(int)>";
				
	static String set_retainAll = "<java.util.Set: boolean retainAll(java.util.Collection)>";
	static String set_removeAll = "<java.util.Set: boolean removeAll(java.util.Collection)>";
	static String set_containsAll = "<java.util.Set: boolean containsAll(java.util.Collection)>";
	static String ARRAY =  "java.util.ArrayList";
	
	static ArrayList<String> arrayMethods = new ArrayList<String>(
					    Arrays.asList(array_contains, array_remove, array_removeAll, array_retainAll,
					    		set_retainAll, array_remove_nonbool, array_containsAll,
					    		set_removeAll, set_containsAll));
}
