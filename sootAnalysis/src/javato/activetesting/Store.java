/*
 * Changes the given class summary from java19 to java form.
 */
package javato.activetesting;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javato.activetesting.T.AdderMethsMap;
import javato.activetesting.T.ClassSummaryMap;
import javato.activetesting.T.ConstructorInfo;
import javato.activetesting.T.ConstructorInfoMap;
import javato.activetesting.T.Detail;
import javato.activetesting.T.LoopDepthMap;
import javato.activetesting.T.LoopDetailsMap;
import javato.activetesting.T.MethodDetail;
import javato.activetesting.T.MethodDetailMap;
import javato.activetesting.T.PerMethodSummaryMap;

public class Store {

	static ClassSummaryMap newSummary = new ClassSummaryMap();

	public static void main(String[] args)
	{
		Store r = new Store();
		ObjectFileStore mof = new ObjectFileStore();
		ClassSummaryMap summs = mof.returnObject("revisedSummary.txt");
		r.displayObjects(summs);

		OutputStream ops = null;
		ObjectOutputStream objOps = null;
		try{
			ops = new FileOutputStream("BaseSummaries.txt");
			objOps = new ObjectOutputStream(ops);
			objOps.writeObject(newSummary);
			objOps.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{
				if(objOps != null) objOps.close();
			} catch (Exception ex){

			}
		}
		summs = mof.returnObject("BaseSummaries.txt");
		Iterator<String> iterator = summs.keySet().iterator();
		while (iterator.hasNext()) {
			String key3 = iterator.next().toString();
			ClassSummary value3 = summs.get(key3);
			value3.print();
		}
	}

	//Display the object 
	private void displayObjects(ClassSummaryMap emp)
	{
		Iterator<String> iterator = emp.keySet().iterator();
		while (iterator.hasNext()) {
			String key3 = iterator.next().toString();
			ClassSummary value3 = emp.get(key3);
			//Rename HashMap<String, constructorInfo> constructorPropMap;
			ConstructorInfoMap constructorPropMap_new = renameConstructorInfo(value3.constructorPropMap);
			//Rename class properties map 
			HashMap<String, String> classPropMap_new =  new HashMap<String, String>();
			Iterator<String> classPropMaps = value3.classPropMap.keySet().iterator();
			while(classPropMaps.hasNext())
			{
				String str = classPropMaps.next();
				String ptr = value3.classPropMap.get(str);
				String str_new = str.replace("benchmarks.instrumented.java19","java");
				String ptr_new = ptr.replace("benchmarks.instrumented.java19","java");
				classPropMap_new.put(str_new, ptr_new);
			}
			//Rename adderMeths.
			Iterator<String> iterator_adder = value3.adderMeths.keySet().iterator();
			AdderMethsMap adderMeths_new =  new AdderMethsMap();
			while (iterator_adder.hasNext()) {
				String key_adder = iterator_adder.next().toString();
				HashSet<String> value_adder = value3.adderMeths.get(key_adder);
				HashSet<String> value_adder_new = new HashSet<String>();
				for(String str : value_adder)
				{
					String str_new = str.replace("benchmarks.instrumented.java19","java");
					value_adder_new.add(str_new);
				}
				String key_adder_new = key_adder.replace("benchmarks.instrumented.java19","java");
				adderMeths_new.put(key_adder_new,value_adder_new);
			}
			//Renamed perMethodSummaries.
			PerMethodSummaryMap summary = new PerMethodSummaryMap();
			Iterator<String> iterator_summ = value3.summary.keySet().iterator();
			while (iterator_summ.hasNext()) {
				String key_summ = iterator_summ.next().toString();
				PerMethodSummary value_summ = value3.summary.get(key_summ);
				PerMethodSummary p = rename(value_summ);
				String str = key_summ.replace("benchmarks.instrumented.java19","java");
				summary.put(str, p);
			}
			String str = key3.replace("benchmarks.instrumented.java19","java");
			ClassSummary new_val = new ClassSummary(summary, constructorPropMap_new, classPropMap_new, adderMeths_new);
			newSummary.put(str, new_val); 
		}
	}

	//Rename constructor information.
	private ConstructorInfoMap renameConstructorInfo(ConstructorInfoMap map)
	{
		ConstructorInfoMap new_map = new ConstructorInfoMap();

		Iterator<String> keys = map.keySet().iterator();
		while(keys.hasNext())
		{
			String key = keys.next();
			ConstructorInfo info =map.get(key);
			String name = info.className.replace("benchmarks.instrumented.java19","java");

			ArrayList<String> list = new ArrayList<String>();
			for(Object str : info.parameterTypes)
				list.add(str.toString().replace("benchmarks.instrumented.java19","java"));

			HashMap<String, String> constraints_new = new HashMap<String, String>();
			Iterator<String> constraint_keys = info.constraints.keySet().iterator();
			while(constraint_keys.hasNext())
			{
				String constraint_key = constraint_keys.next();
				String constraint_new_key = constraint_key.replace("benchmarks.instrumented.java19","java");
				String constraint_value = info.constraints.get(constraint_key);
				String constraints_value_new = constraint_value.replace("benchmarks.instrumented.java19","java");

				constraints_new.put(constraint_new_key, constraints_value_new);
			}
			ConstructorInfo new_info = new ConstructorInfo(list, name, constraints_new, info.constraints_indices);
			new_map.put(key.replace("benchmarks.instrumented.java19","java"), new_info);
		}
		return new_map;
	}

	//Rename perMethodSummary
	private PerMethodSummary rename(PerMethodSummary tmpS)
	{
		PerMethodSummary newS = new PerMethodSummary(new LoopDetailsMap(), 
				new LoopDepthMap(),
				new MethodDetailMap(),"","");
		newS.testName = tmpS.testName;
		
		//loop depth as it is
		newS.loopDepth = tmpS.loopDepth;
		//rename methods invoked
		MethodDetailMap tmpMethodsInvoked = new MethodDetailMap();
		Iterator iterator = tmpS.methodsInvoked.keySet().iterator();
		while (iterator.hasNext()) {
			String key3 = iterator.next().toString();
			MethodDetail value3 = tmpS.methodsInvoked.get(key3);
			String str2 = value3.type.replace("benchmarks.instrumented.java19","java");
			String str = key3.replace("benchmarks.instrumented.java19","java");
			MethodDetail md = new MethodDetail(str2,  value3.list);
			tmpMethodsInvoked.put(str, md);
		}
		newS.methodsInvoked = tmpMethodsInvoked;
		//handle loop details
		LoopDetailsMap tmpLoopDetails = new LoopDetailsMap();
		Iterator iterator1 = tmpS.loopDetails.keySet().iterator();
		while (iterator1.hasNext()) {
			Integer key4 = Integer.parseInt(iterator1.next().toString());
			Detail value4 = tmpS.loopDetails.get(key4);
			//Handle type if present 
			String str = value4.type.replace("benchmarks.instrumented.java19","java");
			MethodDetailMap tmpMethodsInvokedInLoop = new MethodDetailMap();

			//Handle methods invoked from the loop
			Iterator iterator2 = value4.methodsInvokedInLoop.keySet().iterator();
			while (iterator2.hasNext()) {
				String key5 = iterator2.next().toString();
				MethodDetail value5 = value4.methodsInvokedInLoop.get(key5);
				String ptr = key5.replace("benchmarks.instrumented.java19","java");
				String str3 = value5.type.replace("benchmarks.instrumented.java19","java");
				MethodDetail md = new MethodDetail(str3,  value5.list);
				tmpMethodsInvoked.put(str, md);
				tmpMethodsInvokedInLoop.put(ptr, value5);      
			}
			Detail d = new Detail(value4.idx, str, tmpMethodsInvokedInLoop);
			tmpLoopDetails.put(key4, d);
		}
		newS.loopDetails = tmpLoopDetails;
		return newS;
	}
}
