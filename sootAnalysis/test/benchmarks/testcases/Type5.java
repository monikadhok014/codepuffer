/*
 * Generate the test to invoke m132 with large number of elements in
 * l1 and l2. Here, execution of the loop depends on the size of the 
 * inputs l1 and l2.
 */

package benchmarks.testcases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
public class Type5{

	public static void main(String[] args) {
		Type5 t = new Type5();
		ArrayList<Object> l1 = new ArrayList<Object>();
		l1.add(new Object());
		l1.add(new Object());
		l1.add(new Object());
		l1.add(new Object());

		ArrayList<Object> l2 = new ArrayList<Object>();
		l2.add(new Object());
		l2.add(new Object());
		l2.add(new Object());

		t.m1(l1, l2);
	}

	public void m1(ArrayList l1, ArrayList l2)
	{
		if(l1.size() > l2.size())
		{
			for(Iterator itr = l1.iterator(); itr.hasNext();) 
			{
				l1.contains(itr.next());
			}
		}
	}
}
