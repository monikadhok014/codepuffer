/*
* Generate the test to invoke m51 with large number of elements in
* h1 and h2. 
*/

package benchmarks.testcases;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
public class Type3 {

	public static void main(String[] args) {
		Type3 t = new Type3();
		Type3.Type3_inner type_inner = t.new Type3_inner();
		
		HashSet<Object> set1 = new HashSet<Object>();
		set1.add(new Object());
		set1.add(new Object());
		set1.add(new Object());
		
		HashSet<Object> set2 = new HashSet<Object>();
		set2.add(new Object());
		set2.add(new Object());
		set2.add(new Object());
		
		type_inner.m1(set1, set2);
	}
	
	
	class Type3_inner{
		public void m1(Collection h1, Collection h2)
		{
			for(Iterator itr1 = h1.iterator(); itr1.hasNext();) 
			{
				m2(h2);
				itr1.next();
			}
		}
		public void m2(Collection set1)
		{
			for(Iterator itr = set1.iterator(); itr.hasNext();) 
			{
				itr.next(); 
			}
		}
	}
}

