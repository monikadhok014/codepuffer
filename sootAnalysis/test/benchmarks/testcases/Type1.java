/*
* Generates the test to invoke m11 with large number of elements in
* set1 and set2. 
*/

package benchmarks.testcases;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
public class Type1 {

	/* Testing this class */
	public static void main(String[] args) {
		Type1 t = new Type1();
		HashSet<Object> set1 = new HashSet<Object>();
		set1.add(new Object());
		set1.add(new Object());
		set1.add(new Object());
		
		HashSet<Object> set2 = new HashSet<Object>();
		set2.add(new Object());
		set2.add(new Object());
		set2.add(new Object());
		
		t.m1(set1, set2);
	}
	
	public void m1(HashSet set1, Collection set2)
	{
		for(Iterator itr1 = set1.iterator(); itr1.hasNext();) 
		{
			for(Iterator itr2 = set2.iterator(); itr2.hasNext();) 
			{
				itr1.next(); itr2.next();
				m3(true);
			}
			m2();
		}
	}

	public void m2()
	{
		m3(false);
	}

	public void m3(boolean bool)
	{
		//Some operations.
	}
	
	public void m4()
	{
		//Some operations.
	}
}

