/*
 * Generate the test to invoke m61 with large number of elements in
 * h1 and h2. 
 */

package benchmarks.testcases;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
public class Type4 {

	public static void main(String[] args) {
		Type4 t = new Type4();
		Type4.Type4_inner2 type_inner2 = t.new Type4_inner2();

		HashSet<Object> set1 = new HashSet<Object>();
		set1.add(new Object());
		set1.add(new Object());
		set1.add(new Object());
		
		HashSet<Object> set2 = new HashSet<Object>();
		set2.add(new Object());
		set2.add(new Object());
		set2.add(new Object());
		
		type_inner2.m1(set1, set2);
	}
	
	class Type4_inner1{
		
		public Type4_inner1() {}
		
		public void m1(Collection h1, Collection h2)
		{
			for(Iterator itr1 = h1.iterator(); itr1.hasNext();) 
			{
				m2(h2);
				itr1.next();
			}
		}
		public void m2(Collection set1)
		{
			for(Iterator itr = set1.iterator(); itr.hasNext();) 
			{
				itr.next(); 
			}
		}
	}
	
	class Type4_inner2 extends Type4_inner1 {
		public Type4_inner2() {}
	}
}

