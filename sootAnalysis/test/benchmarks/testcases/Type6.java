/*
 * Generate the test to invoke m102 with large number of elements in
 * set. Create the instance of Type10 using appropriate type as 
 * a parameter to the constructor.  
 */

package benchmarks.testcases;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
public class Type6{
	
	public static void main(String[] args) {

		HashSet<Object> set = new HashSet<Object>();
		set.add(new Object());
		set.add(new Object());
		set.add(new Object());
		Type6 t = new Type6(set); 
		t.m1(set);
	}
	
	Collection list;
	public Type6(Collection l)
	{
		list = l;
	}
	
	public void m1(Collection set)
	{
		for(Iterator itr = set.iterator(); itr.hasNext();) 
		{
			list.contains(itr.next());
		}
	}
}
