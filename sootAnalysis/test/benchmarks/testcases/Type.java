/*
 * Generate the test to invoke m132 with large number of elements in
 * l1 and l2. Here, execution of the loop depends on the size of the 
 * inputs l1 and l2.
 */

package benchmarks.testcases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
public class Type{

	public static void main(String[] args) {
		Type t = new Type();
		HashSet<Object> set1 = new HashSet<Object>();
		set1.add(new Object());
		set1.add(new Object());
		set1.add(new Object());

		ArrayList<Object> l = new ArrayList<Object>();
		l.add(new Object());
		l.add(new Object());

		t.m1(set1, l);

		HashSet<Object> set2 = new HashSet<Object>();
		set2.add(new Object());
		set2.add(new Object());
		set2.add(new Object());

		t.m2(set1, set2);
	}

	public void m1(Collection l1, Collection l2)
	{
		for(Iterator itr = l1.iterator(); itr.hasNext();) 
		{
			l2.contains(itr.next());
		}
	}

	public void m2(Collection h1, Collection h2)
	{
		for(Iterator itr1 = h1.iterator(); itr1.hasNext();) 
		{
			m3(h2);
			itr1.next();
		}
	}
	public void m3(Collection set1)
	{
		for(Iterator itr = set1.iterator(); itr.hasNext();) 
		{
			itr.next(); 
		}
	}

}
