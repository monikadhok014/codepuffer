/*
 * Generate the test to invoke m111 with large number of elements in
 * set. Create the instance of Type11 using appropriate type as 
 * a parameter to the constructor. Here, the parameter to the constructor 
 * restricts the type of the class property to arrayList.  
 */

package benchmarks.testcases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
public class Type7{
	
	public static void main(String[] args) {
		
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(new Object());
		list.add(new Object());
		list.add(new Object());
		Type7 t = new Type7(list);
		
		HashSet<Object> set = new HashSet<Object>();
		set.add(new Object());
		set.add(new Object());
		set.add(new Object());
		
		t.m1(set);
	
	}
	
	Collection list;
	
	public Type7(ArrayList l)
	{
		list = l;
	}
	
	public void m1(Collection set)
	{
		for(Iterator itr = set.iterator(); itr.hasNext();) 
		{
			list.contains(itr.next());
		}
	}
}
