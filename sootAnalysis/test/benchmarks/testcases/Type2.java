/*
 * Generate the test to invoke m21 with large number of elements in
 * set1 and set2. 
 */

package benchmarks.testcases;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
public class Type2 {

	public static void main(String[] args) {
		Type2 t = new Type2();
		HashSet<Object> set1 = new HashSet<Object>();
		set1.add(new Object());
		set1.add(new Object());
		set1.add(new Object());
		
		HashSet<Object> set2 = new HashSet<Object>();
		set2.add(new Object());
		set2.add(new Object());
		set2.add(new Object());
		
		t.m1(set1, set2);
	}
	
	public static void m1(Collection set1, Collection set2)
	{
		for(Iterator itr1 = set1.iterator(); itr1.hasNext();) 
		{
			for(Iterator itr2 = set2.iterator(); itr2.hasNext();) 
			{
				itr1.next(); itr2.next();
			}
		}
	}

	public void m2()
	{
		m3(false);
	}

	public void m3(boolean bool)
	{
		//Some operations.
	}
	
	public void m4()
	{
		//Some operations.
	}
}

