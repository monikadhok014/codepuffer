#To run other examples, execute ./sampleTest.sh <identifier> where identifier ranges from 1 to 7.
#For every buggy method, 5 tests are generated with different patterns as discussed in the paper.

ant -f build.xml clean
ant -f build.xml build build-tests
ant -f run.xml example$1
cd classes
java -cp ../lib/soot.jar:.:../../benchmark/. javato.activetesting.DeriveConstraints benchmarks.testcases.Type$1
java -cp ../lib/soot.jar:.:../../benchmark javato.activetesting.GetCandidateMethods benchmarks.testcases.Type$1
