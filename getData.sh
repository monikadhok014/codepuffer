#!/bin/bash

#Remove old figures 
rm figures/*.eps
rm figures/*.dat

cd scripts

#Create tests
./run.sh reuseSummary
./generateFigure9.sh

#Generate all the figures
cd ../figures
gnuplot fig10.plot
gnuplot fig9.dem
cd ../scripts

